# from pyinapp import AppStoreValidator, GooglePlayValidator, InAppValidationError
# from pyinapp.appstore import api_result_errors
from google.oauth2 import service_account
from googleapiclient.discovery import build

from vsms.environment import service_env as env
from vsms.misc.collections import Enum, EnumItem


class OS(Enum):
    ANDROID = EnumItem(0)
    IOS = EnumItem(1)


class GooglePlayAPI:

    def __init__(self, service_accout_file):
        credentials = service_account.Credentials.from_service_account_file(
            service_accout_file,
            scopes=['https://www.googleapis.com/auth/androidpublisher']
        )
        self.service = build('androidpublisher', 'v2', credentials=credentials)

    def purchase(self, product_id, token):
        r = self.service.purchases().products().get(packageName=env.inapp_androidbundle,
                                   productId=product_id, token=token).execute()
        return r
