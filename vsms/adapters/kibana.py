from pycontrib.misc.informer import Informer
from pycontrib.misc.patterns import Singleton
from pycontrib.service.environment import Environment
from tornado.escape import url_escape


class Kibana(Singleton):

    def initialize(self, *args, **kw):
        env = Environment()
        if 'KIBANA' not in env.config:
            raise BaseException('No KIBANA part in configuration.')

        self.cfg = env.config['KIBANA']
        self.base_url = self.cfg.get('baseurl')
        self.auto_refresh = self.cfg.get('autorefresh', 'False') == 'True'

    def get_dashboard_url(self, dashboard_name, refresh_period=None, time_from=None, interval=None, query=None):

        # ident:"{{ ident }}"
        query = query or '*'

        if dashboard_name not in self.cfg:
            Informer.error('Unknown dashboard {}'.format(dashboard_name))
            return None

        refresh_period = refresh_period or '20s'
        time_from = time_from or 'now-4h'
        interval = interval or '2m'

        dashboard_key = self.cfg[dashboard_name]
        dashboard_get_args = 'embed=true&_g=(refreshInterval:{refresh_interval},time:{time})'.format(
            refresh_interval='(display:Off,pause:!f,value:0)' if not self.auto_refresh else '(display:%2730 seconds%27,pause:!f,section:1,value:30000)',
            time='(from:{},interval:%27{}%27,mode:relative,timezone:Asia%2FKarachi,to:now)'.format(time_from, interval),
        )
        if query:
            dashboard_get_args += '&_a=(query:{query_string})'.format(
                query_string='(query_string:(analyze_wildcard:!t,query:%27{}%27))'.format(url_escape(query).replace('%3A', ':'))
            )
        dashboard_url = '{base_url}/dashboard/{dashboard_key}?{dashboard_get_args}'.format(
            base_url=self.base_url,
            dashboard_key=dashboard_key,
            dashboard_get_args=dashboard_get_args
        )

        return dashboard_url
