from vsms.misc.collections import Enum, EnumItem


class NETWORK(Enum):
    GOOGLE = EnumItem(0)
    FACEBOOK = EnumItem(1)
    TWITTER = EnumItem(2)
