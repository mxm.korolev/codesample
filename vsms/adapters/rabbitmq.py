import subprocess
import shlex
import json

from vsms.environment import service_env as env, informer
from vsms.adapters import rabbitmqadmin
from vsms.netcontrol.const import MESSAGE_TYPE


def online_idents():
    cmd = 'python3 {0} -H {host} -V {vhost} -u {user} -p {pass} -f raw_json list queues name'.format(
        rabbitmqadmin.__file__,
        **env.config['AMQP']
    )
    pipe = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE)
    try:
        jsn, err = pipe.communicate(timeout=5)
    except Exception as e:
        informer.error('Can not get online idents: rabbitmqadmin: {0}'.format(e))
        return []

    try:
        queueNames = [list(i.values())[0] for i in json.loads(jsn.decode())]
    except Exception as e:
        informer.error('Can not get online idents: rabbitmqadmin: {0}'.format(e))
        return []

    staff_names = [MESSAGE_TYPE.repr(x) for x in MESSAGE_TYPE.list()]
    return [q for q in queueNames if q not in staff_names and not q.startswith('amq.gen-')]
