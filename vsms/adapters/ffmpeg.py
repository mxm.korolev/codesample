from subprocess import CalledProcessError
from shlex import split as shlex_split
from subprocess import check_output
from os.path import split as path_split

from vsms.misc.collections import EnumItem, Enum
from vsms.environment import informer, service_env as env


class Format(Enum):
    VOD_HLS = EnumItem(0)
    LIVE_HLS = EnumItem(1)
    MP4 = EnumItem(2)


class Quality:
    def __init__(self, w, h, b):
        self.w, self.h, self.b = w, h, b


class Defaults:
    Hq = Quality(2560, 1440, 6000000)
    Mq = Quality(1920, 1080, 2500000)
    Lq = Quality(1280,  720, 1000000)


class FFMPEG:

    def __init__(self, raise_exception=True):
        self.ffmpeg_exec = '{} -loglevel error -y'.format(env.ffmpeg)
        self.ffprobe_exec = env.ffprobe
        self.raise_exception = raise_exception

    def _call(self, cmd, raise_exception=None):
        raise_exception = raise_exception if raise_exception is not None else self.raise_exception
        try:
            output = check_output(shlex_split(cmd))
        except CalledProcessError as e:
            informer.error('{}: {}'.format(cmd, e))
            if raise_exception:
                raise e
            return None
        informer.info('{}: Done'.format(cmd))
        return output.decode()

    def get_duration(self, i):
        cmd = '{} -v quiet -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 {}'.format(
            self.ffprobe_exec, i
        )
        duration = self._call(cmd, raise_exception=False)
        try:
            duration = float(duration)
        except (ValueError, TypeError) as e:
            informer.error('Can not get video {} duration: {}'.format(i, e))
            if self.raise_exception:
                raise e
            return None
        return duration

    def make_preview(self, i, o, ss=None, crop=None, scale=None, noaccurate_seek=False, quality=None):
        cmd = [self.ffmpeg_exec]
        if ss:
            cmd.append('-ss {}'.format(ss))
            if noaccurate_seek:
                cmd.append('-noaccurate_seek')
        cmd.append('-i {}'.format(i))
        if crop or scale:
            vf = []
            if scale:
                vf.append('scale=in_w/{scale}:in_h/{scale}'.format(scale=scale))
            if crop:
                vf.append('crop={}:{}'.format(*crop))
            cmd.append('-vf')
            cmd.append(','.join(vf))
        cmd.append('-q:v {}'.format(quality or 6))
        cmd.append('-vframes 1')
        cmd.append(o)
        return self._call(' '.join(cmd))

    # TODO: HLS var list support
    def make_multires(self, i, o_hq=None, o_mq=None, o_lq=None, o_hls_var=None,
                      format=Format.MP4, hls_time=15, ss=None, t=None, threads=1):
        cmd = ['{} -i {}'.format(self.ffmpeg_exec, i), ]
        playlist = ['#EXTM3U', '#EXT-X-VERSION:3']
        for o, q in [(o_mq, Defaults.Mq),
                     (o_hq, Defaults.Hq),
                     (o_lq, Defaults.Lq)]:
            if o is None:
                continue
            if None not in (ss, t):
                cmd += '-ss {}'.format(ss)
                cmd += '-t {}'.format(t)
                allow_copy = False
            else:
                allow_copy = True
            cmd += '-threads {}'.format(threads)
            if q is Defaults.Hq and allow_copy:
                cmd += '-c copy'
            elif q is Defaults.Hq:
                cmd += '-c:v h264 -b:v {} -c:a copy -fflags +sortdts'.format(q.b)
            else:
                cmd += '-c:v h264 -vf scale={}:{} -b:v {} -c:a copy -fflags +sortdts'.forma(q.w, q.h, q.b)
            if format == Format.VOD_HLS:
                cmd += '-f hls -hls_playlist_type 2 -hls_time {}'.format(hls_time)
            elif format == Format.MP4:
                pass
            cmd += o

            playlist += '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH={},RESOLUTION={}x{}'.format(q.b, q.w, q.h)
            playlist += path_split(o)[-1]

        call_ret = self._call(' '.join(cmd))

        if call_ret is not None and format == Format.VOD_HLS and o_hls_var:
            with open(o_hls_var, 'w') as f:
                f.write('\n'.join(playlist))

        return call_ret

