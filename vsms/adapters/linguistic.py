import csv

from vsms.environment import service_env as env


class Linguistics:

    def __init__(self):

        self.stop_words = set()
        with open(env.stop_words_fn, 'r') as f:
            reader = csv.reader(f, delimiter=',', quotechar='"')
            for line in reader:
                self.stop_words.add(line[0])

    @staticmethod
    def normalize(word):
        return word.lower()

    def is_stop_word(self, word):
        return word in self.stop_words
