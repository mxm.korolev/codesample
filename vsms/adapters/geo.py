import csv
from pytz import common_timezones, timezone
from datetime import datetime

from vsms.environment import service_env as env

class GeoNames:

    def __init__(self):

        self.continents = dict()
        self.countries = dict()
        for fn, field in [(env.continents_fn, self.continents),
                          (env.countries_fn, self.countries)]:
            with open(fn, 'r') as f:
                reader = csv.reader(f, delimiter=',', quotechar='"')
                for line in reader:
                    code, name = line
                    field[code] = name

    @property
    def continents_codes(self):
        return self.continents.keys()

    @property
    def countries_codes(self):
        return self.countries.keys()

    @property
    def timezones(self):
        now = datetime.utcnow()
        return [(tz, '{} UTC{:+03.0f}'.format(
            tz, timezone(tz).utcoffset(now).total_seconds() // 3600)) for tz in common_timezones
        ]
