import json
from random import randint
from tornado.httpclient import HTTPClient, HTTPRequest
from flask_restplus import fields
from oauth2client.service_account import ServiceAccountCredentials

from vsms.environment import service_env as env, informer


class Firebase():

    _cache = dict()

    @classmethod
    def share_url(cls, app_path):
        if app_path in cls._cache:
            link = cls._cache[app_path]
            if link or not randint(0, 100):
                return link

        payload = json.dumps({
            'dynamicLinkInfo': {
                'dynamicLinkDomain': env.firebase_dynamic_link_domain,
                'link': '{}?{}'.format(env.firebase_app_link, app_path),
                'androidInfo': {
                    'androidPackageName': env.firebase_android_package_name,
                    'androidMinPackageVersionCode': env.firebase_android_min_package_version_code
                },
                'iosInfo': {
                    'iosBundleId': env.firebase_ios_bundle_id,
                    'iosAppStoreId': env.firebase_ios_app_store_id
                }
            },
            'suffix': {
                'option': 'SHORT'
            }
        })

        firebase_url = '{}/v1/shortLinks?key={}'.format(env.firebase_firebase_url,
                                                        env.firebase_api_key)

        firebase_request = HTTPRequest(firebase_url, method='POST',
                                       headers={'Content-Type': 'application/json'},
                                       body=payload)

        firebase_client = HTTPClient()
        resp = firebase_client.fetch(firebase_request, raise_error=False)
        if resp.code != 200:
            informer.error('Can not construct dynamic link: {}'.format(resp))
            cls._cache[app_path] = None
            return None
        else:
            try:
                answer = json.loads(resp.body.decode())
            except Exception:
                informer.error('Can not get dynamic link: {}'.format(resp.body.decode()))
                cls._cache[app_path] = None
                return None
            else:
                cls._cache[app_path] = answer.get('shortLink')

        return cls._cache[app_path]

    @classmethod
    def _get_access_token(cls):
        SCOPE = ['https://www.googleapis.com/auth/firebase.messaging']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(env.firebase_service_account_json_fn, SCOPE)
        return credentials.get_access_token().access_token

    @classmethod
    def get_subscriptions(cls, iid_token):
        request = HTTPRequest(
            'https://iid.googleapis.com/iid/info/{}?details=true'.format(iid_token),
            method='GET',
            headers={'Content-Type': 'application/json',
                     'Authorization': 'key=' + env.firebase_api_key_secret},
        )
        resp = HTTPClient().fetch(request, raise_error=False)
        if resp.code != 200:
            informer.error('Fb: {}\n{} - {} - {}'.format(request.url, resp.code, resp.error, resp.body))
            return []

        try:
            data = json.loads(resp.body.decode())
            topics = data['rel']['topics']
        except (json.decoder.JSONDecodeError, KeyError) as e:
            informer.error('Fb: {}'.format(repr(e)))
            return []

        return list(topics.keys())

    @classmethod
    def subscribe(cls, iid_token, topic, method=None):
        method = method or 'POST'
        body = b'' if method == 'POST' else None
        request = HTTPRequest(
            'https://iid.googleapis.com/iid/v1/{}/rel/topics/{}'.format(iid_token, topic),
            method=method,
            headers={'Content-Type': 'application/json',
                     'Authorization': 'key=' + env.firebase_api_key_secret},
            body=body,
        )
        resp = HTTPClient().fetch(request, raise_error=False)
        if resp.code != 200:
            informer.error('Fb: {}\n{} - {} - {}'.format(request.url, resp.code, resp.error, resp.body))
            return None
        informer.info('Fb.subscribe.{} {}'.format(method, topic))

        return True

    @classmethod
    def unsubscribe(cls, iid_token, topic):
        return cls.subscribe(iid_token, topic, method='DELETE')

    @classmethod
    def publish_topic_message(cls, message, validate_only=False):
        request = HTTPRequest(
            'https://fcm.googleapis.com/v1/projects/ultracast-161409/messages:send',
            method='POST',
            headers={'Content-Type': 'application/json; UTF-8',
                     'Authorization': 'Bearer ' + cls._get_access_token()},
            body=json.dumps({
                'validate_only': validate_only,
                'message': message,
            }).encode(),
        )

        resp = HTTPClient().fetch(request, raise_error=False)
        if resp.code != 200:
            informer.error('Fb.publish: {}\n{} - {} - {}'.format(request.url, resp.code, resp.error, resp.body))
            return None

        return True


class ShareUrl(fields.Raw):

    def __init__(self, app_field, **kwargs):
        self.app_field = app_field
        super().__init__(**kwargs)

    def format(self, value):
        return Firebase.share_url('{}={}'.format(self.app_field, value))
