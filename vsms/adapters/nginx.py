from tornado.httpclient import HTTPClient, HTTPError
from xml.etree import ElementTree

from vsms.environment import service_env as env, informer


class Nginx():

    def __init__(self):
        self.http_client = HTTPClient()
        self.duration = dict()

    def fetch(self, query):
        try:
            resp = self.http_client.fetch(query)
        except HTTPError as e:
            informer.error('Nginx: {} {}'.format(query, e))
            return None
        return resp.body.decode()

    def rpc(self, query):
        return self.fetch(query) is not None

    def start_record(self, application, name):
        query = '{}/record/start?app={}&name={}'.format(
            env.nginx_control_url,
            application,
            name
        )
        return self.rpc(query)

    def stop_record(self, application, name):
        query = '{}/record/stop?app={}&name={}'.format(
            env.nginx_control_url,
            application,
            name
        )
        return self.rpc(query)

    def rtmp_drop(self, application, name):
        query = '{}drop/publisher?app={}&name={}'.format(
            env.nginx_control_url,
            application,
            name
        )
        return self.rpc(query)

    def rtmp_status(self):
        resp = self.fetch(env.nginx_stat_url)
        if not resp:
            return []

        rtmp_stream_descriptions = list()
        resp_xml = ElementTree.fromstring(resp)
        for server in resp_xml.findall('server'):
            for application in server.findall('application'):
                for live in application.findall('live'):
                    for stream in live.findall('stream'):
                        stream_description = [(i.tag, i.text) for i in stream if i.text is not None]
                        if stream.find('meta') and stream.find('meta').find('video'):
                            stream_description += [(i.tag, i.text) for i in stream.find('meta').find('video') if i.text is not None]
                        stream_description = dict(map(lambda kv: (kv[0], int(kv[1]) if kv[1].isdigit() else kv[1]), stream_description))
                        name = stream_description.get('name')
                        if name and name[-3:] in ('_lq', '_mq', '_hq'):
                            stream_description['ident'] = name[:-3]
                        rtmp_stream_descriptions.append(stream_description)

        return rtmp_stream_descriptions
