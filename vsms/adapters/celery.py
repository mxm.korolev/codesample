from datetime import datetime
from celery import Celery
from celery.backends.mongodb import MongoBackend
from celery import states
from mongoengine.queryset import DoesNotExist, MultipleObjectsReturned
from vsms.environment import service_env as env, informer
from vsms.models.task import Task

from celery.app.backends import BACKEND_ALIASES
BACKEND_ALIASES['mongoengine'] = 'vsms.adapters.celery:MongoEngineBackend'


class MongoEngineBackend(MongoBackend):

    def _store_result(self, task_id, result, state,
                      traceback=None, request=None, **kwargs):
        """Store return value and state of an executed task."""

        try:
            task = Task.objects.get(task_id=task_id)
        except DoesNotExist:
            task = Task(task_id=task_id)
            if celery.current_worker_task is None:
                task.type = 'UNKNOWN'
            else:
                task.type = celery.current_worker_task.name.split('.')[-1]
        task.result = str(result)
        task.state = state
        task.date = datetime.utcnow()
        task.traceback = traceback
        if state == states.STARTED:
            task.date_started = datetime.utcnow()
            task.args = str(result)

        task.save()

        return result


celery = Celery(
        env.name,
        backend='mongoengine',
        broker=env.amqp_url
    )


#TODO: move to Celery class
def setup_flask_celery(flask_app):
    flask_app.config['CELERY_BROKER_URL'] = env.amqp_url
    flask_app.config['CELERY_RESULT_BACKEND'] = 'mongoengine'
    flask_app.config['BROKER_USE_SSL'] = env.amqp_ssl
    flask_app.config['CELERY_MONGODB_BACKEND_SETTINGS'] = {
        'host': env.db_host,
        'port': env.db_port,
        'database': env.db_collection,
        'taskmeta_collection': 'tasks'
    }
    celery.conf.update(flask_app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with flask_app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery


def current_task():
    task_id = celery.current_worker_task.request.id
    try:
        task = Task.objects.get(task_id=task_id)
    except DoesNotExist as e:
        informer.error('{} for task {}'.format(e, task_id))
        return None
    except MultipleObjectsReturned as e:
        informer.error('{} for task {}'.format(e, task_id))
        return None
    return task


def is_duplicate_task(task):
    same_count = Task.objects(id__ne=task.id,
                              type=task.type,
                              args=task.args,
                              state__in=[states.STARTED, states.RETRY]).count()
    return same_count != 0


def check_tasks_state():

    inspect = celery.control.inspect()
    inspect_active = inspect.active()
    active_tasks = inspect_active.get(celery.current_worker_task.request.hostname, [])

    tasks = Task.objects(state__in=[states.STARTED, states.RETRY])
    for task in active_tasks:
        if any([at['id']==task.task_id for at in active_tasks]):
            continue
        task.state = states.REJECTED
        task.date = datetime.now()
        task.save()
        informer.info('Task {} marked as REJECTED'.format(task.task_id))
