import json
from elasticsearch import Elasticsearch

from vsms.environment import service_env as env, informer
from vsms.netcontrol.const import STATE


class ElasticSearch:

    def __init__(self):
        self.es = Elasticsearch(port=env.elasticsearch_port)

    def discover_idents(self):
        query = {
            'query': {
                'range': {
                    '@timestamp': {'gte': 'now-1m'}
                }
            },
            'aggs': {
                'idents': {
                    'terms': {
                        'field': 'ident.keyword',
                        'size': 100
                    }
                },
            }
        }
        r = self.es.search(index='telemetry', body=json.dumps(query), ignore_unavailable=True)
        if r['hits']['total'] == 0:
            return []

        idents = [x['key'] for x in r['aggregations']['idents']['buckets'] if x['key'] != self._env.name]
        return sorted(idents)

    def get_stitcher_config(self, stitcher_ident):
        query = {
            'query': {
                'bool': {
                    'filter': [
                        {'term': {'state.keyword': STATE.START}},
                        {'term': {'ident.keyword': stitcher_ident}},
                    ]
                },
            },
            'size': 1,
            'sort': [
                {'@timestamp': {'order': 'desc'}},
            ]
        }
        try:
            r = self.es.search(index='state', body=json.dumps(query), ignore_unavailable=True)
        except ConnectionError as e:
            informer.error(e)
            return None
        if r['hits']['total'] == 0:
            return None
        return r['hits']['hits'][0]['_source'].get('config')