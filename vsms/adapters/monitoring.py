import psutil
import shutil

from vsms.environment import service_env as env


class MonitoringMetric:

    def __init__(self):
        self.val = self.get()

    def __repr__(self):
        return repr(self.val)

    def alarm_repr(self):
        return repr(self)

    @staticmethod
    def get():
        raise NotImplementedError

    def is_ok(self):
        raise NotImplementedError


class RamMetric(MonitoringMetric):

    def __repr__(self):
        return 'RAM: {:.1f}%'.format(self.val)

    @staticmethod
    def get():
        return psutil.virtual_memory().percent

    def is_ok(self):
        return self.val < 80


class CpuMetric(MonitoringMetric):

    def __repr__(self):
        return 'CPU: {:.1f}%'.format(self.val)

    @staticmethod
    def get():
        return psutil.cpu_percent()

    def is_ok(self):
        return self.val < 80


class HddMetric(MonitoringMetric):

    def __repr__(self):
        return 'HDD: {:.1f}%'.format(max([usage for path, usage in self.val]))

    def alarm_repr(self):
        return 'HDD: {}'.format(
            ['{}({:.1f}%)'.format(path, usage) for path, usage in self.val]
        )

    @staticmethod
    def get():
        du = []
        for path in (env.project_dir,
                     env.nginx_hls_dir,
                     env.service_upload_dir,
                     env.nginx_record_dir):
            used = shutil.disk_usage(path).used
            total = shutil.disk_usage(path).total
            du.append((path, 100 * used / total))
        return du

    def is_ok(self):
        return any([usage < 90 for path, usage in self.val])


class SystemMonitoring:

    def __init__(self):
        self.metrics = (
            RamMetric(),
            CpuMetric(),
            HddMetric(),
        )

    def __repr__(self):
        return ', '.join([repr(m) for m in self.metrics])

    def is_ok(self):
        return all([m.is_ok() for m in self.metrics])

    def alarm_repr(self):
        return ', '.join([m.alarm_repr() for m in self.metrics if not m.is_ok()])
