import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from vsms.environment import service_env as env, informer
from vsms.models.user import User, Role


class Mailer:

    @classmethod
    def send(cls, subject, body, to=None):
        if to is None:
            role = Role.objects.get(name=Role.monitor)
            users = User.objects(roles=role)
            to = [u.email for u in users]
        if not to:
            return

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = env.mail_fromaddr
        msg['To'] = to[0]
        msg.attach(MIMEText(body, 'html'))
        server = smtplib.SMTP(env.mail_smtpserver, 587)
        server.ehlo()
        server.starttls()
        server.login(env.mail_fromaddr, env.mail_password)
        server.sendmail(msg['From'], to, msg.as_string())
        server.quit()

        informer.info('Mail sent: subj="{}" to={}'.format(subject, to))
