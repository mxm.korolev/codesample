from functools import partial
from flask_restplus import fields

from vsms.models import db
from vsms.environment import service_env as env
from vsms.misc.time import unix_time, utcnow_unix_time

IdFieldSerializer = partial(fields.String, attribute='id', required=True)

TitleField = partial(db.StringField, max_length=300)
TitleFieldSerializer = partial(fields.String, required=True)

DescriptionField = partial(db.StringField)
DescriptionFieldSerializer = partial(fields.String)

OrientationField = partial(db.FloatField, nullable=True, min_value=0, max_value=1)
OrientationFieldSerializer = partial(fields.Float)

FileNameField = partial(db.StringField, max_length=200)

TagListField = partial(db.ListField, db.StringField(max_length=100))
TagListFieldSerializer = partial(fields.List, fields.String, default=[])


class DateTimeFieldSerializer(fields.Raw):
    __schema_type__ = 'number'

    def format(self, value):
        return unix_time(value)


class FormattedStringFieldSerializer(fields.String):

    def __init__(self, format_str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.format_str = format_str

    def format(self, value):
        return self.format_str.format(value)


class StaticUrlFieldSerializer(FormattedStringFieldSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(env.service_cdn_url + '/{}', *args, **kwargs)


class UTCNowFieldSerializer(fields.Raw):
    def output(self, key, obj):
        return utcnow_unix_time()


class ListCountFieldSerializer(fields.Integer):

    def __init__(self, *args, **kwargs):
        self.fields = kwargs.pop('fields')
        super().__init__(*args, **kwargs)

    def output(self, key, obj):
        out = 0
        for f in self.fields:
            val = getattr(obj, f)
            if val:
                out += len(val)
        return out
