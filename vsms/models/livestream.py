from flask_restplus import fields

from vsms.models import db
from vsms.models.fields import TitleField, TitleFieldSerializer, OrientationFieldSerializer, \
    FileNameField, StaticUrlFieldSerializer
from vsms.models.base import CMSModel
from vsms.models.stitcher import Stitcher
from vsms.models.user import User
from vsms.adapters.firebase import ShareUrl


class LiveStream(CMSModel):
    
    meta = {
        'collection': 'livestreams',
        'strict': False,
        'static_dir': 'livestream',
        'indexes': [
            'available',
            'stitcher'
        ],
        'index_cls': False,
        'index_background': True,
    }
    
    title = TitleField()
    image_filename = FileNameField()
    stitcher = db.ReferenceField(Stitcher, reverse_delete_rule=db.DENY, required=True)
    upstreams = db.ListField(db.StringField(max_length=300))
    private = db.BooleanField(default=False)
    vendor = db.ReferenceField(User, reverse_delete_rule=db.NULLIFY)
    views = db.IntField(default=0)
    active = db.IntField(default=0)

    _serializer_model = {
        'id': fields.String(required=True, description='Id'),
        'title': TitleFieldSerializer(),
        'image_url': StaticUrlFieldSerializer(attribute='image_filename', required=True, description='Image'),
        'stitcher': fields.Nested(Stitcher.serializer()),
        'share_url': ShareUrl(attribute='id', app_field='livestream_id', description='Firebase share url'),
        'views': fields.Integer(description='Views Amount'),
        'active': fields.Integer(),
    }

    def __str__(self):
        return '{} ({})'.format(self.title, self.stitcher)
