from flask_restplus import fields

from vsms.models import db as db
from vsms.models.base import Model
from vsms.models.fields import UTCNowFieldSerializer


class GlobalConfiguration(Model):

    meta = {
        'collection': 'global_configuration',
        'strict': False
    }
    just_one = db.IntField(default=0, uniq=True)
    android_minimal = db.StringField(max_length=100, null=False)
    android_latest = db.StringField(max_length=100, null=False)
    android_donation = db.BooleanField(default=False)
    ios_minimal = db.StringField(max_length=100, null=False)
    ios_latest = db.StringField(max_length=100, null=False)
    ios_donation = db.BooleanField(default=False)
    _serializer_model = {
        'android_minimal': fields.String(),
        'android_latest': fields.String(),
        'android_donation': fields.Boolean(),
        'ios_minimal': fields.String(),
        'ios_latest': fields.String(),
        'ios_donation': fields.Boolean(),
        'utc': UTCNowFieldSerializer(),
    }
