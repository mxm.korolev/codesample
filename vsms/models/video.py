from os import remove
from os.path import join as path_join
from random import randint
from time import sleep
from flask_restplus import fields
from mongoengine import signals

from vsms.adapters.celery import celery
from vsms.adapters.ffmpeg import FFMPEG, Format
from vsms.adapters.firebase import ShareUrl
from vsms.adapters.monitoring import CpuMetric
from vsms.environment import service_env as env
from vsms.misc.flask import flash
from vsms.models import db
from vsms.models.fields import TitleField, TitleFieldSerializer, OrientationField, OrientationFieldSerializer, \
    StaticUrlFieldSerializer, DateTimeFieldSerializer, FileNameField
from vsms.models.base import Model, CMSModel
from vsms.models.category import Category
from vsms.models.event import Event
from vsms.models.channel import Channel


class Video(CMSModel):

    meta = {
        'collection': 'videos',
        'strict': False,
        'static_dir': 'video',
        'indexes': [
            'available',
            'private',
            'category',
            'channel',
            {
                'fields': ['$title', '$description', '$tags', '$ngrams'],
                'default_language': 'english',
                'weights': {'title': 10, 'description': 10, 'tags': 10, 'ngrams': 1},
            },
        ],
        'index_cls': False,
        'index_background': True,
    }

    title = TitleField()
    date = db.DateTimeField()
    ts = db.IntField()
    description = db.StringField()
    tags = db.ListField(db.StringField(max_length=100))
    video_filename = FileNameField()
    hq_video_filename = FileNameField()
    mq_video_filename = FileNameField()
    lq_video_filename = FileNameField()
    image_filename = FileNameField()
    full_image_filename = FileNameField()
    video_duration = db.FloatField(default=0)
    category = db.ReferenceField(Category, reverse_delete_rule=db.NULLIFY)
    private = db.BooleanField(default=False)
    views = db.IntField(default=0)
    ngrams = db.ListField(db.StringField(max_length=10))
    start_x = OrientationField()
    start_y = OrientationField()
    rotation_degree = db.IntField(min_value=0, max_value=360)
    coverage_degree = db.IntField(min_value=0, max_value=360)
    channel = db.ReferenceField(Channel, reverse_delete_rule=db.NULLIFY)
    _serializer_model = {
        'id': fields.String(required=True, description='Id'),
        'date': DateTimeFieldSerializer(required=True, description='Video datetime'),
        'ts': fields.Integer(),
        'image_url': StaticUrlFieldSerializer(attribute='image_filename', required=True, description='Preview'),
        'full_image_url': StaticUrlFieldSerializer(attribute='full_image_filename', required=True, description='Full Image'),
        'auto_url': StaticUrlFieldSerializer(attribute='video_filename', required=True, description='Auto URL'),
        'hq_url': StaticUrlFieldSerializer(attribute='hq_video_filename', required=True, description='High'),
        'mq_url': StaticUrlFieldSerializer(attribute='mq_video_filename', required=True, description='Middle'),
        'lq_url': StaticUrlFieldSerializer(attribute='lq_video_filename', required=True, description='Low'),
        'title': TitleFieldSerializer(),
        'description': fields.String(required=True, description='Description'),
        'video_duration': fields.Float(required=True, description='Duration in seconds'),
        'tags': fields.List(fields.String, default=[]),
        'share_url': ShareUrl(attribute='id', app_field='video_id', description='Firebase share url'),
        'category': fields.Nested(Category.serializer(), descripton='Category', allow_null=True),
        'views': fields.Integer(description='Views Amount'),
        'start_x': OrientationFieldSerializer(allow_null=True),
        'start_y': OrientationFieldSerializer(allow_null=True),
        'rotation_degree': fields.Integer(allow_null=True),
        'coverage_degree': fields.Integer(allow_null=True),
        'channel': fields.Nested(Channel.serializer(), allow_null=True)
    }

    def __str__(self):
        return '{} ({})'.format(self.title, self.video_duration)


class EditVideo(Model):
    video = db.ReferenceField(Video, required=True)
    title = TitleField()
    category = db.ReferenceField(Category)
    available = db.BooleanField(default=True)
    _serializer_model = {
        'video': fields.String(attribute='video.id'),
        'title': TitleFieldSerializer(),
        'category': fields.String(attribute='category.id'),
        'available': fields.Boolean(),
    }


class EditVideoResponse(Model):
    video = db.ReferenceField(Video)
    _serializer_model = {
        'video': fields.Nested(Video.serializer(), allow_null=True),
    }


class GetVideo(Model):
    video = db.ReferenceField(Video)
    tags = db.ListField(db.StringField(max_length=100), default=[])
    first = db.IntField(min_value=0, default=0)
    count = db.IntField(min_value=1, default=10)
    text = db.StringField()
    categories = db.ListField(db.ReferenceField(Category))
    private = db.BooleanField(default=False)
    _serializer_model = {
        'video': fields.String(),
        'first': fields.Integer(default=0),
        'count': fields.Integer(default=10),
        'tags': fields.List(fields.String, default=[]),
        'text': fields.String(),
        'categories': fields.List(fields.String(), default=[]),
        'private': fields.Boolean(default=False),
    }


class GetVideoResponse(Model):
    total = db.IntField(default=0)
    videos = db.ListField(db.ReferenceField(Video))
    _serializer_model = {
        'total': fields.Integer(),
        'videos': fields.List(fields.Nested(Video.serializer())),
    }


class GetRelatedVideo(Model):
    video = db.ReferenceField(Video)
    event = db.ReferenceField(Event)
    first = db.IntField(min_value=0, default=0)
    count = db.IntField(min_value=1, default=10)
    _serializer_model = {
        'video': fields.String,
        'event': fields.String,
        'first': fields.Integer(default=0),
        'count': fields.Integer(default=10),
    }


class VideoSuggestion(Model):
    meta = {
        'collection': 'video_suggestions',
        'strict': False,
        'indexes': [
            'ngrams',
            {
                'fields': ['$collocations'],
                'default_language': 'english',
                'weights': {'collocations': 1},
                'cls': False,
            },
        ],
        'index_cls': False,
        'index_background': True,
    }
    word = db.StringField()
    freq = db.IntField()
    ngrams = db.ListField(db.StringField())
    collocations = db.ListField(db.StringField())


class GetSuggestion(Model):
    text = db.StringField()
    _serializer_model = {
        'text': fields.String(),
    }


class GetSuggestionResponse(Model):
    extensions = db.ListField(db.StringField())
    collocations = db.ListField(db.StringField())
    _serializer_model = {
        'extensions': fields.List(fields.String()),
        'collocations': fields.List(fields.String()),
    }


@celery.task
def static_video_make_hls(video_id=None, video_upload_dir=None):

    celery.backend.mark_as_started(
        celery.current_worker_task.request.id,
        video_id=video_id,
        video_upload_dir=video_upload_dir
    )

    if video_id:
        video = Video.objects.get(id=video_id)
    elif video_upload_dir:
        video = Video.objects.get(upload_dir=video_upload_dir)
    else:
        raise Exception('Bad arguments')

    while CpuMetric.get() > 50:
        sleep(randint(300, 600))

    ffmpeg = FFMPEG(raise_exception=False)

    video_fn = video.video_filename
    video_fn_path = video.video_filename_path
    video.hq_video_filename = '{}_hq.m3u8'.format(video_fn)
    video.mq_video_filename = '{}_mq.m3u8'.format(video_fn)
    video.lq_video_filename = '{}_lq.m3u8'.format(video_fn)
    video.video_filename = '{}.m3u8'.format(video_fn)

    if ffmpeg.make_multires(video_fn_path, o_hq=video.hq_video_filename_path, o_mq=video.mq_video_filename_path,
                            o_lq=video.lq_video_filename_path, o_hls_var=video.video_filename_path,
                            format=Format.VOD_HLS) is None:
        raise Exception('FFMPEG error')

    if env.delete_outdated_files and not video_fn_path.endswith('m3u8'):
        remove(video_fn_path)

    Video.objects(id=video.id).update_one(
        set__video_filename=video.video_filename,
        set__hq_video_filename=video.hq_video_filename,
        set__mq_video_filename=video.hq_video_filename,
        set__lq_video_filename=video.hq_video_filename,
    )

    return 'Done'


def video_pre_save(_, document, force_image_reencode=False, force_video_reencode=False, **kwargs):
    video = document

    if video.id is None:
        prev_state = None
    else:
        prev_state = Video.objects.get(id=video.id)

    if not video.video_filename:
        return

    encode = (not prev_state or prev_state.video_filename != video.video_filename) \
             and not video.video_filename.endswith('m3u8')
    force_image_reencode = force_image_reencode or encode
    force_video_reencode = force_video_reencode or encode

    if not force_image_reencode and not force_video_reencode:
        return

    video.get_upload_path()

    if force_image_reencode:
        base_filename = video.video_filename.split('/')[-1]
        video.image_filename = path_join(video.upload_dir, '{}.jpeg'.format(base_filename))
        video.full_image_filename = path_join(video.upload_dir, '{}_full.jpeg'.format(base_filename))

        if prev_state and env.delete_outdated_files:
            for fn in (prev_state.image_filename_path,
                       prev_state.full_image_filename_path,):
                if fn is None:
                    continue
                try:
                    remove(fn)
                except OSError as e:
                    flash('Can not remove {}: {}'.format(fn, e))

        ffmpeg = FFMPEG(raise_exception=False)

        video_duration = ffmpeg.get_duration(video.video_filename_path)
        if video_duration is None:
            flash('Can not get video duration', category='error')
        else:
            video.video_duration = video_duration

        if ffmpeg.make_preview(video.video_filename_path, video.image_filename_path,
                               ss=5, crop=(320, 180), scale=4) is None:
            flash('Can not make cropped preview', category='error')
            video.image_filename = None

        if ffmpeg.make_preview(video.video_filename_path, video.full_image_filename_path) is None:
            flash('Can not make full preview', category='error')
            video.full_image_filename = None

    if force_video_reencode:
        video.hq_video_filename = video.video_filename

        if video.id:
            kwargs = dict(video_id=str(document.id))
        else:
            kwargs = dict(video_upload_dir=document.upload_dir)

        if not env.unittest:
            static_video_make_hls.apply_async(kwargs=kwargs, countdown=2)


signals.pre_save.connect(video_pre_save, sender=Video)
