from vsms.models import db
from vsms.models.base import Model
from vsms.adapters.geo import GeoNames


class GeoRule(Model):
    meta = {
        'collection': 'georules',
        'strict': False,
    }
    title = db.StringField(max_length=200, required=True)
    continents = db.ListField(db.StringField(max_length=2, choices=GeoNames().continents.items()))
    countries = db.ListField(db.StringField(max_length=2, choices=GeoNames().countries.items()))
    allow_policy = db.BooleanField(default=True)

    def __str__(self):
        return '{} ({}: {})'.format(self.title,
                                    'Allow' if self.allow_policy else 'Deny',
                                    ','.join((self.continents + self.countries)[:20]))
