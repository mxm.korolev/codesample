'''
Users models
'''

from vsms.models import db
from flask_security import MongoEngineUserDatastore, UserMixin, RoleMixin


class Role(db.Document, RoleMixin):
    super = 'super'
    content = 'content'
    dev = 'dev'
    vendor = 'vendor'
    monitor = 'monitor'
    
    name = db.StringField(
        max_length=50, 
        nullable=False, 
        unique=True, 
        choices=((super, 'Super User'),
                 (content, 'Content Manager'),
                 (dev, 'Developer'),
                 (vendor, 'Vendor'),
                 (monitor, 'Monitor'))
    )
    description = db.StringField(max_length=200)
    
    def __str__(self):
        return '{}'.format(self.name)


class User(db.Document, UserMixin):
    email = db.StringField(nullable=False, unique=True, max_length=255)
    password = db.StringField(nullable=False, max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    roles = db.ListField(db.ReferenceField(Role, reverse_delete_rule=db.DENY), default=[])
    
    def __str__(self):
        return '{} {}'.format(self.email, self.roles)


user_datastore = MongoEngineUserDatastore(db, User, Role)
