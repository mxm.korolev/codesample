from flask_restplus import fields
from time import sleep
from mongoengine import signals

from vsms.environment import service_env as env
from vsms.adapters.monitoring import CpuMetric
from vsms.adapters.celery import celery
from vsms.adapters.ffmpeg import FFMPEG, Format
from vsms.models import db
from vsms.models.base import Model, CMSModel
from vsms.models.fields import TitleField, TitleFieldSerializer, FileNameField, StaticUrlFieldSerializer, \
    OrientationField, OrientationFieldSerializer


class Ad(CMSModel):
    title = TitleField()
    hq_video_filename = FileNameField()
    mq_video_filename = FileNameField()
    lq_video_filename = FileNameField()
    start_x = OrientationField()
    start_y = OrientationField()
    _serializer_model = {
        'title': TitleFieldSerializer(),
        'hq_url': StaticUrlFieldSerializer(attribute='hq_video_filename'),
        'mq_url': StaticUrlFieldSerializer(attribute='mq_video_filename'),
        'lq_url': StaticUrlFieldSerializer(attribute='lq_video_filename'),
        'start_x': OrientationFieldSerializer(allow_null=True),
        'start_y': OrientationFieldSerializer(allow_null=True),
    }


@celery.task
def ad_make_multires(ad_id=None, ad_upload_dir=None):

    celery.backend.mark_as_started(
        celery.current_worker_task.request.id,
        ad_id=ad_id,
        ad_upload_dir=ad_upload_dir
    )

    if ad_id:
        ad = Ad.objects.get(id=ad_id)
    elif ad_upload_dir:
        ad = Ad.objects.get(upload_dir=ad_upload_dir)
    else:
        raise Exception('Bad arguments')

    while CpuMetric.get() > 50:
        sleep(600)

    ad.mq_video_filename = '{}_mq.mp4'.format(ad.hq_video_filename)
    ad.lq_video_filename = '{}_lq.mp4'.format(ad.hq_video_filename)

    ffmpeg = FFMPEG(raise_exception=False)
    if ffmpeg.make_multires(ad.hq_video_filename_path, o_mq=ad.mq_video_filename_path, o_lq=ad.lq_video_filename_path,
                            format=Format.MP4) is None:
        raise Exception('FFMPEG error')

    ad.save()
    return 'Done'


def ad_pre_save(_, document, **kwargs):
    ad = document

    if ad.id is None:
        prev_state = None
    else:
        prev_state = Ad.objects.get(id=ad.id)

    if not ad.video_filename:
        return

    if prev_state and prev_state.hq_video_filename == ad.hq_video_filename:
        return

    ad.get_upload_path()

    if ad.id:
        kwargs = dict(ad_id=str(document.id))
    else:
        kwargs = dict(ad_upload_dir=document.upload_dir)

    if not env.unittest:
        ad_make_multires.apply_async(kwargs=kwargs, countdown=2)


signals.pre_save.connect(ad_pre_save, sender=Ad)


class AdCampaign(Model):
    title = TitleField()
    period = db.IntField(default=300)
    ads = db.ListField(db.ReferenceField(Ad, reverse_delete_rule=db.PULL))
    _serializer_model = {
        'title': TitleFieldSerializer(),
        'period': fields.Integer(),
        'ads': fields.List(fields.Nested(Ad.serializer())),
    }


class AdCountdown(Model):
    ad = db.ReferenceField(Ad, required=True)
    ts = db.IntField()
    _serializer_model = {
        'ad': fields.Nested(Ad.serializer()),
        'ts': fields.Integer(),
    }
