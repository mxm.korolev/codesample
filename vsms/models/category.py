from flask_restplus import fields

from vsms.models import db
from vsms.models.base import CMSModel
from vsms.models.fields import StaticUrlFieldSerializer


class Category(CMSModel):

    meta = {
        'static_dir': 'category',
        'strict': False,
    }

    name = db.StringField(max_length=200, required=True)
    description = db.StringField()
    icon_filename = db.StringField()
    image_filename = db.StringField()
    video_count = db.IntField(default=0)

    _serializer_model = {
        'id': fields.String(required=True, description='Id'),
        'name': fields.String(description='Name'),
        'description': fields.String(description='Description'),
        'icon_url': StaticUrlFieldSerializer(attribute='icon_filename', required=True, description='Icon'),
        'image_url': StaticUrlFieldSerializer(attribute='image_filename', required=True, description='Image'),
        'video_count': fields.Integer(),
    }

    def __str__(self):
        return self.name