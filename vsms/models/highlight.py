from flask_restplus import fields
from os.path import join as path_join
from os import remove
from mongoengine import signals

from vsms.environment import service_env as env
from vsms.misc.flask import flash
from vsms.models import db
from vsms.models.fields import TitleField, TitleFieldSerializer, OrientationField, OrientationFieldSerializer, \
    FileNameField
from vsms.models.base import CMSModel, Model
from vsms.models.fields import StaticUrlFieldSerializer
from vsms.models.video import Video
from vsms.models.event import Event
from vsms.adapters.ffmpeg import FFMPEG


class Highlight(CMSModel):
    meta = {
        'strict': False,
        'collection': 'highlights',
        'static_dir': 'highlight',
    }
    title = TitleField()
    origin_video = db.ReferenceField(Video, required=True, reverse_delete_rule=db.DENY)
    start = db.FloatField(default=0, min_value=0)
    duration = db.FloatField(default=7, min_value=7)
    hq_video_filename = FileNameField()
    mq_video_filename = FileNameField()
    image_filename = FileNameField()
    full_image_filename = FileNameField()
    tags = db.ListField(db.StringField(max_length=100))
    start_x = OrientationField()
    start_y = OrientationField()
        
    _serializer_model = {
        'id': fields.String(required=True, description='Id'),
        'origin_video': fields.Nested(Video.serializer()),
        'start': fields.Float(),
        'image_url': StaticUrlFieldSerializer(attribute='image_filename', required=True, description='Preview'),
        'full_image_url': StaticUrlFieldSerializer(attribute='full_image_filename', required=True, description='Full Image'),
        'video_url_hq': StaticUrlFieldSerializer(attribute='hq_video_filename', required=True, description='High'),
        'video_url_mq': StaticUrlFieldSerializer(attribute='mq_video_filename', required=True, description='Middle'),
        'title': TitleFieldSerializer(),
        'duration': fields.Float(required=True, description='Duration in seconds'),
        'tags': fields.List(fields.String, default=[]),
        'start_x': OrientationFieldSerializer(),
        'start_y': OrientationFieldSerializer(),
    }


class GetHighlights(Model):
    count = db.IntField(min_value=1, max_value=100, default=6)
    tags = db.ListField(db.StringField(max_length=100), default=[])
    _serializer_model = {
        'count': fields.Integer(default=6),
        'tags': fields.List(fields.String, default=[]),
    }

    def __repr__(self):
        return 'count={}, tags={}'.format(self.count, self.tags)


class GetHighlightsResponse(Model):
    events = db.ListField(db.ReferenceField(Event))
    highlights = db.ListField(db.ReferenceField(Highlight))    
    _serializer_model = {
        'events': fields.List(fields.Nested(Event.serializer())),
        'highlights': fields.List(fields.Nested(Highlight.serializer())),
    }

    def __repr__(self):
        return ', '.join((
            super().__repr__(), 
            'events.count={}'.format(len(self.events)),
            'highlights.count={}'.format(len(self.highlights))
        ))


def highlight_pre_save(_, document, force_reencode=False, **kwargs):
    highlight = document

    if highlight.id is None:
        prev_state = None
    else:
        prev_state = Highlight.objects.get(id=highlight.id)

    if prev_state is None and highlight.origin_video is None:
        return

    if prev_state and \
            prev_state.origin_video == highlight.origin_video and \
            prev_state.start == highlight.start and \
            prev_state.duration == highlight.duration and \
            not force_reencode:
        return

    if prev_state and env.delete_outdated_files:
        for fn in (prev_state.hq_video_filename_path,
                   prev_state.mq_video_filename_path,
                   prev_state.image_filename_path,
                   prev_state.full_image_filename_path,):
            if fn is None:
                continue
            try:
                remove(fn)
            except OSError as e:
                flash('Can not remove {}: {}'.format(fn, e))

    if not highlight.origin_video or not highlight.origin_video.hq_video_filename:
        highlight.hq_video_filename = None
        highlight.mq_video_filename = None
        highlight.start = None
        highlight.duration = None
        return

    highlight.get_upload_path()
    origin_fn = highlight.origin_video.hq_video_filename_path
    base_fn = highlight.origin_video.hq_video_filename.split('/')[-1]
    highlight.image_filename = path_join(highlight.upload_dir, '{}.jpeg'.format(base_fn))
    highlight.full_image_filename = path_join(highlight.upload_dir, '{}_full.jpeg'.format(base_fn))
    highlight.hq_video_filename = path_join(highlight.upload_dir, '{}_hq.mp4'.format(base_fn))
    highlight.mq_video_filename = path_join(highlight.upload_dir, '{}_mq.mp4'.format(base_fn))

    ffmpeg = FFMPEG(raise_exception=False)

    if ffmpeg.make_multires(origin_fn,
                            o_hq=highlight.hq_video_filename_path,
                            o_mq=highlight.mq_video_filename_path,
                            ss=highlight.start,
                            t=highlight.duration,
                            threads=3) is None:
        flash('Can not encode video', category='error')
        highlight.hq_video_filename = None
        highlight.mq_video_filename = None
        return

    if ffmpeg.make_preview(highlight.hq_video_filename_path, highlight.image_filename_path,
                           ss=2, crop=(320, 180), scale=4) is None:
        flash('Can not make cropped preview', category='error')
        highlight.image_filename = None

    if ffmpeg.make_preview(highlight.hq_video_filename_path, highlight.full_image_filename_path) is None:
        flash('Can not make full preview', category='error')
        highlight.full_image_filename = None


signals.pre_save.connect(highlight_pre_save, sender=Highlight)
