from flask_restplus import fields

from vsms.models import db
from vsms.models.fields import TitleField, TitleFieldSerializer, IdFieldSerializer
from vsms.misc.collections import Enum, EnumItem
from vsms.models.base import Model


class PUSH_NOTIFICATION_TYPE(Enum):
    EVENT_START = EnumItem(0)
    EVENT_FINISH = EnumItem(1)


class PushNotificationData(Model):
    tp = db.StringField(choices=list(PUSH_NOTIFICATION_TYPE.__map__.values()), required=True)
    title = TitleField()
    _serializer_model = {
        'tp': fields.String(example=tp.choices),
        'id': IdFieldSerializer(),
        'title': TitleFieldSerializer(),
    }


class PushNotificationAndroid(Model):
    collapse_key = db.StringField(default='')
    _serializer_model = {
        'collapse_key': fields.String(),
    }


class PushNotificationApnsHeaders(Model):
    apns_priority = db.IntField(choices=[('5', 'NORMAL'), ('10', 'HIGH')], default='5')
    _serializer_model = {
        'apns-priority': fields.String(attribute='apns_priority', example=apns_priority.choices),
    }


class PushNotificationApnsPayloadAlert(Model):
    title = TitleField()
    body = db.StringField()
    _serializer_model = {
        'title': TitleFieldSerializer(),
        'body': fields.String(),
    }


class PushNotificationApnsPayloadAps(Model):
    thread_id = db.StringField(default='')
    alert = db.ReferenceField(PushNotificationApnsPayloadAlert)
    content_available = db.IntField(default=1)
    _serializer_model = {
        'thread-id': fields.String(attribute='thread_id'),
        'alert': fields.Nested(PushNotificationApnsPayloadAlert.serializer()),
        'content-available': fields.Integer(attribute='content_available'),
    }


class PushNotificationApnsPayload(Model):
    aps = db.ReferenceField(PushNotificationApnsPayloadAps)
    _serializer_model = {
        'aps': fields.Nested(PushNotificationApnsPayloadAps.serializer()),
    }


class PushNotificationApns(Model):
    headers = db.ReferenceField(PushNotificationApnsHeaders)
    payload = db.ReferenceField(PushNotificationApnsPayload)
    _serializer_model = {
        'headers': fields.Nested(PushNotificationApnsHeaders.serializer()),
        'payload': fields.Nested(PushNotificationApnsPayload.serializer()),
    }


class PushNotification(Model):
    topic = db.StringField(required=True)
    data = db.ReferenceField(PushNotificationData)
    android = db.ReferenceField(PushNotificationAndroid)
    apns = db.ReferenceField(PushNotificationApns)
    _serializer_model = {
        'topic': fields.String(),
        'data': fields.Nested(PushNotificationData.serializer()),
        'android': fields.Nested(PushNotificationAndroid.serializer()),
        'apns': fields.Nested(PushNotificationApns.serializer()),
    }
