from flask_restplus import fields
from flask_restplus.fields import get_value
from copy import deepcopy

from vsms.api import api
from vsms.environment import service_env as env
from vsms.models import db
from vsms.models.base import CMSModel
from vsms.models.user import User
from vsms.models.fields import OrientationField, OrientationFieldSerializer, StaticUrlFieldSerializer


# TODO: deprecated
class LiveStreamUrl(fields.Raw):

    def format(self, value):
        manual_mode = get_value('manual_mode', self._obj)
        if manual_mode:
            return value
        name = get_value('name', self._obj)
        if self._key == 'auto_url':
            return '{}/{}.m3u8'.format(env.nginx_hls_server_url, name)
        for quality in ('hq', 'mq', 'lq'):
            if self._key.startswith(quality):
                return '{}/{}_{}/index.m3u8'.format(env.nginx_hls_server_url, name, quality)
        return None

    def output(self, key, obj):
        '''
        standart output() not calls format() for null
        '''
        self._obj = obj
        self._key = key

        value = get_value(key if self.attribute is None else self.attribute, obj)
        if value is None:
            return self.format(value)
        return super().output(key, obj)


class StitcherConfigurationStream(db.EmbeddedDocument):

    meta = {
        'strict': False,
    }

    id = db.StringField(max_length=2, default='hq', uniq=True, required=True)
    height = db.IntField(min_value=1024, default=1440, required=True)
    width = db.IntField(min_value=1024, default=2560, required=True)
    min_bitrate = db.IntField(min_value=200000, default=3000000, required=True)
    cur_bitrate = db.IntField(min_value=200000, default=3000000, required=True)
    max_bitrate = db.IntField(min_value=200000, default=3000000, required=True)

    @classmethod
    def serializer(cls):
        serializer_model = {
            'id': fields.String,
            'height': fields.Integer,
            'width': fields.Integer,
            'max_bitrate': fields.Integer,
            'min_bitrate': fields.Integer,
            'cur_bitrate': fields.Integer,
        }
        return api.model(cls.__name__, serializer_model)

    def marshal(self):
        return api.marshal_with(self.__class__.serializer())(lambda x: x)(self)


class StitcherConfiguration(db.EmbeddedDocument):

    meta = {
        'strict': False,
    }

    rtmp_url = db.StringField(max_length=200)
    gop_size = db.IntField(min_value=10, default=60)
    streams = db.ListField(db.EmbeddedDocumentField(StitcherConfigurationStream))
    mic_name = db.StringField(max_length=200, required=True, default='Line')
    mono_sound = db.BooleanField(default=True)
    external_mic = db.BooleanField(default=False)
    sound_enabled = db.BooleanField(default=True)
    wait_accelerometer = db.BooleanField(default=False)
    adaptive_bitrate = db.BooleanField(default=False)
    camera_url = db.StringField(max_length=200, required=True, default='tcp://0.0.0.0:2000')

    _serializer_model = {
        'rtmpUrl': fields.String(attribute='rtmp_url'),
        'gopSize': fields.Integer(attribute='gop_size'),
        'streams': fields.List(fields.Nested(StitcherConfigurationStream.serializer())),
        'mic_name': fields.String,
        'isMonoSound': fields.Boolean(attribute='mono_sound'),
        'useExternalMic': fields.Boolean(attribute='external_mic'),
        'isSoundEnabled': fields.Boolean(attribute='sound_enabled'),
        'waitAccelerometerData': fields.Boolean(attribute='wait_accelerometer'),
        'cameraUrl': fields.String(attribute='camera_url'),
        'useAdaptiveBitrate': fields.Boolean(attribute='adaptive_bitrate'),
    }

    @classmethod
    def serializer(cls):
        return api.model(cls.__name__, cls._serializer_model)

    def marshal(self):
        return api.marshal_with(self.__class__.serializer())(lambda x: x)(self)

    def dump_stitcher_format(self):
        data = self.marshal()
        data['soundParams'] = {}
        for attr in ('mic_name', 'isMonoSound'):
            data['soundParams'][attr] = data[attr]
            del data[attr]
        return data

    def load_stitcher_format(self, data):
        d = deepcopy(data)
        d.update(data.get('soundParams', {}))
        for attr, value in data.items():
            field = self.__class__._serializer_model.get(attr)
            if not field:
                continue
            if attr == 'streams':
                self.streams = [StitcherConfigurationStream(**v) for v in value]
            elif field.attribute:
                setattr(self, field.attribute, value)
            else:
                setattr(self, attr, value)


class Stitcher(CMSModel):

    meta = {
        'static_dir': 'stitcher',
        'strict': False,
        'collection': 'stitchers',
        'indexes': [
            'name',
        ],
        'index_cls': False,
        'index_background': True,
    }

    name = db.StringField(max_length=200, uniq=True, required=True)
    description = db.StringField()
    ec2_multiresolution = db.BooleanField(default=False)
    manual_mode = db.BooleanField(default=False)
    auto_url = db.StringField(max_length=200)
    hq_url = db.StringField(max_length=200)
    mq_url = db.StringField(max_length=200)
    lq_url = db.StringField(max_length=200)
    configuration = db.EmbeddedDocumentField(StitcherConfiguration)
    vendor = db.ReferenceField(User, reverse_delete_rule=db.NULLIFY)
    start_x = OrientationField()
    start_y = OrientationField()
    rotation_degree = db.IntField(min_value=0, max_value=360)
    coverage_degree = db.IntField(min_value=0, max_value=360)
    image_filename = db.StringField(max_length=200)

    _serializer_model = {
        'name': fields.String(description='Name'),
        'description': fields.String(description='Description'),
        'auto_url': fields.String(),
        'hq_url': fields.String(),
        'mq_url': fields.String(),
        'lq_url': fields.String(),
        'start_x': OrientationFieldSerializer(allow_null=True),
        'start_y': OrientationFieldSerializer(allow_null=True),
        'rotation_degree': fields.Integer(allow_null=True),
        'coverage_degree': fields.Integer(allow_null=True),
        'image_url': StaticUrlFieldSerializer(attribute='image_filename', required=True, description='Preview'),
    }

    def __str__(self):
        return self.name
