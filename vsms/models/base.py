from os import makedirs
from os.path import join as path_join
from random import choice
from string import ascii_letters, digits


from flask_restplus import fields
from mongoengine.base.metaclasses import TopLevelDocumentMetaclass

from vsms.misc.time import unix_time, utcnow_unix_time
from vsms.api import api
from vsms.environment import service_env as env
from vsms.models import db


class ModelMetaclass(TopLevelDocumentMetaclass):
    def __init__(cls, name, bases, nmspc):
        if cls.__name__ is not 'Model':
            for base in bases:
                cls._serializer_model.update(base._serializer_model)
                if 'type' in cls._serializer_model:
                    cls._serializer_model['type'] = fields.String(required=True,
                                                                  description='Signal name',
                                                                  default=cls.__name__)
                break
        super(ModelMetaclass, cls).__init__(name, bases, nmspc)


class Model(db.Document, metaclass=ModelMetaclass):
    meta = {         
        'allow_inheritance': True,
    }
    _serializer_model = {}

    @classmethod
    def serializer(cls):
        return api.model(cls.__name__, cls._serializer_model)

    def marshal(self):
        return api.marshal_with(self.__class__.serializer())(lambda x: x)(self)

    def __repr__(self):
        if env.debug:
            return ', '.join(['{}={}'.format(k, v.__repr__()) for k, v in self.marshal().items()])[:300]
        return super().__repr__()


class CMSModel(Model):
    meta = {         
        'allow_inheritance': True,
    }
    upload_dir = db.StringField()
    order = db.IntField(min_value=0, max_value=1000, default=0)
    available = db.BooleanField(default=True)
    
    def get_upload_path(self, force_update=False):
        if not self.upload_dir:
            self.upload_dir = path_join(self.__class__._meta.get('static_dir', ''),
                                        ''.join([choice(ascii_letters + digits) for _ in range(12)]))
            makedirs(path_join(env.service_upload_dir, self.upload_dir))
            if force_update:
                self.__class__.objects(id=self.id).update_one(set__upload_dir=self.upload_dir)
        return path_join(env.service_upload_dir, self.upload_dir)
    
    # *_filename_path
    def __getattr__(self, name):
        if not name.endswith('_filename_path'):
            raise AttributeError
        filename_attr = name[:-5]
        filename = getattr(self, filename_attr)
        if not filename:
            return filename
        return path_join(env.service_upload_dir, getattr(self, filename_attr)) 

