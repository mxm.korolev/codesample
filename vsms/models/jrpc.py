'''
JSON RPC wrappers
'''
from flask_restplus import fields
from vsms.api import api
from vsms.models import db
from vsms.models.base import Model


def JRPCCall(PayloadModel):

    class JRPCWrapper(Model):
        id = db.StringField(required=True)
        jsonrpc = db.StringField(default='2.0')
        method = db.StringField(required=True)
        params = db.ReferenceField(PayloadModel)
        _serializer_model = {
            'id': fields.String(),
            'jsonrpc': fields.String(default='2.0'),
            'method': fields.String(required=True, description='Signal name', default=PayloadModel.__name__),
            'params': fields.Nested(PayloadModel.serializer()),
        }
        __payload_class__ = PayloadModel

        def __repr__(self):
            return 'JRPCCall({})(v={},id={}): {}'.format(
                PayloadModel.__name__,
                self.jsonrpc,
                self.id,
                repr(self.params),
            )

    JRPCWrapper.__name__ = 'JRPCCall{}'.format(PayloadModel.__name__)
    return JRPCWrapper


def JRPCResponse(PayloadModel):

    class JRPRWrapper(Model):
        id = db.StringField(required=True)
        jsonrpc = db.StringField(default='2.0')
        result = db.ReferenceField(PayloadModel)
        _serializer_model = {
            'id': fields.String(),
            'jsonrpc': fields.String(default='2.0'),
            'result': fields.Nested(PayloadModel.serializer()),
        }
        __payload_class__ = PayloadModel

        def __repr__(self):
            return 'JRPCResp({})(v={},id={}): {}'.format(
                PayloadModel.__name__,
                self.jsonrpc,
                self.id,
                repr(self.result),
            )

    JRPRWrapper.__name__ = 'JRPCResponse{}'.format(PayloadModel.__name__)
    return JRPRWrapper


class JRPCErrorPayload(db.EmbeddedDocument):
    code = db.IntField(default=-32600)
    message = db.StringField(default='')


error_payload_serializer = api.model('JRPCErrorPayload', {
    'code': fields.Integer(),
    'message': fields.String(),
})


class JRPCError(Model):
    id = db.ObjectIdField(required=True)
    jsonrpc = db.StringField(default='2.0')
    error = db.EmbeddedDocumentField(JRPCErrorPayload)
    _serializer_model = {
        'id': fields.String(),
        'jsonrpc': fields.String(default='2.0'),
        'error': fields.Nested(error_payload_serializer, required=True),
    }

    def __repr__(self):
        return 'JRPCError(v={},id={}): {}({})'.format(
            self.jsonrpc,
            self.id,
            self.error.message,
            self.error.code,
        )
