from flask_restplus import fields

from vsms.models import db
from vsms.misc.collections import Enum, EnumItem
from vsms.models.base import Model, CMSModel
from vsms.models.fields import StaticUrlFieldSerializer, DateTimeFieldSerializer, IdFieldSerializer, TitleField, \
    TitleFieldSerializer, DescriptionField, DescriptionFieldSerializer, TagListField, TagListFieldSerializer, \
    FileNameField
from vsms.models.livestream import LiveStream
from vsms.models.category import Category
from vsms.models.user import User
from vsms.models.geo import GeoRule
from vsms.adapters.firebase import ShareUrl
from vsms.adapters.geo import GeoNames
from vsms.models.channel import Channel


class REPEAT_MODE(Enum):
    NONE = EnumItem(0)
    DAILY = EnumItem(1)
    WEEKLY = EnumItem(2)


class Event(CMSModel):
    class GeneratedByType(Enum):
        company = EnumItem(0)
        user = EnumItem(1)
    meta = {
        'collection': 'events',
        'strict': False,
        'static_dir': 'event',
        'indexes': [
            'available',
            'private',
            'category',
            'date',
            'end_date',
            'generated_by_tp',
            'channel',
            {
                'fields': ['$title', '$description', '$tags'],
                'default_language': 'english',
                'weights': {'title':10, 'description': 1, 'tags': 10},
            },
        ],
    }
    generated_by_tp = db.IntField(default=GeneratedByType.company, choices=list(GeneratedByType.items()))
    title = TitleField()
    date = db.DateTimeField(required=True)
    end_date = db.DateTimeField()
    description = DescriptionField()
    tags = TagListField()
    icon_filename = FileNameField()
    image_filename = FileNameField()
    livestreams = db.ListField(db.ReferenceField(LiveStream, reverse_delete_rule=db.PULL), db_field='live_streams')
    record = db.BooleanField(default=False)    # marks to record when event is active
    recording = db.BooleanField(default=False) # record in process
    category = db.ReferenceField(Category, reverse_delete_rule=db.NULLIFY)
    private = db.BooleanField(default=False)
    vendor = db.ReferenceField(User, reverse_delete_rule=db.NULLIFY)
    georule = db.ReferenceField(GeoRule, reverse_delete_rule=db.NULLIFY)
    timezone = db.StringField(choices=GeoNames().timezones, default='UTC')
    repeat = db.IntField(choices=REPEAT_MODE.__map__.items(), default=REPEAT_MODE.NONE)
    channel = db.ReferenceField(Channel, reverse_delete_rule=db.NULLIFY)
    _serializer_model = {
        'generated_by_tp': fields.Integer(example=generated_by_tp.choices),
        'id': IdFieldSerializer(),
        'title': TitleFieldSerializer(),
        'description': DescriptionFieldSerializer(),
        'tags': TagListFieldSerializer(),
        'date': DateTimeFieldSerializer(required=True, description='Date'),
        'end_date': DateTimeFieldSerializer(description='End Date'),
        'icon_url': StaticUrlFieldSerializer(attribute='icon_filename', required=True, description='Icon'),
        'image_url': StaticUrlFieldSerializer(attribute='image_filename', required=True, description='Image'),
        'livestreams': fields.List(fields.Nested(LiveStream.serializer())),
        'category': fields.Nested(Category.serializer(), descripton='Category', allow_null=True),
        'share_url': ShareUrl(attribute='id', app_field='event_id', description='Firebase share url'),
        'channel': fields.Nested(Channel.serializer(), allow_null=True)
    }

    def __str__(self):
        return '{} ({})'.format(self.title, self.date)


class GetEvent(Model):
    event = db.ReferenceField(Event)
    private = db.BooleanField(default=False)
    _serializer_model = {
        'event': fields.String,
        'private': fields.Boolean(default=False),
    }

