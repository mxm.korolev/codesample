from flask_restplus import fields

from vsms.adapters.inapp import OS
from vsms.adapters.social import NETWORK
from vsms.models import db
from vsms.models.base import Model
from vsms.models.video import Video
from vsms.models.channel import Channel


class History(Model):
    ts = db.IntField()
    video = db.LazyReferenceField(Video)
    _serializer_model = {
        'ts': fields.Integer(),
        'video': fields.Nested(Video.serializer())
    }


class Profile(Model):
    meta = {
        'collection': 'profiles',
        'strict': False,
    }
    name = db.StringField(required=True)
    social_network_id = db.IntField(required=True)
    social_client_id = db.StringField(required=True)
    token = db.StringField(required=True)
    registered = db.DateTimeField()
    email = db.StringField(required=True)
    avatar_url = db.StringField(required=True)
    video_history = db.DictField()
    private = db.BooleanField(default=False)
    streamer = db.BooleanField(default=False)
    channel = db.ReferenceField(Channel, reverse_delete_rule=db.NULLIFY)
    _serializer_model = {
        'id': fields.String(),
        'name': fields.String(description='Name'),
        'avatar_url': fields.String(description='Avatar'),
        'channel': fields.Nested(Channel.serializer(), allow_null=True),
    }


class GetProfile(Model):
    pass


class GetProfileResponse(Profile):
    _serializer_model = {
        'private': fields.Boolean(default=False)
    }


class Register(Model):
    name = db.StringField(required=True)
    social_network_id = db.IntField(choices=[(x, NETWORK.repr(x)) for x in NETWORK.list()], required=True)
    social_client_id = db.StringField(required=True)
    token = db.StringField(max_length=200)
    email = db.StringField()
    avatar_url = db.StringField()
    _serializer_model = {
        'name': fields.String(required=True, description='Name'),
        'social_network_id': fields.Integer(required=True, example=social_network_id.choices),
        'social_client_id': fields.String(required=True, description='Social Client Id'),
        'token': fields.String(required=True, description='Token'),
        'email': fields.String(description='Email'),
        'avatar_url': fields.String(description='Avatar'),
    }


class RegisterResponse(Model):
    pass


class Authorize(Model):
    social_network_id = db.IntField(choices=[(x, NETWORK.repr(x)) for x in NETWORK.list()], required=True)
    social_client_id = db.StringField(required=True)
    token = db.StringField(max_length=200)
    client_os = db.IntField(choices=[(x, OS.repr(x)) for x in OS.list()])
    client_version = db.StringField()
    iid_token = db.StringField()
    _serializer_model = {
        'social_network_id': fields.Integer(required=True, example=social_network_id.choices),
        'social_client_id': fields.String(required=True),
        'token': fields.String(required=True, description='Token'),
        'client_os': fields.Integer(example=client_os.choices),
        'client_version': fields.String(example='1.2.3'),
        'iid_token': fields.String(),
    }


class AuthorizeResponse(GetProfileResponse):
    pass


class EditProfile(Model):
    name = db.StringField(required=True)
    avatar_url = db.StringField(required=True)


class EditProfileResponse(Model):
    pass


class AcceptPromoCode(Model):
    promo_code = db.StringField()
    _serializer_model = {
        'promo_code': fields.String(),
    }


class AcceptPromoCodeResponse(Model):
    pass
