from flask_restplus import fields

from vsms.models import db
from vsms.misc.collections import EnumItem, Enum
from vsms.models.base import Model
from vsms.models.video import Video
from vsms.models.channel import Channel
from vsms.models.event import Event


class Content(Model):

    class Type(Enum):
        video = EnumItem(0)
        channel = EnumItem(1)
        event = EnumItem(2)

    tp = db.IntField(default=Type.video, choices=list(Type.items()))
    video = db.ReferenceField(Video)
    channel = db.ReferenceField(Channel)
    event = db.ReferenceField(Event)
    _serializer_model = {
        'tp': fields.Integer(example=tp.choices),
        'video': fields.Nested(Video.serializer(), allow_null=True),
        'channel': fields.Nested(Channel.serializer(), allow_null=True),
        'event': fields.Nested(Event.serializer(), allow_null=True),
    }


class GetContent(Model):
    first = db.IntField(min_value=0, default=0)
    count = db.IntField(min_value=1, default=10)
    text = db.StringField()
    channel = db.ReferenceField(Channel)
    tp = db.ListField(db.IntField(choices=list(Content.Type.items())), default=list(Content.Type.list()))
    private = db.BooleanField(default=False)
    _serializer_model = {
        'first': fields.Integer(default=0),
        'count': fields.Integer(default=10),
        'text': fields.String(),
        'channel': fields.String(),
        'tp': fields.List(fields.Integer(), example=list(Content.Type.items())),
        'private': fields.Boolean(default=False),
    }


class GetContentResponse(Model):
    total = db.IntField(default=0)
    content = db.ListField(db.ReferenceField(Content))
    _serializer_model = {
        'total': fields.Integer(),
        'content': fields.List(fields.Nested(Content.serializer())),
    }


class GetSuggestion(Model):
    text = db.StringField()
    _serializer_model = {
        'text': fields.String(),
    }


class GetSuggestionResponse(Model):
    extensions = db.ListField(db.StringField())
    collocations = db.ListField(db.StringField())
    _serializer_model = {
        'extensions': fields.List(fields.String()),
        'collocations': fields.List(fields.String()),
    }


class Suggestion(Model):
    meta = {
        'collection': 'suggestions',
        'strict': False,
        'indexes': [
            'ngrams',
            {
                'fields': ['$collocations'],
                'default_language': 'english',
                'weights': {'collocations': 1},
                'cls': False,
            },
        ],
        'index_cls': False,
        'index_background': True,
    }
    word = db.StringField()
    freq = db.IntField()
    ngrams = db.ListField(db.StringField())
    collocations = db.ListField(db.StringField())
