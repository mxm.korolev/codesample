from flask_restplus import fields

from vsms.models import db
from vsms.models.base import Model
from vsms.models.video import Video, EditVideo
from vsms.models.livestream import LiveStream
from vsms.models.event import Event
from vsms.models.category import Category
from vsms.models.fields import TitleField, TitleFieldSerializer


class UserActivityVideo(Model):
    meta = {
        'allow_inheritance': True,
    }
    media = db.LazyReferenceField(Video)
    _serializer_model = {
        'media': fields.String(attribute='media.id')
    }


class UserActivityVideoStart(UserActivityVideo):
    pass


class UserActivityVideoFinish(Model):
    pass


class UserActivityLiveStream(Model):
    meta = {
        'allow_inheritance': True,
    }
    event = db.LazyReferenceField(Event)
    media = db.LazyReferenceField(LiveStream)
    _serializer_model = {
        'event': fields.String(attribute='event.id'),
        'media': fields.String(attribute='media.id'),
    }


class UserActivityLiveStreamStart(UserActivityLiveStream):
    pass


class UserActivityLiveStreamStartResponse(Model):
    active = db.IntField(default=0)
    _serializer_model = {
        'active': fields.Integer(),
    }


class UserActivityLiveStreamFinish(Model):
    pass


class UserActivityResponse(Model):
    pass


class EventPublish(Model):
    title = TitleField()
    _serializer_model = {
        'title': TitleFieldSerializer(),
    }


class EventPublishResponse(Model):
    rtmp_url = db.StringField()
    event = db.ReferenceField(Event)
    livestream = db.ReferenceField(LiveStream)
    _serializer_model = {
        'rtmp_url': fields.String(),
        'event': fields.String(attribute='event.id'),
        'livestream': fields.String(attribute='livestream.id'),
    }


class EventDone(Model):
    title = TitleField()
    category = db.ReferenceField(Category)
    available = db.BooleanField(default=True)
    _serializer_model = {
        'title': TitleFieldSerializer(),
        'category': fields.String(attribute='category.id'),
        'available': fields.Boolean(),
    }


class EventDoneResponse(Model):
    video = db.ReferenceField(Video)
    _serializer_model = {
        'video': fields.Nested(Video.serializer())
    }
