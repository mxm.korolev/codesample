from flask_restplus import fields

from vsms.misc.collections import Enum, EnumItem
from vsms.adapters.inapp import OS
from vsms.models import db
from vsms.models.base import Model
from vsms.models.video import Video
from vsms.models.livestream import LiveStream
from vsms.models.event import Event
from vsms.models.appuser.profile import Profile


class MESSAGE_TYPE(Enum):
    TEXT = EnumItem(0)
    STAR = EnumItem(1)
    HEART = EnumItem(2)
    DONATE = EnumItem(3)
    ENTER = EnumItem(4)
    EXIT = EnumItem(5)
    AD = EnumItem(6)


class Message(Model):
    meta = {
        'allow_inheritance': True,
        'indexes': [
            'ts',
            'tp',
            'media',
        ],
        'index_cls': False,
        'index_background': True,
    }
    ts = db.IntField()
    text = db.StringField()
    amount = db.StringField()
    tp = db.IntField(default=MESSAGE_TYPE.TEXT, choices=list(MESSAGE_TYPE.items()))
    profile = db.ReferenceField(Profile, reverse_delete_rule=db.CASCADE)
    media = db.StringField()
    _serializer_model = {
        'id': fields.String(),
        'ts': fields.Integer(),
        'text': fields.String(),
        'tp': fields.Integer(example=tp.choices),
        'profile': fields.Nested(Profile.serializer(), allow_null=True),
        'media': fields.String(attribute='media.id'),
    }


class VideoMessage(Message):
    meta = {
        'collection': 'video_messages',
        'strict': False,
    }
    media = db.ReferenceField(Video, reverse_delete_rule=db.CASCADE)


class LiveStreamMessage(Message):
    meta = {
        'collection': 'livestream_messages',
        'strict': False,
    }
    event = db.ReferenceField(Event, reverse_delete_rule=db.CASCADE)
    media = db.ReferenceField(LiveStream, reverse_delete_rule=db.CASCADE)
    _serializer_model = {
        'event': fields.String(attribute='event.id'),
    }


class MessageResponse(Model):
    pass


class GetMessages(Model):
    ts = db.IntField(default=0)
    tp = db.ListField(db.IntField())
    count = db.IntField(default=100)
    _serializer_model = {
        'ts': fields.Integer(default=0),
        'tp': fields.List(fields.Integer()),
        'count': fields.Integer(default=100),
    }


class GetMessagesResponse(Model):
    messages = db.ListField(Message)
    _serializer_model = {
        'messages': fields.List(fields.Nested(Message.serializer()))
    }


class Donate(Model):
    meta = {
        'collection': 'donate',
        'strict': False,
    }
    client_os = db.IntField(choices=[(x, OS.repr(x)) for x in OS.list()])
    amount = db.StringField() # TODO: remove in future
    order_id = db.StringField()
    package_name = db.StringField()
    product_id = db.StringField()
    purchase_time = db.IntField()
    purchase_state = db.IntField()
    purchase_token = db.StringField()
    signature = db.StringField()
    event = db.LazyReferenceField(Event, required=True)
    media = db.LazyReferenceField(LiveStream, required=True)
    _serializer_model = {
        'amount': fields.String(),
        'order_id': fields.String(),
        'package_name': fields.String(),
        'product_id': fields.String(),
        'purchase_time': fields.Integer(),
        'purchase_state': fields.Integer(),
        'purchase_token': fields.String(),
        'signature': fields.String(),
        'event': fields.String(attribute='event.id'),
        'media': fields.String(attribute='media.id'),
    }


class DonateResponse(Model):
    pass
