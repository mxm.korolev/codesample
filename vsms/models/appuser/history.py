from flask_restplus import fields

from vsms.models import db
from vsms.models.fields import IdFieldSerializer
from vsms.models.base import Model
from vsms.models.video import Video


class GetVideoHistory(Model):
    first = db.IntField(min_value=0)
    count = db.IntField(min_value=10)
    _serializer_model = {
        'first': fields.Integer(default=0),
        'count': fields.Integer(default=10),
    }


class VideoHistory(Model):
    ts = db.IntField()
    video = db.ReferenceField(Video)
    _serializer_model = {
        'ts': fields.Integer(),
        'video': fields.Nested(Video.serializer())
    }


class GetHistoryResponse(Model):
    total = db.IntField(default=0)
    videos = db.ListField(VideoHistory)
    _serializer_model = {
        'total': fields.Integer(),
        'videos': fields.List(fields.Nested(VideoHistory.serializer()))
    }


class DeleteVideoHistory(Model):
    ids = db.ListField(db.StringField())
    _serializer_model = {
        'ids': fields.List(fields.String())
    }


class DeleteVideoHistoryResponse(Model):
    pass
