from flask_restplus import fields

from vsms.environment import informer
from vsms.models import db
from vsms.models.fields import IdFieldSerializer
from vsms.models.base import Model
from vsms.models.category import Category
from vsms.models.event import Event
from vsms.models.channel import Channel
from vsms.models.appuser.profile import Profile


class Subscription(Model):
    meta = {
        'collection': 'subscription',
        'strict': False,
        'indexes': [
            'profile',
            {
                'fields': ['profile', 'category', 'event', 'channel'],
                'unique': True,
            },
        ],
        'index_cls': False,
        'index_background': True,
    }

    profile = db.ReferenceField(Profile, reverse_delete_rule=db.CASCADE)
    category = db.ReferenceField(Category, reverse_delete_rule=db.CASCADE)
    event = db.ReferenceField(Event, reverse_delete_rule=db.CASCADE)
    channel = db.ReferenceField(Channel, reverse_delete_rule=db.CASCADE)

    _serializer_model = {
        'id': IdFieldSerializer(),
        'category': fields.Nested(Category.serializer(), allow_null=True),
        'event': fields.Nested(Event.serializer(), allow_null=True),
        'channel': fields.Nested(Channel.serializer(), allow_null=True),
    }

    def fcm_topic_name(self):
        if self.category:
            return str(self.category.id)
        elif self.event:
            return str(self.event.id)
        elif self.channel:
            return str(self.channel.id)
        informer.error('{} - can not construct gcm name'.format(repr(self)))
        return None


class Subscribe(Model):
    meta = {
        'allow_inheritance': True,
    }
    category = db.ReferenceField(Category)
    event = db.ReferenceField(Event)
    channel = db.ReferenceField(Channel)
    _serializer_model = {
        'category': fields.String(attribute='category.id'),
        'event': fields.String(attribute='event.id'),
        'channel': fields.String(attribute='channel.id')
    }


class SubscribeResponse(Model):
    _serializer_model = {
        'id': IdFieldSerializer()
    }


class Unsubscribe(Model):
    _serializer_model = {
        'id': IdFieldSerializer(),
    }


class UnsubscribeResponse(Model):
    pass


class GetSubscriptions(Model):
    pass


class GetSubscriptionsResponse(Model):

    subscriptions = db.ListField(Subscription)

    _serializer_model = {
        'subscriptions': fields.List(fields.Nested(Subscription.serializer()))
    }
