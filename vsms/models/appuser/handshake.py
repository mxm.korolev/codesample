from flask_restplus import fields
from vsms.adapters.inapp import OS
from vsms.models import db
from vsms.models.base import Model


class Handshake(Model):
    uid = db.StringField()
    client_os = db.IntField(choices=[(x, OS.repr(x)) for x in OS.list()], required=True)
    client_version = db.StringField()
    _serializer_model = {
        'uid': fields.String(),
        'client_os': fields.Integer(example=client_os.choices),
        'client_version': fields.String(example='1.2.3'),
    }


class HandshakeResponse(Model):
    pass
