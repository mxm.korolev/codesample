from flask_restplus import fields
from mongoengine import signals

from vsms.models import db
from vsms.models.base import CMSModel, Model
from vsms.models.fields import TitleField, TitleFieldSerializer, TagListField, TagListFieldSerializer, \
    DescriptionField, DescriptionFieldSerializer, IdFieldSerializer, ListCountFieldSerializer


class Channel(CMSModel):
    meta = {
        'collection': 'channels',
        'strict': False,
        'static_dir': 'channel',
        'indexes': [
            'available',
            'title',
            {
                'fields': ['$title', '$description', '$tags'],
                'default_language': 'english',
                'weights': {'title': 2, 'description': 1, 'tags': 2},
            },
        ],
        'index_cls': False,
        'index_background': True,
    }
    title = TitleField()
    description = DescriptionField()
    tags = TagListField()
    image_url = db.StringField(required=True)
    publishing = db.BooleanField(default=False)
    private = db.BooleanField(default=False)
    subscribers = db.IntField(default=0)
    items = db.IntField(default=0)
    _serializer_model = {
        'id': IdFieldSerializer(),
        'title': TitleFieldSerializer(),
        'description': DescriptionFieldSerializer(),
        'tags': TagListFieldSerializer(),
        'image_url': fields.String(),
        'publishing': fields.Boolean(),
        'subscribers': fields.Integer(),
        'items': fields.Integer(),
    }

    def __str__(self):
        return self.title


class EditChannel(Model):
    title = TitleField()
    description = DescriptionField()
    _serializer_model = {
        'title': TitleFieldSerializer(),
        'description': DescriptionFieldSerializer(),
    }


class EditChannelResponse(Model):
    channel = db.ReferenceField(Channel)
    _serializer_model = {
        'channel': fields.Nested(Channel.serializer(), allow_null=True),
    }


class DeleteChannel(Model):
    pass


class DeleteChannelResponse(Model):
    pass
