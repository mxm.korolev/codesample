from flask_restplus import fields
from vsms.api import api
from vsms.models import db


class Task(db.Document):

    meta = {
        'collection' : 'tasks',
        'strict': False,
    }

    task_id = db.StringField()
    type = db.StringField()
    args = db.StringField()
    traceback = db.StringField()
    date_started = db.DateTimeField()
    date = db.DateTimeField()
    result = db.StringField()
    state = db.StringField()
