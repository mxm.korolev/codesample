from vsms.models.user import Role
from vsms.views.base import CommonModelView


class GlobalConfigurationView(CommonModelView):
    can_delete = False
    access_permissions = (
        Role.dev,
    )
    column_list = (
        'android_minimal',
        'android_latest',
        'android_donation',
        'ios_minimal',
        'ios_latest',
        'ios_donation',
    )
    column_editable_list = column_list
    form_columns = column_list
