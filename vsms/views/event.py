from copy import deepcopy
from pytz import timezone
from random import choice
import string
from os.path import splitext as path_splitext
from shutil import copy
from flask import flash
from flask_admin.actions import action
from flask_admin.form.widgets import Select2Widget
from flask_admin.form.rules import FieldSet
from flask_security import current_user

from vsms.views.base import CommonModelView, EnvImageUploadField, ModelSelectMultipleQueryField, generate_filename
from vsms.models.user import Role
from vsms.models.event import Event
from vsms.models.livestream import LiveStream


class EventView(CommonModelView):
    list_template = 'admin/eventlist.html'
    access_permissions = (
        Role.content,
    )
    can_view_details = False
    column_list = (
        'title',
        'date',
        'category',
        'tags',
        'record',
        'available',
        'private',
    )
    column_editable_list = (
        'title',
        'record',
        'category',
        'available',
        'private',
    )
    form_excluded_columns = (
        'upload_dir',
    )
    column_searchable_list = (
        'title',
        'description',
    )
    column_default_sort = ('date', True)
    form_extra_fields = {
        'icon_filename': EnvImageUploadField('Icon', model=Event),
        'image_filename': EnvImageUploadField('Image', model=Event),
    }
    form_args = {
        'timezone': {
            'widget': Select2Widget()
        }
    }
    form_rules = (
        FieldSet(('timezone', 'date', 'end_date', 'repeat'), 'Time'),
        FieldSet(('title', 'category', 'channel', 'description', 'tags'), 'Description'),
        FieldSet(('icon_filename', 'image_filename'), 'Style'),
        FieldSet(('livestreams', 'georule', 'vendor', 'record','available', 'private', 'order'), 'Advanced')
    )

    def to_utc_time(self, model):
        model.date = timezone(model.timezone).localize(model.date).astimezone(timezone('UTC'))
        if model.end_date is not None:
            model.end_date = timezone(model.timezone).localize(model.end_date).astimezone(timezone('UTC'))

    def to_local_time(self, model):
        model.date = timezone('UTC').localize(model.date).astimezone(timezone(model.timezone))
        if model.end_date is not None:
            model.end_date = timezone('UTC').localize(model.end_date).astimezone(timezone(model.timezone))

    def on_model_change(self, form, model, is_created):
        self.to_utc_time(model)

    def list_form(self, obj=None):
        if obj is not None:
            self.to_local_time(obj)
        return super().list_form(obj=obj)

    def edit_form(self, obj=None):
        if obj is not None:
            self.to_local_time(obj)
        return super().edit_form(obj=obj)

    @action('copy', 'Copy')
    def action_copy(self, ids):

        for event in Event.objects(id__in=ids):
            event_copy = deepcopy(event)
            event_copy.id = None
            event_copy.title = event.title + ' (copy)'
            event_copy.icon_filename = generate_filename(path_splitext(event.icon_filename)[-1])
            copy(event.icon_filename_path, event_copy.icon_filename_path)
            event_copy.image_filename = generate_filename(path_splitext(event_copy.image_filename)[-1])
            copy(event.image_filename_path, event_copy.image_filename_path)
            event_copy.save()
            flash('"{}" copied'.format(event.title))


class EventVendorView(EventView):
    access_permissions = (
        Role.vendor,
    )
    column_list = (
        'title',
        'date',
        'category',
        'tags',
        'record',
        'available',
    )
    column_editable_list = ()
    form_excluded_columns = (
        'order',
        'upload_dir',
        'private',
        'vendor',
        'georule',
    )
    form_extra_fields = EventView.form_extra_fields
    form_extra_fields.update({
        'livestreams': ModelSelectMultipleQueryField(
            model=LiveStream,
            query_call=lambda: LiveStream.objects(vendor=current_user.id) \
                               if current_user.has_role(Role.vendor) \
                               else LiveStream.objects,
            label='Live Stream',
            allow_blank=False
        )
    })
    form_rules = (
        FieldSet(('timezone', 'date', 'end_date'), 'Time'),
        FieldSet(('title', 'category', 'description', 'tags'), 'Description'),
        FieldSet(('icon_filename', 'image_filename'), 'Style'),
        FieldSet(('livestreams', 'record', 'available'), 'Advanced'),
    )

    def get_query(self):
        if current_user.has_role(Role.vendor):
            return Event.objects(vendor=current_user.id)
        return super().get_query()

    def create_model(self, form):
        model = super().create_model(form)
        if current_user.has_role(Role.vendor):
            model.vendor = current_user.id
        return model

