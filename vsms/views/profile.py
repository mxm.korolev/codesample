from vsms.models.user import Role
from vsms.views.base import CommonModelView


class ProfileView(CommonModelView):
    access_permissions = (
        Role.content,
    )
    column_list = (
        'name',
        'email',
        'social_network_id',
    )
    column_searchable_list = (
        'name',
        'email',
    )
