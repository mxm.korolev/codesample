from flask_admin.contrib.mongoengine.ajax import QueryAjaxModelLoader
from flask_admin.actions import action
from flask_admin.form.rules import FieldSet

from vsms.models.user import Role
from vsms.models.highlight import Highlight, highlight_pre_save
from vsms.models.video import Video
from vsms.views.base import CommonModelView, EnvVideoUploadField, EnvImageUploadField


class HighlightView(CommonModelView):
    access_permissions = (
        Role.content,
    )
    column_list = (
        'title',
        'tags',
        'origin_video',
        'start',
        'available',
    )
    column_editable_list = (
        'title',
        'available',
    )
    form_excluded_columns = (
        'upload_dir',
        'hq_video_filename',
        'mq_video_filename',
        'private',
    )
    column_searchable_list = (
        'title',
    )
    form_extra_fields = {
        'full_image_filename': EnvImageUploadField('Full Image', model=Highlight),
        'image_filename': EnvImageUploadField('Image', model=Highlight),
        'hq_video_filename': EnvVideoUploadField('Video HQ'),
        'mq_video_filename': EnvVideoUploadField('Video MQ'),
    }
    form_ajax_refs = {
        'origin_video': QueryAjaxModelLoader(
            'origin_video',
            Video,
            fields=['title', 'description', 'tags']
        )
    }
    form_rules = (
        FieldSet(('title', 'tags'), 'Description'),
        FieldSet(('origin_video', 'start', 'duration', 'image_filename',
                 'full_image_filename', 'mq_video_filename', 'hq_video_filename',
                 'start_x', 'start_y'), 'Media'),
        FieldSet(('available', 'order'), 'Advanced'),
    )

    @action('reencode', 'Reencode', confirmation='Run reencoding?')
    def action_reencode(self, ids):
        for hl in Highlight.objects(id__in=ids):
            highlight_pre_save(None, hl, force_reencode=True)
            hl.save()