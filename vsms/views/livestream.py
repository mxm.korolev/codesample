from flask_admin.form.rules import FieldSet

from vsms.models.user import Role
from vsms.models.livestream import LiveStream
from vsms.views.base import CommonModelView, EnvImageUploadField


class LiveStreamView(CommonModelView):
    access_permissions = (
        Role.content,
    )
    list_template = 'admin/livestreamlist.html'
    column_list = (
        'title',
        'stitcher',
        'available',
        'private',
    )
    column_editable_list = (
        'title',
        'available',
        'private',
    )
    form_excluded_columns = (
        'upload_dir',
        'available',
        'order',
    )
    column_searchable_list = (
        'title',
    )
    form_extra_fields = {
        'image_filename': EnvImageUploadField('Image', model=LiveStream),
    }
    form_rules = (
        FieldSet(('title', 'views'), 'Description'),
        FieldSet(('image_filename',), 'Media'),
        FieldSet(('stitcher', 'upstreams', 'vendor', 'private'), 'Advanced'),
    )
