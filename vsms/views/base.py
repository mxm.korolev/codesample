import string
from os.path import join as path_join, splitext as path_splitext
from random import choice
from celery import states
from flask import url_for, redirect, request, abort, flash
from flask_admin.contrib.mongoengine import ModelView
from flask_admin.form.upload import ImageUploadField
from flask_admin.form.widgets import Select2Widget
from flask_mongoengine.wtf.fields import ModelSelectMultipleField
from flask_security import current_user
from werkzeug import secure_filename
from werkzeug.datastructures import FileStorage
from wtforms.fields import StringField
from wtforms.widgets import HTMLString, html_params

from vsms.adapters.monitoring import SystemMonitoring
from vsms.environment import service_env as env
from vsms.models.event import Event
from vsms.models.task import Task
from vsms.models.user import Role


def generate_filename(ext):
    fn = '{}{}'.format(''.join([choice(string.ascii_letters + string.digits) for _ in range(12)]), ext)
    return secure_filename(fn)


class EnvImageUploadField(ImageUploadField):
    def __init__(self, name, model=None, static_dir=None, **kwargs):
        base_path = env.service_upload_dir
        static_dir = static_dir or (model._meta['static_dir'] if model else None)

        def namegen(_, file_data):
            file_type = path_splitext(file_data.filename)[-1]
            filename = generate_filename(file_type)
            if static_dir is not None:
                return path_join(static_dir, filename)
            else:
                return file_data.filename

        kwargs.update(dict(base_path=base_path, namegen=namegen))
        super().__init__(name, **kwargs)


class ModelViewAuthMixin:

    access_permissions = ()

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role(Role.super):
            return True
        for role in type(self).access_permissions:
            if current_user.has_role(role):
                return True

        return False

    def inaccessible_callback(self, name, **kwargs):
        if not self.is_accessible():
            if current_user.is_authenticated:
                abort(403)
            else:
                return redirect(url_for('security.login', next=request.url))


class ModelViewAdditionalDataMixin:

    def render(self, template, **kwargs):
        system_monitoring = SystemMonitoring()
        if not system_monitoring.is_ok():
            flash(system_monitoring.alarm_repr(), category='error')
        if current_user.has_role(Role.dev) or current_user.has_role(Role.super):
            kwargs['system_monitoring'] = repr(system_monitoring)
        else:
            kwargs['system_monitoring'] = ''
        if not current_user.has_role(Role.vendor):
            kwargs['active_tasks'] = Task.objects(state=states.STARTED).count()
            kwargs['recording'] = Event.objects(recording=True).count()
        else:
            kwargs['active_tasks'] = None
            kwargs['recording'] = None
        return super().render(template, **kwargs)


class CommonModelView(ModelViewAuthMixin, ModelViewAdditionalDataMixin, ModelView):

    create_modal = False
    edit_modal = False
    can_view_details = True


class StreamUploadInput(object):
    empty_template = '''
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="/static/js/bootstrap.min.js"></script>
        <script>
            $(function () {
                $('#%(selector_field_name)s').change(function () {
                    var file_name = $('#%(selector_field_name)s').val();
                    if (file_name == '') {
                        alert('Please enter file name and select file');
                        return;
                    }
                    var formData = new FormData();
                    formData.append('file', $('#%(selector_field_name)s')[0].files[0]);
                    $('#%(selector_field_name)s-div-upload-progress').removeClass('hidden');
                    $('#%(selector_field_name)s').attr('disabled', 'disabled');                    
                    $('.%(selector_field_name)s-upload-progress').text('Uploading in progress...');
                    $.ajax({
                        url: '/api/upload',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        dataType: 'json',
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.%(selector_field_name)s-upload-progress').css('width', percentComplete + '%%');                                
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (data) {
                            $('.%(selector_field_name)s-upload-progress').text('Uploaded as ' + data['filename']);                            
                            $('#%(field_name)s').val(data['filename']);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert('Upload error (code=' + xhr.status + ')');
                            $('.%(selector_field_name)s-upload-progress').text('Error');
                            $('#%(selector_field_name)s').removeAttr('disabled');                            
                        }
                    });
                });
            });
        </script>
        <input type="file" accept="video/mp4,video/x-m4v,video/*" class="form-control" style="height:auto;" id="%(selector_field_name)s"/>
        <div class="progress hidden" id="%(selector_field_name)s-div-upload-progress">
            <div class="progress-bar progress-bar-success %(selector_field_name)s-upload-progress" role="progressbar" style="width:0%%"></div>
        </div>
        <input %(hiden_input)s>
    '''

    data_template = '<a href=%(video_url)s>%(value)s</a>' + empty_template

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        kwargs.setdefault('name', field.name)

        template = self.data_template if field.data else self.empty_template

        if field.errors:
            template = self.empty_template

        if field.data and isinstance(field.data, FileStorage):
            value = field.data.filename
        else:
            value = field.data or ''

        return HTMLString(template % {
            'selector_field_name': 'selector_%s' % field.name,
            'field_name': field.name,
            'value': value,
            'video_url': '{}/{}'.format(env.service_cdn_url, value),
            'hiden_input': html_params(
                type='hidden',
                value=value,
                id=field.name,
                name=field.name),
        })


class EnvVideoUploadField(StringField):
    widget = StreamUploadInput()

    def __init__(self, name, **kwargs):
        super().__init__(name, **kwargs)


class ModelSelectMultipleQueryField(ModelSelectMultipleField):
    widget = Select2Widget(multiple=True)

    def __init__(self, model=None, query_call=None, **kwargs):
        self.model = model
        self.query_call = query_call
        super().__init__(model=model, **kwargs)

    def __call__(self, *args, **kwargs):
        self.queryset = self.query_call()
        return super().__call__(*args, **kwargs)


class ImageAdjXYInput(object):
    data_template = '''
<img id="ImageAdjXYInput" src="%(img_src)s" onclick="click_event(event)" height="200" style="cursor:crosshair"></img>
Click for start position
<script>
function click_event(event) {
    var rect = document.getElementById("ImageAdjXYInput").getBoundingClientRect();
    console.log(rect.top, rect.right, rect.bottom, rect.left);
    var x = (event.clientX - rect.left) / (rect.right - rect.left);
    var y = (event.clientY - rect.top) / (rect.bottom - rect.top);
    document.getElementById("start_x").value = x;
    document.getElementById("start_y").value = y;
}
</script>'''

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        kwargs.setdefault('name', field.name)

        if field.errors:
            template = self.empty_template

        if field.data and isinstance(field.data, FileStorage):
            value = field.data.filename
        else:
            value = field.data or ''

        return HTMLString(self.data_template % {
            'img_src': '{}/{}'.format(env.service_cdn_url, value)
        })


class ImageAdjXYField(StringField):
    widget = ImageAdjXYInput()

    def __init__(self, name, **kwargs):
        super().__init__(name, **kwargs)