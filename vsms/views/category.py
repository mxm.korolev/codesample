from vsms.models.user import Role
from vsms.models.category import Category
from vsms.views.base import CommonModelView, EnvImageUploadField


class CategoryView(CommonModelView):
    access_permissions = (
        Role.content,
    )
    column_list = (
        'name',
        'description',
    )
    form_excluded_columns = (
        'upload_dir',
        'private',
        'available',
        'order',
    )
    column_searchable_list = (
        'name',
        'description',
    )
    form_extra_fields = {
        'icon_filename': EnvImageUploadField('Icon', model=Category),
        'image_filename': EnvImageUploadField('Image', model=Category),
    }
