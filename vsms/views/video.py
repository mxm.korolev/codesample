from flask_admin.actions import action
from shutil import rmtree
from flask_admin.form.rules import FieldSet

from vsms.environment import service_env as env
from vsms.views.base import CommonModelView, EnvImageUploadField, EnvVideoUploadField
from vsms.models.user import Role
from vsms.models.video import Video, video_pre_save
from vsms.misc.flask import flash


class VideoView(CommonModelView):
    access_permissions = (
        Role.content,
    )
    column_list = (
        'title',
        'date',
        'category',
        'tags',
        'video_duration',
        'available',
        'private',
    )
    column_editable_list = (
        'title',
        'category',
        'available',
        'private',
    )
    form_excluded_columns = (
        'upload_dir',
        'hq_video_filename',
        'mq_video_filename',
        'lq_video_filename',
    )
    column_searchable_list = (
        'title',
        'description',
    )
    column_default_sort = ('date', True)
    form_extra_fields = {
        'image_filename': EnvImageUploadField('Preview', model=Video),
        'full_image_filename': EnvImageUploadField('Full Image', model=Video),
        'video_filename': EnvVideoUploadField('Video')
    }
    form_rules = (
        FieldSet(('title', 'category', 'channel', 'date', 'description',  'video_duration', 'tags', 'views'), 'Description'),
        FieldSet(('image_filename', 'full_image_filename', 'video_filename'), 'Media'),
        FieldSet(('start_x', 'start_y', 'rotation_degree', 'coverage_degree'), 'Geometry'),
        FieldSet(('available', 'private', 'order'), 'Advanced'),
    )

    @action('image_reencode', 'Images Reencode', confirmation='Run?')
    def action_image_reencode(self, ids):
        for video in Video.objects(id__in=ids):
            video_pre_save(None, video, force_image_reencode=True)
            video.save()

    @action('video_reencode', 'Video Reencode', confirmation='Run?')
    def action_video_reencode(self, ids):
        for video in Video.objects(id__in=ids):
            video_pre_save(None, video, force_video_reencode=True)
            video.save()

    def after_model_delete(self, model):
        if not env.delete_outdated_files:
            return
        if not model.upload_dir:
            return
        # check other refs
        if Video.objects(upload_dir=model.upload_dir).count():
            flash('Can not delete data for {}. Other record uses {}'.format(model.id, model.upload_dir),
                  category='error')
        try:
            rmtree(model.get_upload_path())
        except OSError as e:
            flash('Can not delete data for {}. {}'.format(model.id, e),
                  category='error')
