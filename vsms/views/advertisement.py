from flask_admin.form.rules import FieldSet

from vsms.models.user import Role
from vsms.views.base import CommonModelView, EnvVideoUploadField


class AdView(CommonModelView):
    access_permissions = (
        Role.content,
    )
    column_list = (
        'title',
        'hq_video_filename',
    )
    column_editable_list = (
        'title',
        'available',
    )
    form_excluded_columns = (
        'upload_dir',
        'mq_video_filename',
        'lq_video_filename'
        'private',
    )
    column_searchable_list = (
        'title',
    )
    form_extra_fields = {
        'hq_video_filename': EnvVideoUploadField('Video HQ'),
    }
    form_rules = (
        FieldSet(('title', ), 'Description'),
        FieldSet(('hq_video_filename', 'start_x', 'start_y'), 'Media'),
        FieldSet(('available', 'order'), 'Advanced'),
    )
