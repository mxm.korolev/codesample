from flask_admin.form.widgets import Select2Widget

from vsms.views.base import CommonModelView
from vsms.models.user import Role


class GeoRuleView(CommonModelView):
    access_permissions = (
        Role.content,
    )
    column_list = (
        'title',
        'continents',
        'countries',
        'allow_policy',
    )
    column_searchable_list = (
        'title',
    )
    form_args = {
        'continents': {
            'widget': Select2Widget(multiple=True)
        },
        'countries': {
            'widget': Select2Widget(multiple=True)
        },
    }
