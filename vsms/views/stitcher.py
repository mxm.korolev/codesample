from os.path import join as path_join
from functools import partial
from flask import request, abort, flash, session
from flask_admin.model.helpers import get_mdict_item_or_list
from flask_admin.base import expose
from flask_admin.actions import action
from mongoengine.queryset import DoesNotExist
from tornado.gen import IOLoop, coroutine

from vsms.environment import service_env as env, informer
from vsms.misc.collections import recursive_update
from vsms.misc.files import random_filename
from vsms.misc.flask import flash
from vsms.models.user import Role
from vsms.models.stitcher import Stitcher, StitcherConfiguration
from vsms.netcontrol.root import RootController
from vsms.adapters.kibana import Kibana
from vsms.adapters.elastic import ElasticSearch
from vsms.adapters.rabbitmq import online_idents
from vsms.adapters.ffmpeg import FFMPEG
from vsms.views.base import CommonModelView, ImageAdjXYField
from vsms.business.stitcher import online_stitchers, update_stitcher


class StitcherView(CommonModelView):
    access_permissions = (
        Role.content,
    )
    list_template = 'admin/stitcherlist.html'
    details_template = 'admin/stitcherdetails.html'
    edit_template = 'admin/stitcheredit.html'
    column_list = (
        'name',
        'description',
        'ec2_multiresolution',
    )
    form_excluded_columns = (
        'upload_dir',
        'private',
        'available',
        'order',
    )
    form_extra_fields = {
        'image_filename': ImageAdjXYField('Preview'),
    }

    @expose('/')
    def index_view(self):

        es = ElasticSearch()
        session.stitcher_names = []
        for stitcher_name in online_idents():
            try:
                stitcher = Stitcher.objects.get(name=stitcher_name)
            except DoesNotExist:
                if request.args.get('discover'):
                    stitcher = Stitcher(name=stitcher_name)
                    stitcher.configuration = StitcherConfiguration()
                    if update_stitcher(stitcher, es=es):
                        stitcher.save()
                        session.stitcher_names.append(stitcher.name)
                        flash('{} was found'.format(stitcher.name), category='info')
            else:
                if not update_stitcher(stitcher, es=es):
                    flash('Can not update {}'.format(stitcher.name))
                session.stitcher_names.append(stitcher.name)

        self.kibana_report_url = Kibana().get_dashboard_url(dashboard_name='overviewdashboard',
                                                            time_from='now-1d', interval='10m')
        return super().index_view()

    @expose('/details/')
    def details_view(self):

        id = get_mdict_item_or_list(request.args, 'id')
        if id is None:
            abort(400)
        try:
            stitcher = Stitcher.objects.get(id=id)
        except DoesNotExist:
            abort(404)

        self.kibana_report_url = Kibana().get_dashboard_url(dashboard_name='stitcherdashboard',
                                                            query='ident:"{}"'.format(stitcher.name))

        return super().details_view()

    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self):

        id = get_mdict_item_or_list(request.args, 'id')
        if id is None:
            abort(400)
        try:
            stitcher = Stitcher.objects.get(id=id)
        except DoesNotExist:
            abort(404)

        if not update_stitcher(stitcher):
            flash('Can not update {}'.format(stitcher.name))

        return super().edit_view()

    def on_model_change(self, form, model, is_created):

        if is_created:
            return

        old_model = Stitcher.objects.get(id=model.id)
        if old_model.name == model.name and old_model.configuration == model.configuration:
            return

        if old_model.name not in online_idents():
                raise Exception('Can not perform {} update. Offline.'.format(old_model.name))

        if old_model.configuration != model.configuration:

            @coroutine
            def _stitcher_update_config(name, config):
                response = yield RootController().rpc_stitcher_get_config(name)
                if not response:
                    informer.error('RPC: Can not perform {} update.'.format(name))
                    return
                stitcher_config = response.get('done')
                if not stitcher_config:
                    informer.error('RPC: {} bad response: {}.'.format(name, response))
                    return
                recursive_update(stitcher_config, config)
                RootController().rpc_stitcher_push_config(name, stitcher_config)
                informer.info('{} configuration updated'.format(name))

            IOLoop.instance().add_callback(
                partial(_stitcher_update_config, old_model.name, model.configuration.dump_stitcher_format()))
            flash('{} configuration updated'.format(old_model.name))

            if old_model.name != model.name:
                IOLoop.instance().add_callback(partial(RootController().rpc_rename, old_model.name, model.name))
                flash('{} renamed to {}'.format(old_model.name, model.name))

    @action('restart', 'Restart', confirmation='Restart?')
    def action_restart(self, ids):

        idents = online_idents()
        for stitcher in Stitcher.objects(id__in=ids):
            if stitcher.name not in idents:
                flash('"{}" offline.'.format(stitcher.name), category='error')
                continue
            IOLoop.instance().add_callback(partial(RootController().rpc_stitcher_restart, stitcher.name))
            flash('"{}" restarted.'.format(stitcher.name))

    @action('get_image', 'Get Image')
    def action_get_image(self, ids):
        for stitcher in Stitcher.objects(id__in=ids):
            stitcher.get_upload_path()
            stitcher.image_filename = path_join(stitcher.upload_dir, random_filename('jpeg'))
            ret = FFMPEG(raise_exception=False).make_preview(stitcher.auto_url, stitcher.image_filename_path)
            if ret is None:
                flash('Can not make preview for {}'.format(stitcher.name), category='error')
            else:
                stitcher.save()
