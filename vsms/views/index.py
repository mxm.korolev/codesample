import datetime
from flask_admin.base import AdminIndexView, expose
from flask_security import current_user
from flask import redirect

from vsms.environment import service_env as env, informer
from vsms.models.user import Role
from vsms.business.report import BigQuieryStatistics
from vsms.views.base import ModelViewAuthMixin, ModelViewAdditionalDataMixin


class HomeView(ModelViewAuthMixin, ModelViewAdditionalDataMixin, AdminIndexView):
    access_permissions = (
        Role.content,
        Role.dev,
        Role.vendor,
    )

    @expose()
    def index(self):
        if not self.is_accessible():
            return self.inaccessible_callback('')

        if current_user.has_role(Role.vendor):
            # return self.render('admin/index_vendor.html')
            return redirect('/admin/event2', code=302)
        else:
            date = datetime.date.today() - datetime.timedelta(days=1)

            stat = BigQuieryStatistics(
                env.bigquery_service_account_json_fn, {
                    'ios': env.bigquery_ios_dataset,
                    'android': env.bigquery_android_dataset,
                }
            )
            try:
                statistics = stat.analyse(date)
            except Exception as e:
                statistics = {}
                stat_render = False
                informer.error('Can not make report: {}'.format(e))
            else:
                stat_render = True
            return self.render('admin/index.html', stat_render=stat_render, **statistics)