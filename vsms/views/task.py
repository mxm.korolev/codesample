from flask_admin.actions import action
from celery import states

from vsms.views.base import CommonModelView
from vsms.models.user import Role
from vsms.models.task import Task
from vsms.adapters.celery import celery


class TaskView(CommonModelView):
    list_template = 'admin/tasklist.html'
    can_create = False
    can_delete = False
    can_edit = False
    access_permissions = (
        Role.dev,
    )
    column_list = (
        'date',
        'type',
        'state',
        'args',
    )
    column_searchable_list = (
        'type',
        'args',
        'state',
    )
    column_default_sort = ('date', True)

    @action('revoke', 'Revoke', confirmation='Revoke?')
    def action_restart(self, ids):

        for task in Task.objects(id__in=ids):
            celery.control.revoke(task.task_id, terminate=True)
            Task.objects(id=task.id).update_one(
                set__state=states.REVOKED
            )
