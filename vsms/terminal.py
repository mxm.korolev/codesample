'''
Tools for console control
'''

from vsms.environment import service_env as env
env.initialize(command_interface=True)
from vsms.wsgi import WSGIApplication
from vsms.adapters.celery import setup_flask_celery
from vsms.business.terminal import rtmp_stream_upstream_on_publish_multiple, \
    rtmp_stream_record_on_publish, rtmp_stream_make_hls_on_publish, rtmp_stream_make_hls_on_done

PUBLISH_CMD = 'publish'
PUBLISH_DONE_CMD = 'publish_done'


def main():

    app = WSGIApplication()
    setup_flask_celery(app)

    if env.command == PUBLISH_CMD:
        application, name = env.command_args
        rtmp_stream_record_on_publish(application, name)
        rtmp_stream_upstream_on_publish_multiple(application=application, name=name)
        rtmp_stream_make_hls_on_publish.apply_async(
            kwargs=dict(
                application=application,
                name=name
            ),
            countdown=5
        )

    elif env.command == PUBLISH_DONE_CMD:
        application, name = env.command_args
        rtmp_stream_make_hls_on_done(application, name)

    else:
        raise Exception('Unknown command')

if __name__ == '__main__':
    main()

