from flask_restplus import Resource
from flask import request, abort
from mongoengine.errors import FieldDoesNotExist
from mongoengine.queryset import DoesNotExist
from werkzeug.exceptions import BadRequest, NotFound, InternalServerError

from vsms.api import api
from vsms.environment import service_env as env, informer


def ResourceFactory(Input, Output):

    class RestResource(Resource):

        def post_route(self, input):
            raise NotImplementedError()

        @api.expect(Input.serializer(), validate=True)
        @api.marshal_with(Output.serializer())
        def post(self):

            payload = request.get_json()
            try:
                input = Input(**payload)
                if env.debug:
                    informer.info('{} Q: {}'.format(self.__class__.__name__, repr(input)))
            except FieldDoesNotExist:
                abort(BadRequest.code, BadRequest.description)
            except DoesNotExist:
                abort(NotFound.code, 'Object not found')

            try:
                output = self.post_route(input)
            except DoesNotExist:
                abort(NotFound.code, 'Object not found')
            except Exception as e:
                if env.debug:
                    raise e
                abort(InternalServerError, e)

            if env.debug:
                informer.info('{} R: {}'.format(self.__class__.__name__, repr(output)))

            return output

    RestResource.__name__ = '{}{}Resource'.format(Input.__name__, Output.__name__)
    return RestResource
