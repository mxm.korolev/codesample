from flask_restplus import Resource
from vsms.api import api
from vsms.models.advertisement import Advertisement, advertisement_serializer
from mongoengine.queryset import DoesNotExist

ns = api.namespace('advertisement', description='Advertisement')

@ns.route('/')
class AdvertisementCollection(Resource):

    @api.marshal_with(advertisement_serializer)
    def get(self):
        """
        Get advertisement.
        """
        try:
            ad = Advertisement.objects.get()
        except DoesNotExist:
            ad = None
            
        return ad