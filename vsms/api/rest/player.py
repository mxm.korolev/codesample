'''
    360 video player
'''

from tornado.web import stream_request_body, RequestHandler

class PlayerHandler(RequestHandler):

    def get(self, *args, **kwargs):
        source_hls = self.get_argument('source_hls', default=None)
        if not source_hls:
            self.set_status(404, 'No source_hls passed')
            return

        self.render('player/vrview.html', source_hls=source_hls)
