# TODO: legacy methods


import re
from flask import abort
from mongoengine.errors import ValidationError
from mongoengine.queryset.visitor import Q

from vsms.api import api
from vsms.api.rest import ResourceFactory
from vsms.models.video import Video, GetVideo, GetVideoResponse, GetRelatedVideo, \
    GetSuggestion, GetSuggestionResponse, VideoSuggestion

ns = api.namespace('video', description='Video Export')


@ns.route('')
class VideoCollection(ResourceFactory(GetVideo, GetVideoResponse)):

    def post_route(self, query):
        '''
        :return: Video list
        '''
        response = GetVideoResponse()

        if query.video:
            if query.video.available and (not query.video.private or query.private):
                response.total = 1
                response.videos = [query.video, ]
            return response

        q = Q(available=True)
        if not query.private:
            q &= Q(private=False)
        if query.categories:
            q &= Q(category__in=query.categories)
        if query.tags:
            q &= Q(tags__in=query.tags)

        if query.text:
            videos = Video.objects.search_text(query.text).filter(q).order_by('$text_score')
        else:
            videos = Video.objects(q).order_by('-date')

        response.total = videos.count()
        try:
            response.videos = videos[query.first:query.first + query.count]
        except ValidationError:
            abort(400, 'Bad query payload')
        return response


@ns.route('/related')
class RelatedVideoCollection(ResourceFactory(GetRelatedVideo, GetVideoResponse)):

    def post_route(self, query):
        '''
        :return: Related video list
        '''

        response = GetVideoResponse()

        q = Q(available=True) & Q(private=False)
        if query.event:
            search_text = ' '.join((query.event.title,) + tuple(query.event.tags))
        elif query.video:
            search_text = ' '.join((query.video.title,) + tuple(query.video.tags))
            q &= Q(id__ne=query.video.id)
        else:
            abort(400, 'Bad query payload')

        videos = Video.objects.search_text(search_text).filter(q).order_by('$text_score')
        response.total = videos.count()
        try:
            response.videos = videos[query.first:query.first + query.count]
        except ValidationError:
            abort(400, 'Bad query payload')
        return response


@ns.route('/suggestion')
class SuggestionCollection(ResourceFactory(GetSuggestion, GetSuggestionResponse)):

    def post_route(self, query):
        suggestion = GetSuggestionResponse()
        ws = [w.lower() for w in re.split('\W+', query.text) if w]
        last_w = ws[-1]
        prev_w = ' '.join(ws[:-1])
        for s in VideoSuggestion.objects.search_text(last_w).order_by('-freq'):
            suggestion.collocations.append(' '.join((prev_w, last_w, s.word)).lstrip())
        if query.text[-1].isalpha() or query.text[-1].isdigit():
            for s in VideoSuggestion.objects(ngrams=last_w):
                suggestion.extensions.append(' '.join((prev_w, s.word)).lstrip())
        return suggestion
