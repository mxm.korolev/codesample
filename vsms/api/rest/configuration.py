from flask_restplus import Resource
from mongoengine.queryset import DoesNotExist

from vsms.api import api
from vsms.models.configuration import GlobalConfiguration

ns = api.namespace('globalconfiguration', description='Operations related service configuration')


@ns.route('/')
class GlobalConfigurationCollection(Resource):

    @api.marshal_with(GlobalConfiguration.serializer())
    def get(self):
        """
        Get service configuration.
        """
        try:
            configuration = GlobalConfiguration.objects.get()
        except DoesNotExist:
            configuration = None
            
        return configuration

