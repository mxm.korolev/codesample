import re
from datetime import datetime
from mongoengine.queryset.visitor import Q

from vsms.api import api
from vsms.api.rest import ResourceFactory
from vsms.models.appuser.content import Content, GetContent, GetContentResponse, \
    Suggestion, GetSuggestion, GetSuggestionResponse
from vsms.models.channel import Channel
from vsms.models.video import Video
from vsms.models.event import Event

ns = api.namespace('content', description='Content search API')


@ns.route('')
class ContentCollection(ResourceFactory(GetContent, GetContentResponse)):

    def post_route(self, get_content):

        q = Q(available=True)
        if not get_content.private:
            q &= Q(private=False)
        if get_content.channel:
            try:
                get_content.tp.remove(Content.Type.channel)
            except ValueError:
                pass
            q &= Q(channel=get_content.channel)

        resp = GetContentResponse()
        channels, videos, events = [], [], []
        event_q = Q(end_date__not__lt=datetime.utcnow())
        q_limit = 20

        def base_sort_order(c):
            if isinstance(c, Channel):
                r = c.items * 10 + c.subscribers
            elif isinstance(c, Video):
                r = c.views // 10
            elif isinstance(c, Event):
                r = 100
            else:
                r = 1
            return r

        if get_content.text:
            if Content.Type.channel in get_content.tp:
                channels = Channel.objects.search_text(get_content.text).filter(q).order_by('$text_score').limit(q_limit)
            if Content.Type.video in get_content.tp:
                videos = Video.objects.search_text(get_content.text).filter(q).order_by('$text_score').limit(q_limit)
            if Content.Type.event in get_content.tp:
                events = Event.objects.search_text(get_content.text).filter(q & event_q).order_by('$text_score').limit(q_limit)

            def sort_order(c):
                return c._data['_text_score'] * base_sort_order(c) ** 0.1

        else:
            if Content.Type.channel in get_content.tp:
                channels = Channel.objects(q).order_by('-subscribers').limit(q_limit)
            if Content.Type.video in get_content.tp:
                videos = Video.objects(q).order_by('-date').limit(q_limit)
            if Content.Type.event in get_content.tp:
                events = Event.objects(q & event_q).order_by('-date').limit(q_limit)
            sort_order = base_sort_order

        aggregated = sorted(list(events) + list(channels) + list(videos), key=sort_order, reverse=True)
        resp.total = len(aggregated)
        for c in aggregated[get_content.first:get_content.first + get_content.count]:
            if isinstance(c, Channel):
                resp.content.append(Content(tp=Content.Type.channel, channel=c))
            elif isinstance(c, Video):
                resp.content.append(Content(tp=Content.Type.video, video=c))
            elif isinstance(c, Event):
                resp.content.append(Content(tp=Content.Type.event, event=c))

        return resp


@ns.route('/suggestion')
class SuggestionCollection(ResourceFactory(GetSuggestion, GetSuggestionResponse)):

    def post_route(self, query):
        suggestion = GetSuggestionResponse()
        ws = [w.lower() for w in re.split('\W+', query.text) if w]
        last_w = ws[-1]
        prev_w = ' '.join(ws[:-1])
        for s in Suggestion.objects.search_text(last_w).order_by('-freq'):
            suggestion.collocations.append(' '.join((prev_w, last_w, s.word)).lstrip())
        if query.text[-1].isalpha() or query.text[-1].isdigit():
            for s in Suggestion.objects(ngrams=last_w).order_by('-freq'):
                suggestion.extensions.append(' '.join((prev_w, s.word)).lstrip())
        return suggestion
