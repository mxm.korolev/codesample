from flask_restplus import Resource
from mongoengine.queryset import DoesNotExist

from vsms.api import api
from vsms.models.category import Category

ns = api.namespace('category', description='Category Export')

@ns.route('')
class CategoryCollection(Resource):

    @api.marshal_list_with(Category.serializer())
    def get(self):
        '''
        :return: Category list
        '''

        try:
            categories = Category.objects
        except DoesNotExist:
            return []

        return list(categories)