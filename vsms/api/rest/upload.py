import re
import json
from os.path import join as path_join
from tornado.web import stream_request_body, RequestHandler
from tornado.gen import sleep

from vsms.environment import service_env as env, informer
from vsms.misc.files import random_filename


@stream_request_body
class StreamUploadHandler(RequestHandler):

    def prepare(self):
        self.data_rcvd = False
        self.prepared = True

    def data_received(self, data):
        if not self.prepared:
            return

        if not self.data_rcvd:
            self.data_rcvd = True
            match = re.search(b'^([^\r\n]+)\r\n(.*)\r\n\r\n(.+)$', data, re.MULTILINE | re.DOTALL)
            if not match:
                self.prepared = False
                return
            _, headers, data = match.groups()

            match = re.search(b'filename=".*\.(\w+)"', headers)
            if not match:
                self.prepared = False
                return
            file_type = match.group(1).decode()
            self.file_name = random_filename(file_type)
            self.upload_file_name = path_join(env.service_upload_dir, self.file_name)
            self.fd = open(self.upload_file_name, 'wb')

        self.fd.write(data)
        return sleep(0.5)

    def post(self, *args, **kwargs):

        if not self.prepared:
            self.set_status(400, 'Bad query')
            return
        if not self.data_rcvd:
            self.set_status(400, 'Empty body')
            return

        self.fd.close()
        informer.info('Uploaded {}'.format(self.upload_file_name))
        
        resp = {'filename': self.file_name}
        self.write(json.dumps(resp))
        self.finish()
        
        
