import datetime
from mongoengine.queryset.visitor import Q
from mongoengine.queryset import DoesNotExist

from vsms.api import api
from vsms.api.rest import ResourceFactory
from vsms.models.event import Event, GetEvent

ns = api.namespace('event', description='Event Export')


@ns.route('')
class EventCollection(ResourceFactory(GetEvent, Event)):

    def post_route(self, query):
        '''
        :return: Event list
        '''
        events = []
        try:
            query.event
        except DoesNotExist:
            return events

        if query.event:
            if query.event.available and (not query.event.private or query.private):
                events = [query.event, ]
        else:
            q = Q(available=True) & Q(end_date__not__lt=datetime.datetime.utcnow()) \
                & Q(generated_by_tp__ne=Event.GeneratedByType.user)
            if not query.private:
                q &= Q(private=False)
            events = Event.objects(q).select_related()
        if not query.private:
            for e in events:
                e.livestreams = [ls for ls in e.livestreams if not ls.private]
        return events
