from flask_restplus import Api
import vsms

api = Api(version=vsms.__version__, title='VSMS API',
          description='v{}'.format(vsms.__version__))

