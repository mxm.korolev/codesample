import json
from tornado.websocket import WebSocketHandler, WebSocketClosedError
from flask_restplus import Resource
from werkzeug.exceptions import HTTPException, BadRequest, MethodNotAllowed, Forbidden, NotFound, InternalServerError, \
    Conflict
from mongoengine.queryset import DoesNotExist
from mongoengine.errors import NotUniqueError
from pymongo.errors import DuplicateKeyError
from datetime import datetime

from vsms.api import api
from vsms.environment import informer, service_env as env
from vsms.netcontrol.controller import BaseAMQPController
from vsms.models.jrpc import JRPCCall, JRPCResponse, JRPCError, JRPCErrorPayload

ns = api.namespace('ws', description='WebSocket JRPC')


class GroupExchange(BaseAMQPController):

    def initialize(self):
        super().initialize(env.name, env, exchange_name='GroupExchange')
        self.add_consumer(self._consume)

    def _consume(self, ch, method, props, body):
        try:
            payload = json.loads(body.decode())
        except json.decoder.JSONDecodeError:
            self.error('Can not decode message body')
            return
        try:
            message = payload['message']
            group = payload['group_name']
        except KeyError:
            informer.error('Bad GroupExchange format: {}'.format(body))
            return
        WSHandler._group_broadcast(group, message)

    def publish(self, group_name, message):
        group_name = str(group_name)
        payload = dict(message=message, group_name=group_name)
        super().publish(payload)


class WSHandler(WebSocketHandler):

    PERM_ANY = 0
    PERM_NOTAUTHORIZED = 1
    PERM_GUEST = 2
    PERM_AUTHORIZED = 4

    callbacks_on_open = dict()
    callbacks_on_close = dict()
    callbacks = dict()

    group_exchange = GroupExchange()
    client_groups = dict()

    @classmethod
    def register_on_open(cls, signal):
        def wrapper(callback):
            cls.callbacks_on_open[signal.__name__] = callback
            return callback
        return wrapper

    @classmethod
    def register_on_close(cls, signal):
        def wrapper(callback):
            cls.callbacks_on_close[signal.__name__] = callback
            return callback
        return wrapper

    @classmethod
    def register_callback(cls, call_type, response_type, permissions=PERM_ANY):

        # swagger documentation generation
        @ns.route('/{}'.format(call_type.__name__),
                  doc={'description': 'Permissions: {:03b} [Authorized|Guest|NotAuthorized]'.format(permissions)})
        class SwaggerInterface(Resource):
            @api.expect(JRPCCall(call_type).serializer())
            @api.marshal_with(JRPCResponse(response_type).serializer())
            def post(self):
                pass

        def wrapper(callback):
            cls.callbacks[call_type.__name__] = (callback, call_type, response_type, permissions)
            return callback
        return wrapper

    def check_origin(self, origin):
        # Disabled for apple IPv6 proxy support
        return True
        return super().check_origin(origin) or self.request.headers.get('Host', '').split(':')[0] == '127.0.0.1'
    
    def open(self, **kwargs):
        self.permissions = WSHandler.PERM_NOTAUTHORIZED
        for c in self.__class__.callbacks_on_open.values():
            c(self)

    def on_close(self):
        for c in self.__class__.callbacks_on_close.values():
            c(self)

    @classmethod
    def parse_json(cls, json_message):
        try:
            message = json.loads(json_message)
            call_id = message['id']
        except (json.decoder.JSONDecodeError, KeyError) as e:
            raise BadRequest('Malformed JSON: {}'.format(e))
        return message, call_id

    @classmethod
    def parse_model(cls, message):
        method = message.get('method')
        if not method in cls.callbacks:
            raise MethodNotAllowed('Unknown method "{}"'.format(method))
        callback, call_type, response_type, permissions  = cls.callbacks[method]
        if 'params' in message:
            try:
                params = call_type(**message.pop('params'))
            except Exception as e:
                raise BadRequest('Params validation error: {}'.format(e))
        else:
            params = call_type()
        try:
            jrpc_call = JRPCCall(call_type)(params=params, **message)
        except Exception as e:
            raise BadRequest('Validation error: {}'.format(e))
        return callback, call_type, response_type, permissions, jrpc_call

    def on_message(self, json_message):
        self.now = datetime.utcnow()
        cls = self.__class__
        call_id = None
        try:
            message, call_id = cls.parse_json(json_message)
            callback, call_type, response_type, permissions, jrpc_call = cls.parse_model(message)
            if env.debug:
                informer.info('WSQ {}'.format(repr(jrpc_call)))
            if permissions != cls.PERM_ANY and not self.permissions & permissions:
                raise Forbidden('Method not allowed')
            try:
                response_payload = callback(self, jrpc_call.params)
            except (DoesNotExist, KeyError) as e:
                raise NotFound(e)
            except (NotUniqueError, DuplicateKeyError) as e:
                raise Conflict(e)
            response = JRPCResponse(response_type)()
            response.result = response_payload

        except Exception as e:
            response = JRPCError()
            response.error = JRPCErrorPayload()
            response.error.message = str(e)
            if isinstance(e, HTTPException):
                response.error.code = -32000 - e.code
            else:
                if env.debug:
                    raise e
                response.error.code = -32000 - InternalServerError.code

        response.id = call_id
        response_marshal = response.marshal()
        self.write_message(json.dumps(response_marshal))
        if env.debug:
            d = datetime.utcnow() - self.now
            informer.info('WSR {:.2f}ms {}'.format(d.seconds * 1000 + d.microseconds // 1000, repr(response)))

    def group_add(self, group_name):
        group_name = str(group_name)
        if group_name not in self.__class__.client_groups:
            WSHandler.client_groups[group_name] = set()
        WSHandler.client_groups[group_name].add(self)

    def group_remove(self, group_name):
        group_name = str(group_name)
        if group_name not in self.__class__.client_groups:
            return
        try:
            WSHandler.client_groups[group_name].remove(self)
        except KeyError:
            pass

    @classmethod
    def _group_broadcast(cls, group_name, message):
        group_name = str(group_name)
        if group_name not in cls.client_groups:
            return
        for h in cls.client_groups[group_name]:
            try:
                h.write_message(message)
            except WebSocketClosedError:
                h.group_remove(group_name)

    @classmethod
    def group_connect(cls):
        return WSHandler.group_exchange.connect()

    @classmethod
    def group_publish(cls, group_name, message):
        WSHandler.group_exchange.publish(group_name, message)
