from vsms.api import api
from vsms.models.push import PushNotification
from flask_restplus import Resource

ns = api.namespace('push', description='Push Notification')


@ns.route('/{}'.format(PushNotification.__name__))
class PushInterface(Resource):
    @api.marshal_with(PushNotification.serializer())
    def post(self):
        pass
