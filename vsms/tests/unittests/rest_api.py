from unittest import TestCase
import json
from werkzeug.datastructures import Headers

from vsms.environment import informer
from vsms.misc.time import utcnow_unix_time
from vsms.wsgi import WSGIApplication
from vsms.tests.unittests import fill_db, clear_db


class FlaskTestCase(TestCase):

    def setUp(self):
        super().setUp()
        clear_db()
        fill_db()
        self.app = WSGIApplication()
        self.client = self.app.test_client()

    def tearDown(self):
        clear_db()
        super().tearDown()

    def query(self, path, method=None, payload=None, wait_for_success=True):
        if method == 'GET':
            method = self.client.get
        elif method == 'POST':
            method = self.client.post
        elif method is None and payload is None:
            method = self.client.get
        elif method is None and payload is not None:
            method = self.client.post
        payload = payload or {}

        headers = Headers()
        headers.add('Accept', 'application/json')
        headers.add('Content-Type', 'application/json')

        resp = method(path, headers=headers, data=json.dumps(payload))
        self.assertEqual(resp.status_code == 200, wait_for_success, resp.status_code)
        if wait_for_success:
            return json.loads(resp.data.decode())
        return None

    def test_post_events(self):
        events = self.query('/api/event', method='POST')
        self.assertEqual(len(events), 2)

        events = self.query('/api/event', payload={'event': events[0]['id']})
        self.assertEqual(len(events), 1)

    def test_post_videos(self):
        resp = self.query('/api/video', method='POST')
        self.assertEqual(resp['total'], 3)
        self.assertEqual(len(resp['videos']), 3)

        resp = self.query('/api/video', payload={'count': 1})
        self.assertEqual(len(resp['videos']), 1)

        resp = self.query('/api/video', payload={'text': 'rAce'})
        self.assertEqual(resp['total'], 2)

        resp = self.query('/api/video', payload={'text': 'racE car'})
        self.assertEqual(resp['total'], 2)
        self.assertEqual(resp['videos'][0]['tags'][0], 'video11')

        resp = self.query('/api/video', payload={'text': 'beauty guitar'})
        self.assertEqual(resp['total'], 3)
        self.assertEqual(resp['videos'][0]['tags'][0], 'video21')

        resp = self.query('/api/video', payload={'text': 'rAcE2017'})
        self.assertEqual(resp['total'], 0)

        resp = self.query('/api/video', payload={'text': 'races 2017'})
        self.assertEqual(resp['total'], 2)

        resp = self.query('/api/video', payload={'text': 'ra'})
        self.assertEqual(resp['total'], 2)

        resp = self.query('/api/video', payload={'text': 'rac'})
        self.assertEqual(resp['total'], 2)

        resp = self.query('/api/video', payload={'text': 'song'})
        self.assertEqual(resp['total'], 3)
        self.assertEqual(resp['videos'][0]['tags'][0], 'video21')

        resp = self.query('/api/video', payload={'text': 'cars'})
        self.assertEqual(resp['total'], 2)
        self.assertEqual(resp['videos'][0]['tags'][0], 'video11')

        resp = self.query('/api/video', payload={'text': 'cars300'})
        self.assertEqual(resp['total'], 1)

    def test_post_videos_related(self):
        resp = self.query('/api/video/related', method='POST', wait_for_success=False)

        resp = self.query('/api/video', payload={'text': 'cars300'})
        self.assertEqual(resp['total'], 1)

        video_id = resp['videos'][0]['id']
        resp = self.query('/api/video/related', payload={'video': video_id})
        self.assertEqual(resp['total'], 2)
        self.assertEqual(resp['videos'][0]['tags'][0], 'video01')

    def test_globalconfiguration(self):
        resp = self.query('/api/globalconfiguration/', method='GET')
        self.assertLess(utcnow_unix_time() - resp['utc'], 1000)

