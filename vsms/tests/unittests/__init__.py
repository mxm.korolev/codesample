import datetime

from flask import Flask

from vsms.environment import service_env as env

# mongomock://localhost

env.initialize(
    redefine_tornado_logging=True,
    config_data='''
[GENERAL]
name = VSMS-UNITTEST
debug = True
log = /dev/stdout
threads = 1 
timeout = 10
unittest = True

[NETWORK]
host = 0.0.0.0
port = 8889

[MAIL]
enabled = False
smtpserver = smtp.googlemail.com
fromaddr = info@splinex.com
password = splinex271813
toaddrs = mkorolev@splinex.com

[DB]
host = localhost
port = 27017
collection = vsms-unittest

[SERVICE]
upload_dir = /tmp
temp_upload_dir = /tmp
cdn_url = http://192.168.2.100:8889/static
private_key = N0TFBFT4Q750R7NW

[NGINX]
stat_url = http://vsms.splinex-team.com/stat
rtmp_server_local_url = rtmp://vsms.splinex-team.com
rtmp_server_url = rtmp://vsms.splinex-team.com
rtmp_multiresolution_server_url = rtmp://encoder.splinex-team.com,rtmp://encoder.splinex-team.com
hls_server_url = http://vsms.splinex-team.com/hls
control_url = http://vsms.splinex-team.com/control
record_dir = /tmp
hls_dir = /tmp
hls_time = 6
hls_list_size = 3

[AMQP]
host = vsms.splinex-team.com
port = 5671
ssl = True
vhost = dev
user = human
pass = splinex271813

[BIGQUERY]
service_account_json_fn = /home/maxim/dv/PyVSMS/vsms/config/ULTRACAST-47232919929b.json
ios_dataset = com_360racing_360Racing_IOS
android_dataset = com_racing_racing360_ANDROID

[FIREBASE]
androidPackageName = com.ultracast.app.dev
iosAppStoreId = 1
iosBundleId = com
androidMinPackageVersionCode = 22
dynamicLinkDomain = q
appLink = http://ultracast.com/content
firebaseUrl = https://firebasedynamiclinks.googleapis.com
apiKey = AIzaSyBkXDCTnu1zuxkquuhH8LuE3jA5MgrAU2k
apiKeySecret = AAAAE8-uT_o:APA91bFumgjBzAmGfGkG1vTwR5LfX0xJicnXSU1o86n17x9PUpQUrUEqpxQFgb2bZSg2DKbMk_mxVY5Xgbx9GEScaz3Tm71MpOXAHH4t3tY61lRG2KQ89kPJNHEhwWy3CGQ8IjusdBxn
service_account_json_fn = /home/maxim/dv/PyVSMS/vsms/config/ultracast-161409-firebase-adminsdk-25du4-9b1c0b01ef.json

[ELASTICSEARCH]
port = 9201

[INAPP]
sandbox = True
applebundle = com.360racing.360Racing
androidbundle = com.racing.racing360
androidapikey = AIzaSyDJfKwxQd8bVDzhZe9p6CujcvaPeAk4UwE=
''')

from vsms.models import db
from vsms.models.channel import Channel
from vsms.models.video import Video
from vsms.models.event import Event
from vsms.models.stitcher import Stitcher, StitcherConfiguration, StitcherConfigurationStream
from vsms.models.livestream import LiveStream
from vsms.models.highlight import Highlight
from vsms.models.category import Category
from vsms.models.appuser.profile import Profile
from vsms.models.appuser.message import Message, MESSAGE_TYPE, LiveStreamMessage, VideoMessage
from vsms.models.appuser.subscription import Subscription
from vsms.misc.time import utcnow_unix_time

app = Flask(env.name)

db.init_app(app, {
    'MONGODB_SETTINGS': {
        'host': env.db_host,
        'DB': env.db_collection,
    }
})


def fill_db():

    clear_db()

    category1 = Category(name='category1')
    category1.save()

    video0 = Video(
        title='title0 beauty song guitar',
        tags=[
            'video01',
            'video02',
        ],
        category=category1
    )
    video0.save()

    video1 = Video(
        title='title1 race cars beauty 2017',
        tags=[
            'video11',
            'video12',
            'songhhh',
        ],
        category=category1
    )
    video1.save()

    video2 = Video(
        title='title2 beauty song guitar cars300',
        tags=[
            'video21',
            'video22',
            'video23',
            'race',
            'beauty',
            'song',
        ],
        category=category1
    )
    video2.save()

    video3 = Video(
        title='title3',
        tags=[
            'video31'
        ],
        category=category1,
        private=True
    )
    video3.save()

    video4 = Video(
        title='title3',
        tags=[
            'video31'
        ],
        category=category1,
        available=False
    )
    video4.save()

    highlight1 = Highlight(
        origin_video=video1,
        tags=[
            'highlight1'
        ]
    )
    highlight1.save()
    highlight2 = Highlight(
        origin_video=video2
    )
    highlight2.save()
    highlight3 = Highlight(
        origin_video=video1,
        tags=[
            'highlight3'
        ]
    )
    highlight3.save()
    highlight4 = Highlight(
        origin_video=video3,
        tags=[
            'highlight4'
        ]
    )
    highlight4.save()
    highlight5 = Highlight(
        origin_video=video3,
        tags=[
            'highlight5'
        ],
        available=False,
    )
    highlight5.save()

    stitcher1 = Stitcher(
        name='sticher1',
        manual_mode=False,
        auto_url='http://',
        configuration=StitcherConfiguration(
            streams=[
                StitcherConfigurationStream(),
            ]
        )
    )
    stitcher1.save()

    stitcher2 = Stitcher(
        name='sticher2',
        manual_mode=True,
        auto_url='http://',
        hq_url='http://',
        lq_url='http://',
        configuration=StitcherConfiguration(
            rtmp_url='rtmp://',
            streams=[
                StitcherConfigurationStream(),
            ]
        )
    )
    stitcher2.save()

    livestream1 = LiveStream(
        title='livestream1',
        stitcher=stitcher1
    )
    livestream1.save()

    livestream2 = LiveStream(
        title='livestream2',
        stitcher=stitcher2
    )
    livestream2.save()

    event1 = Event(
        title='event1',
        date=datetime.datetime.utcnow() - datetime.timedelta(days=1),
        end_date=datetime.datetime.utcnow() + datetime.timedelta(hours=1),
        livestreams=[
            livestream1,
            livestream2,
        ],
        tags=[
            'event11',
            'event12'
        ],
        category=category1,
    )
    event1.save()

    event2 = Event(
        title='event2',
        date=datetime.datetime.utcnow() - datetime.timedelta(days=2),
        end_date=datetime.datetime.utcnow() - datetime.timedelta(days=1),
        livestreams=[
            livestream1,
        ],
        tags=[
            'event21',
            'event22'
        ],
        category=category1,
    )
    event2.save()

    event3 = Event(
        title='event3',
        date=datetime.datetime.utcnow() - datetime.timedelta(minutes=1),
        end_date=datetime.datetime.utcnow() + datetime.timedelta(minutes=1),
        livestreams=[
            livestream1,
        ],
        tags=[
            'event31',
            'event32'
        ],
        category=category1,
    )
    event3.save()

    event4 = Event(
        title='event4',
        date=datetime.datetime.utcnow() - datetime.timedelta(days=2),
        end_date=datetime.datetime.utcnow() + datetime.timedelta(days=1),
        livestreams=[
            livestream1,
        ],
        tags=[
            'event41',
            'event42'
        ],
        category=category1,
        available=False,
    )
    event4.save()

    # social shit

    profile1 = Profile(name='profile1', social_client_id='social_client_id', social_network_id=0,
                       token='token', email='email', avatar_url='avatar_url')
    profile1.save()

    ls_message1 = LiveStreamMessage(event=event1, media=livestream1, ts=utcnow_unix_time(),
                                    text='text', tp=MESSAGE_TYPE.TEXT, profile=profile1.id)
    ls_message1.save()


def clear_db():
    Highlight.drop_collection()
    Video.drop_collection()
    Event.drop_collection()
    Stitcher.drop_collection()
    Profile.drop_collection()
    LiveStream.drop_collection()
    LiveStreamMessage.drop_collection()
    Video.drop_collection()
    VideoMessage.drop_collection()
    Subscription.drop_collection()
    Channel.drop_collection()

