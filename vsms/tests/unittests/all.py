from unittest import TestLoader, TextTestRunner, TestSuite

import vsms.tests.unittests
from vsms.tests.unittests.ws_api import TestWSHandlers
from vsms.tests.unittests.rest_api import FlaskTestCase
from vsms.tests.unittests.models import TestModels
from vsms.tests.unittests.business import TestBusiness
from vsms.tests.unittests.adapters import TestFirebase

if __name__ == "__main__":

    loader = TestLoader()
    suite = TestSuite(())
    suite = TestSuite((
        loader.loadTestsFromTestCase(TestWSHandlers),
        loader.loadTestsFromTestCase(FlaskTestCase),
        loader.loadTestsFromTestCase(TestModels),
    ))

    runner = TextTestRunner(verbosity=2)
    runner.run(suite)
