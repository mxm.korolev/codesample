import json
from random import choice
from string import ascii_letters
from tornado.testing import AsyncTestCase, gen_test
from tornado.gen import sleep, coroutine, Future
from tornado.ioloop import IOLoop

from vsms.api.ws import WSHandler
import vsms.business
from vsms.environment import service_env as env
from vsms.adapters.inapp import OS
from vsms.models.highlight import GetHighlights
from vsms.models.jrpc import JRPCCall
from vsms.models.video import Video, EditVideo
from vsms.models.livestream import LiveStream
from vsms.models.event import Event
from vsms.models.category import Category
from vsms.models.channel import Channel, EditChannel
from vsms.models.appuser.handshake import Handshake
from vsms.models.appuser.history import GetVideoHistory, DeleteVideoHistory
from vsms.models.appuser.profile import Register, Authorize
from vsms.models.appuser.message import Message, GetMessages, Donate, MESSAGE_TYPE
from vsms.models.appuser.activity import UserActivityVideoStart, UserActivityVideoFinish, \
    UserActivityLiveStreamStart, EventPublish, EventDone
from vsms.models.appuser.subscription import Subscribe, Unsubscribe, GetSubscriptions, Subscription
from vsms.tests.unittests import fill_db, clear_db


class UnittestWsHandler(WSHandler):

    def __init__(self):
        self.output = [Future()]
        self.cur_output = 0

    def write_message(self, data):
        self.output[-1].set_result(data)
        self.output.append(Future())

    @coroutine
    def read_output(self):
        output = yield self.output[self.cur_output]
        self.cur_output += 1
        return output

    def finish(self):
        pass


class TestWSHandlers(AsyncTestCase):

    def setUp(self):
        super().setUp()
        fill_db()
        self.handler = UnittestWsHandler()
        self.handler.open()

    def tearDown(self):
        clear_db()
        super().tearDown()

    def get_new_ioloop(self):
        return IOLoop.current()

    @coroutine
    def communicate(self, payload, message=None, handler=None, wait_for_success=True):
        handler = handler or self.handler
        if message is None:
            sid = ''.join([choice(ascii_letters) for _ in range(4)])
            signal = JRPCCall(payload.__class__)()
            signal.id = sid
            signal.params = payload
            message = json.dumps(signal.marshal())
        handler.on_message(message)
        response_message = yield handler.read_output()
        # NOTE: here is difficult to make a full model for complex types in general case
        response_dict = json.loads(response_message)
        self.assertEqual(json.loads(message).get('id'), response_dict['id'])
        if wait_for_success:
            self.assertTrue('result' in response_dict, response_dict.get('error', ''))
            return response_dict['result']
        else:
            self.assertTrue('error' in response_dict)
            return response_dict['error']

    @coroutine
    def handshake(self, client_os=OS.ANDROID, handler=None, wait_for_success=True):
        handler = handler or self.handler
        uid = ''.join([choice(ascii_letters) for _ in range(4)])
        yield self.communicate(Handshake(uid=uid, client_os=client_os),
                               handler=handler, wait_for_success=wait_for_success)
        if wait_for_success:
            self.assertTrue(handler.permissions & WSHandler.PERM_GUEST)

    @gen_test
    def test_malformed_message(self):
        yield self.communicate(None, message=json.dumps(dict(params={}, id='123')), wait_for_success=False)
        yield self.communicate(None, message=json.dumps(dict(params={})), wait_for_success=False)
        yield self.communicate(None, message='{}', wait_for_success=False)

    @gen_test
    def test_handshake_handler(self):
        yield self.handshake()

    @gen_test
    def test_highlights_handler(self):

        yield self.handshake()

        get_highlights_response = yield self.communicate(GetHighlights(count=4))
        self.assertEqual(len(get_highlights_response['events']), 2)
        self.assertEqual(len(get_highlights_response['highlights']), 2)
        for event in get_highlights_response['events']:
            if event['title'] != 'event1':
                continue

            self.assertEqual(
                event['livestreams'][1]['stitcher']['auto_url'],
                'http://'
            )

        get_highlights_response = yield self.communicate(GetHighlights(count=4))
        self.assertEqual(len(get_highlights_response['events']), 2)
        self.assertEqual(len(get_highlights_response['highlights']), 2)

        get_highlights_response = yield self.communicate(GetHighlights(count=4, tags=['event12']))
        self.assertEqual(len(get_highlights_response['events']), 1)
        self.assertEqual(len(get_highlights_response['highlights']), 0)

        get_highlights_response = yield self.communicate(GetHighlights(count=4, tags=['video21']))
        self.assertEqual(len(get_highlights_response['events']), 0)
        self.assertEqual(len(get_highlights_response['highlights']), 1)

        get_highlights_response = yield self.communicate(GetHighlights(count=4, tags=['highlight3']))
        self.assertEqual(len(get_highlights_response['events']), 0)
        self.assertEqual(len(get_highlights_response['highlights']), 1)

        # check private highlights
        get_highlights_response = yield self.communicate(GetHighlights(count=4, tags=['highlight4']))
        self.assertEqual(len(get_highlights_response['events']), 0)
        self.assertEqual(len(get_highlights_response['highlights']),0)

        # check disabled highlights
        get_highlights_response = yield self.communicate(GetHighlights(count=4, tags=['highlight5']))
        self.assertEqual(len(get_highlights_response['events']), 0)
        self.assertEqual(len(get_highlights_response['highlights']), 0)

    @coroutine
    def register(self, social_client_id=None, handler=None):
        social_client_id = social_client_id or 'social_client_id'
        yield self.communicate(Register(name='Name', social_network_id=3, social_client_id=social_client_id,
                                        token='token', email='email@mail.com', avatar_url='http://url.com'),
                               handler=handler)

    @coroutine
    def authorize(self, social_client_id=None, handler=None):

        social_client_id = social_client_id or 'social_client_id'
        handler = handler or self.handler

        yield self.communicate(Authorize(social_network_id=3, social_client_id=social_client_id + 'qwe', token='token'),
                               handler=handler,
                               wait_for_success=False)

        yield self.communicate(Authorize(social_network_id=2, social_client_id=social_client_id, token='token'),
                               handler=handler,
                               wait_for_success=False)

        yield self.communicate(Authorize(social_network_id=3, social_client_id=social_client_id, token='token'),
                               handler=handler)

        self.assertTrue(handler.permissions & WSHandler.PERM_AUTHORIZED)

        yield self.communicate(Authorize(social_network_id=3, social_client_id=social_client_id, token='token'),
                               handler=handler,
                               wait_for_success=False)

    @gen_test(timeout=15)
    def test_messages(self):

        # yield UnittestWsHandler.group_exchange.connect()

        yield self.communicate(Message(text='text'), wait_for_success=False)

        yield sleep(10)

        yield self.register()
        yield self.authorize()
        yield self.communicate(Message(text='text'), wait_for_success=False)

        video = Video.objects.first()
        yield self.communicate(UserActivityVideoStart(media=str(video.id)))
        self.assertEqual(Video.objects.get(id=video.id).views, 1)
        self.assertEqual(self.handler.activity, UserActivityVideoStart)
        self.assertEqual(self.handler.media.id, str(video.id))

        self.communicate(Message(text='text', ts=10))

        resp = yield self.communicate(GetVideoHistory())
        resp = yield self.communicate(DeleteVideoHistory(ids=[video.id]))
        resp = yield self.communicate(DeleteVideoHistory())
        resp = yield self.communicate(GetVideoHistory())

        yield self.communicate(UserActivityVideoStart(media=str(video.id)))

        handler2 = UnittestWsHandler()
        handler2.open()

        yield self.register(social_client_id='social_client_id_2', handler=handler2)
        yield self.authorize(social_client_id='social_client_id_2', handler=handler2)

        yield self.communicate(UserActivityVideoStart(media=str(video.id)))
        self.assertEqual(Video.objects.get(id=video.id).views, 3)

        resp = yield self.communicate(GetMessages(ts=0))
        self.assertEqual(len(resp['messages']), 1)
        self.assertEqual(len(resp['messages'][0]['media']), 24)

        resp = yield self.communicate(GetMessages(ts=0, tp=[MESSAGE_TYPE.TEXT]))
        self.assertEqual(len(resp['messages']), 1)
        self.assertEqual(len(resp['messages'][0]['media']), 24)

        resp = yield self.communicate(GetMessages(ts=0, tp=[MESSAGE_TYPE.STAR]))
        self.assertEqual(len(resp['messages']), 0)

        yield self.communicate(UserActivityVideoFinish())
        self.assertIsNone(self.handler.activity)

        ls = LiveStream.objects.first()
        event = Event.objects(livestreams=ls).first()
        group_name = '{}/{}'.format(str(event.id), str(ls.id))
        yield self.communicate(UserActivityLiveStreamStart(event=str(event.id), media=str(ls.id)))
        self.assertEqual(LiveStream.objects.get(id=ls.id).views, 1)
        self.assertEqual(self.handler.activity, UserActivityLiveStreamStart)
        self.assertEqual(self.handler.media.id, ls.id)
        self.assertEqual(len(WSHandler.client_groups[group_name]), 1)

        yield self.communicate(UserActivityLiveStreamStart(event=str(event.id), media=str(ls.id)), handler=handler2)
        self.assertEqual(LiveStream.objects.get(id=ls.id).views, 2)
        self.assertEqual(LiveStream.objects.get(id=ls.id).active, 2)
        self.assertEqual(len(WSHandler.client_groups[group_name]), 2)

        resp1 = yield self.handler.read_output()
        resp2 = yield handler2.read_output()

        yield self.communicate(Message(text='text', ts=100, tp=MESSAGE_TYPE.STAR))

        resp1 = yield self.handler.read_output()
        resp2 = yield handler2.read_output()
        self.assertEqual(resp1, resp2)
        self.assertEqual(json.loads(resp1)['params']['tp'], MESSAGE_TYPE.STAR)
        self.assertEqual(json.loads(resp1)['params']['media'], str(ls.id))
        self.assertIsNotNone(json.loads(resp1).get('params', {}).get('text'))
        self.assertIsNotNone(json.loads(resp1).get('params', {}).get('id'))

        self.handler.on_close()
        self.assertEqual(LiveStream.objects.get(id=ls.id).active, 1)
        self.assertEqual(len(WSHandler.client_groups[group_name]), 1)
        handler2.on_close()
        self.assertEqual(len(WSHandler.client_groups[group_name]), 0)
        self.assertEqual(LiveStream.objects.get(id=ls.id).active, 0)

    @gen_test(timeout=15)
    def test_donate(self):

        yield UnittestWsHandler.group_connect()
        yield sleep(5)
        # yield self.handshake(client_os=None, wait_for_success=False)
        yield self.handshake()
        # yield self.authorize()

        ls = LiveStream.objects.first()
        event = Event.objects(livestreams=ls).first()

        handler2 = UnittestWsHandler()
        handler2.open()

        yield self.register(social_client_id='social_client_id_2', handler=handler2)
        yield self.authorize(social_client_id='social_client_id_2', handler=handler2)
        yield self.communicate(UserActivityLiveStreamStart(event=str(event.id), media=str(ls.id)),
                               handler=handler2)

        yield self.communicate(
            Donate(order_id='GPA.3384-6987-3304-35484',
                   package_name='com.racing.racing360',
                   product_id='com.racing.racing360.donation.1',
                   purchase_time=1515574135655,
                   purchase_state=0,
                   purchase_token='cgodkgbihphhdalondmdanbc.AO-J1OzebkYRxoY8JVB8YO5AGn10H4zWTQD9aOSxZc9Z23WM7UidGA9rC3S_T53RrK1DI4wYylb5uPDDiMZj43XA7rEMWrcdBhHs-HrpcERJF72lvArIa-14Nv2Bz-ykU30gB29hkt1mjwJNOyQP0Xt67pK0WORpbA',
                   signature='D+3pGLJtTs8NNfKyug/5spqFeBNBnIFP9pOoYfpJ9oNtW9MhtkwORVuBV+Io16QCjBM+L3XqtRhNww0eZmv+8jP9DXaZRjAU0LqhXGRoc5qQHiCdA05ePPXwxNMbdRCBRxc+F5LLHFCbMjmOT6Aj4zzn1cHWOE1xd9/F4dwQv3T7Jdf6P6Y08RV80nLaUJYuBmxC4OqSV9vpVnsHJgXmt5IFT7BNUyy9mIy6+3Rv2NbbKgZahRqTNhh4LpXd2EUW1KwfP9xRtAdWGEd18t+gFSMYANvIFSg60JbkskOmN/dPCXD+ylJhehrMTRgYQ6fXROT1fregjjEkQy+6GS3CPQ==',
                   event=str(event.id),
                   media=str(ls.id))
        )

        resp = yield handler2.read_output()
        self.assertEqual(json.loads(resp)['params']['tp'], MESSAGE_TYPE.DONATE)


    @gen_test
    def test_subscriptions(self):

        yield self.register()
        yield self.authorize()

        category = Category.objects().first()
        event, event2 = Event.objects()[:2]

        resp = yield self.communicate(Subscribe(category=category.id))
        self.assertEqual(resp['id'], str(Subscription.objects().first().id))

        yield self.communicate(Subscribe(category=category.id), wait_for_success=False)

        yield self.communicate(Subscribe(event=event.id))
        yield self.communicate(Subscribe(event=event2.id))
        resp = yield self.communicate(GetSubscriptions())
        self.assertEqual(len(resp['subscriptions']), 3)

        handler2 = UnittestWsHandler()
        handler2.open()

        yield self.register(social_client_id='social_client_id_2', handler=handler2)
        yield self.authorize(social_client_id='social_client_id_2', handler=handler2)
        yield self.communicate(Subscribe(event=event.id), handler=handler2)
        yield self.communicate(Subscribe(category=category.id), handler=handler2)

        sub_id = resp['subscriptions'][0]['id']
        yield self.communicate(Unsubscribe(id=sub_id))

        resp = yield self.communicate(GetSubscriptions())
        self.assertEqual(len(resp['subscriptions']), 2)

    @gen_test
    def test_publishing(self):
        yield self.register()
        yield self.authorize()

        resp = yield self.communicate(EditChannel(title='Channel title', description='description'))

        channel = Channel.objects.get(title='Channel title')
        self.assertEqual(channel.items, 0)
        resp = yield self.communicate(Subscribe(channel=channel.id))

        resp = yield self.communicate(EventPublish(title='Live title'))
        # self.assertEqual(Channel.objects.get(title='Channel title').items, 1)
        resp = yield self.communicate(EventDone(available=False))

        resp

    @gen_test(timeout=5)
    def test_video_history(self):

        yield self.register()
        yield self.authorize()

        video = Video.objects.first()
        yield self.communicate(UserActivityVideoStart(media=str(video.id)))
        yield self.communicate(UserActivityVideoFinish())
        resp = yield self.communicate(GetVideoHistory())
        self.assertEqual(resp['total'], 1)
        video.delete()
        yield sleep(1)
        resp = yield self.communicate(GetVideoHistory())
        self.assertEqual(resp['total'], 0)
        1
