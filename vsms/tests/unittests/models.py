import unittest

from flask_restplus.model import Model
from vsms.models.highlight import GetHighlightsResponse
from vsms.models.appuser.handshake import HandshakeResponse
from vsms.models.stitcher import StitcherConfiguration, StitcherConfigurationStream
from collections import OrderedDict


class TestModels(unittest.TestCase):

    def test_serializer_schema(self):
        
        schema = GetHighlightsResponse.serializer()
        self.assertEqual(type(schema), Model)
        
        handshake_response = HandshakeResponse(id='123')
        self.assertEqual(type(handshake_response.marshal()), OrderedDict)

    def test_stitcher_configuration_schema(self):

        streams_conf1 = StitcherConfigurationStream(
            id='hq',
            height=1440,
            width=2560,
        )
        streams_conf2 = StitcherConfigurationStream(
            id='lq',
            height=1440,
            width=2560,
        )
        conf = StitcherConfiguration(
            rtmp_url='rtmp://',
            gop_size=100,
            streams=[
                streams_conf1.marshal(),
                streams_conf2.marshal(),
            ],
            mic_name='Line',
            mono_sound=True,
            external_mic=False,
            sound_enabled=True,
            wait_accelerometer=True,
            camera_url='tcp://:2000',
        )
        dump = conf.dump_stitcher_format()
        conf_loaded = StitcherConfiguration()
        conf_loaded.load_stitcher_format(dump)

        self.assertEqual(conf.rtmp_url, conf_loaded.rtmp_url)
        self.assertEqual(conf.mic_name, conf_loaded.mic_name)
        self.assertEqual(conf.streams[1].id, conf_loaded.streams[1].id)


