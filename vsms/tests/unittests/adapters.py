import unittest
from os.path import join as path_join
from time import sleep

from vsms.tests.unittests import fill_db, clear_db
from vsms.environment import service_env as env, informer
from vsms.adapters.firebase import Firebase
from vsms.business.push import notify_event_start, notify_event_finish
from vsms.models.event import Event


class TestFirebase(unittest.TestCase):

    def setUp(self):
        super().setUp()
        fill_db()
        # self.iid_token = 'dBlilKwMFT8:APA91bHD2xzNfVKF4qQHHA0rMPowCE_Hu4VMQVa00aivPVxgopJa_I1q866-rE1iJL53pf7FUh5hiNCVNOUGDlqUIhFFnlsaPf8Kps22jbyIAmb8LbhnMegbjfQrwCX21FU12J0SwiE-'
        self.iid_token = 'd5TOCrW5vBg:APA91bGeSO44qaZDL5XhO_mwPNKBkaF0coACCgTRH_qhvjFh4Z37kELdL1M56ODpQHhdleo23Ilw_OEuJ2JKE90MxRNZqSPa504H9RHuuGJoX8G8ht3raTfEEm23GhZS6ILMQXByGy0o'

    def tearDown(self):
        clear_db()
        super().tearDown()

    def test_get_subscriptions(self):

        informer.info(Firebase._get_access_token())

        subs = Firebase.get_subscriptions(self.iid_token)
        for sub in subs:
            Firebase.unsubscribe(self.iid_token, sub)
        #
        event = Event.objects().first()
        #
        Firebase.subscribe(self.iid_token, str(event.id))
        # # self.assertEqual(len(Firebase.get_subscriptions(self.iid_token)), 1)

        sleep(1)
        self.assertTrue(notify_event_start(event))

        Firebase.unsubscribe(self.iid_token, str(event.id))
        self.assertCountEqual(Firebase.get_subscriptions(self.iid_token), [])
