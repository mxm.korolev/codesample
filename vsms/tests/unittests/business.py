import unittest
from os.path import join as path_join

from vsms.tests.unittests import fill_db, clear_db
from vsms.environment import service_env as env
from vsms.business.record import stop_record
from vsms.models.livestream import LiveStream
from vsms.models.video import Video
from vsms.models.appuser.message import VideoMessage, LiveStreamMessage


class TestBusiness(unittest.TestCase):

    def setUp(self):
        super().setUp()
        fill_db()

    def tearDown(self):
        clear_db()
        super().tearDown()

    def test_record_done(self):
        ls = LiveStream.objects.get(title='livestream1')
        stream_name = ls.stitcher.name + '_hq'
        flv_fn = path_join(env.nginx_record_dir, stream_name + '.flv')
        with open(flv_fn, 'a') as f:
            pass
        ls_messages = list(LiveStreamMessage.objects(media=ls))

        stop_record(ls)

        video = Video.objects.get(title=str(ls))
        v_messages = list(VideoMessage.objects(media=video))
        self.assertEqual(len(ls_messages), len(v_messages))
