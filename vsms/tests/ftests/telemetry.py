'''
Created on May 15, 2017

@author: maxim
'''
import json
import os
import unittest

from pycontrib.service.environment import Environment
from tornado.gen import coroutine, sleep
from tornado.httpclient import HTTPClient
from tornado.testing import AsyncTestCase, gen_test
from tornado.testing import main
from tornado.websocket import websocket_connect


# Environment(config_file='/home/maxim/dev/PyVCMS/vsms/config/service.cfg')
# dir_ = os.path.dirname(__file__)
# paths = [os.path.join(dir_, ".."), os.path.join(dir_, "..", "..")]
# sys.path.extend(paths)
class TelemetryTest(AsyncTestCase):

    def setUp(self):
        super().setUp()
#         self.env = Environment(config_file=os.environ.get('ENVIRONMENT_CONFIG_FILE'))

    @gen_test(timeout=30000)
    def test_publish(self):
#         HTTPClient().fetch('https://vsms-testing.splinex-team.com/video/export', validate_cert=False)
#         return
# wss://vsms-testing.splinex-team.com/video/589c6392f6ae6c053b862ce8/telemetry

        url = 'wss://vsms-testing.splinex-team.com/video/589c6392f6ae6c053b862ce8/telemetry'  # .format(self.env.config['NETWORK'])
        ws = yield websocket_connect(url)
        for _ in range(2):
            ws.write_message(json.dumps({'ts': 1345342322323, 'a':0.34, 'b':6.345, 'c':45}))
            yield sleep(0.1)

if __name__ == '__main__':
    unittest.main()
