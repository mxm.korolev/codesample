from os import environ as os_environ
from vsms.environment import service_env as env

env.initialize(config_fn=os_environ.get('VSMS_CFG', './service.cfg'))

from vsms.adapters.celery import celery, setup_flask_celery, check_tasks_state
from vsms.wsgi import WSGIApplication

from vsms.models.video import static_video_make_hls
from vsms.business.terminal import rtmp_stream_make_hls_on_publish, rtmp_stream_upstream_on_publish

app = WSGIApplication()
setup_flask_celery(app)

celery
