from flask import Flask, Blueprint
from flask_admin import Admin
from flask_security import Security

import vsms
from vsms.environment import service_env as env
import vsms.business

from vsms.models import db
from vsms.models.configuration import GlobalConfiguration
from vsms.models.user import user_datastore, Role, User
from vsms.models.channel import Channel
from vsms.models.video import Video
from vsms.models.event import Event
from vsms.models.highlight import Highlight
from vsms.models.livestream import LiveStream
from vsms.models.category import Category
from vsms.models.stitcher import Stitcher
from vsms.models.task import Task
from vsms.models.geo import GeoRule
from vsms.models.appuser.profile import Profile
from vsms.models.advertisement import Ad
from vsms.views.base import CommonModelView
from vsms.views.configuration import GlobalConfigurationView
from vsms.views.index import HomeView
from vsms.views.category import CategoryView
from vsms.views.highlight import HighlightView
from vsms.views.stitcher import StitcherView
from vsms.views.event import EventView, EventVendorView
from vsms.views.video import VideoView
from vsms.views.livestream import LiveStreamView
from vsms.views.task import TaskView
from vsms.views.geo import GeoRuleView
from vsms.views.profile import ProfileView
from vsms.views.advertisement import AdView

from vsms.api import api
from vsms.api.rest.video import ns as video_ns
from vsms.api.rest.event import ns as event_ns
from vsms.api.rest.content import ns as content_ns
from vsms.api.rest.category import ns as category_ns
from vsms.api.rest.configuration import ns as configuration_ns
from vsms.api.ws import ns as ws_ns
from vsms.api.push import ns as push_ns


class WSGIApplication(Flask):
    
    def __init__(self):
        super().__init__(env.name, template_folder=env.template_dir)

        self.config['DEBUG'] = env.debug
        self.config['MONGODB_SETTINGS'] = {'DB': env.db_collection,
                                           'port': env.db_port}
        self.config['SECURITY_PASSWORD_SALT'] = 'fghfb876gdghdsf87b6s87ef6x'
        self.config['SECURITY_POST_LOGIN_VIEW'] = '/admin'
        self.config['SECURITY_POST_LOGOUT_VIEW'] = '/admin'
        self.config['CELERY_BROKER_URL'] = env.amqp_url
        self.config['CELERY_RESULT_BACKEND'] = 'mongoengine'
        self.config['BROKER_USE_SSL'] = env.amqp_ssl
        self.config['CELERY_MONGODB_BACKEND_SETTINGS'] = {
            'host': env.db_host,
            'port': env.db_port,
            'database': env.db_collection,
            'taskmeta_collection': 'tasks'
        }

        db.init_app(self)

        self.blueprint = Blueprint('api', __name__,
                                   url_prefix='/api')
        api.init_app(self.blueprint)
        api.add_namespace(ws_ns)
        api.add_namespace(push_ns)
        api.add_namespace(video_ns)
        api.add_namespace(event_ns)
        api.add_namespace(content_ns)
        api.add_namespace(category_ns)
        api.add_namespace(configuration_ns)
        self.register_blueprint(self.blueprint)

        Security(self, user_datastore)
                
        self.admin = Admin(
            self, 
            name='{} v{}'.format(env.name, vsms.__version__),
            template_mode='bootstrap3',
            index_view=HomeView(
                name='Info'
            ),
        )

        self.admin.add_view(VideoView(Video))
        self.admin.add_view(AdView(Ad))
        self.admin.add_view(HighlightView(Highlight))
        self.admin.add_view(StitcherView(Stitcher))
        self.admin.add_view(LiveStreamView(LiveStream))
        self.admin.add_view(EventView(Event))
        self.admin.add_view(EventVendorView(Event, endpoint='event2')),
        self.admin.add_view(CommonModelView(Channel))
        self.admin.add_view(CategoryView(Category))
        self.admin.add_view(ProfileView(Profile))
        self.admin.add_view(GeoRuleView(GeoRule))
        self.admin.add_view(GlobalConfigurationView(GlobalConfiguration))
        self.admin.add_view(CommonModelView(Role))
        self.admin.add_view(CommonModelView(User))
        self.admin.add_view(TaskView(Task))
        
        self.secret_key = env.service_private_key

