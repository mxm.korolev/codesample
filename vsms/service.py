from vsms.environment import service_env as env, informer
env.initialize()

from vsms.adapters.celery import setup_flask_celery

from pycontrib import monguo

import signal
from functools import partial
from tornado.httpserver import HTTPServer
from tornado.web import Application, FallbackHandler
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.wsgi import WSGIContainer
from tornado.netutil import bind_sockets

import vsms
from vsms.wsgi import WSGIApplication
from vsms.business.initial import first_run_init
from vsms.business.periodic import Periodic
from vsms.netcontrol.root import RootController
from vsms.cms.models import Event
from vsms.cms.handlers import HttpRedirectHandler, EntityExportHandler, CachedStaticFileHandler
from vsms.api.rest.upload import StreamUploadHandler
from vsms.api.rest.player import PlayerHandler
from vsms.api.ws import WSHandler
from vsms.misc.process import tornado_sigterm_handler, fork_processes, is_first_child


def main():
    informer.info('Started v{} at {}:{}, home {}, processes {}, debug {}'.format(vsms.__version__,
                                                                                 env.host, env.port,
                                                                                 env.project_dir, env.threads,
                                                                                 env.debug))
    sockets = bind_sockets(env.port, reuse_port=True)

    if env.threads != 1:
        signal.signal(signal.SIGTERM, tornado_sigterm_handler)
        signal.signal(signal.SIGINT, tornado_sigterm_handler)
        fork_processes(env.threads)

    app = WSGIApplication()
    with app.app_context():
        first_run_init()
    setup_flask_celery(app)

    root_controller = RootController()
    root_controller.connect()
    WSHandler.group_exchange.connect()

    if env.threads == 1 or is_first_child():
        periodic = Periodic(root_controller)
        PeriodicCallback(periodic.tick, env.timeout * 1000).start()

    app_map = [
        (r'/', HttpRedirectHandler, dict(target_path='/admin')),
        (r'/static/(.*)', CachedStaticFileHandler, dict(path=env.service_upload_dir, cache_ttl=10)),
        (r'/event/export', EntityExportHandler, dict(env=env, entity_cls=Event)), # legacy
        (r'/api/upload', StreamUploadHandler),
        (r'/api/ws', WSHandler),
        (r'/player', PlayerHandler),
        (r'.*', FallbackHandler, dict(fallback=WSGIContainer(app))),
    ]
    http_app = Application(app_map, debug=env.debug, autoreload=False)
    http_server = HTTPServer(http_app, chunk_size=10*2**20, max_body_size=10*2**30)
    http_server.add_sockets(sockets)

    IOLoop.instance().start()


if __name__ == '__main__':
    main()
