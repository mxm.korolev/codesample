from random import choice
from string import ascii_letters, digits


def random_filename(extension, length=12):
    return '{}.{}'.format(''.join([choice(ascii_letters + digits) for _ in range(length)]), extension)
