from datetime import datetime
from pytz import timezone


def unix_time(dt):
    return int(timezone('UTC').localize(dt).timestamp() * 1000)


def utcnow_unix_time():
    return unix_time(datetime.utcnow())
