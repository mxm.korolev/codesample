import collections


def recursive_update(d, up):
    for k, v in up.items():
        if k not in d:
            d[k] = v
        elif isinstance(v, collections.Mapping):
            d[k] = recursive_update(d[k], v)
        else:
            d[k] = v
    return d


class EnumItem:
    def __init__(self, val):
        self.val = val


class EnumMeta(type):
    def __init__(cls, name, bases, nmspc):
        cls.__map__ = dict()
        for attr, val in nmspc.items():
            if isinstance(val, EnumItem):
                cls.__map__[val.val] = attr
                setattr(cls, attr, val.val)


class Enum(metaclass=EnumMeta):
    @classmethod
    def repr(cls, val):
        return cls.__map__[val]

    @classmethod
    def list(cls):
        return cls.__map__.keys()

    @classmethod
    def items(cls):
        return cls.__map__.items()

