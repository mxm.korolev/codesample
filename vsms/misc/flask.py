from flask import flash as flask_flash

from vsms.environment import informer


def flash(message, category='message'):

    try:
        flask_flash(message, category=category)
    except RuntimeError:
        if category == 'message':
            informer.info(message)
        else:
            informer.error(message)
