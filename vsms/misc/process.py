import signal
import os
import sys
import errno
import psutil
from tornado.ioloop import IOLoop
from tornado.process import cpu_count, _reseed_random
from tornado.util import errno_from_exception

from vsms.environment import informer


def tornado_sigterm_handler(signum, frame):

    if IOLoop.initialized():
        # child process
        informer.info('Child process {} got SIGTERM. Shutdown'.format(os.getpid()))
        IOLoop.instance().stop()
    else:
        # master process
        informer.info('Master process {} got SIGTERM. Shutdown'.format(os.getpid()))
        master = psutil.Process(os.getpid())
        for child in master.children(recursive=True):
            child.send_signal(signal.SIGTERM)
    # exit(0)


_task_id = None
_first_child = False


def is_first_child():
    return _first_child


def fork_processes(num_processes=None, max_restarts=100):
    global _task_id, _first_child
    assert _task_id is None
    num_processes = num_processes or cpu_count()
    if IOLoop.initialized():
        raise RuntimeError("Cannot run in multiple processes: IOLoop instance "
                           "has already been initialized. You cannot call "
                           "IOLoop.instance() before calling start_processes()")
    informer.info("Starting %d processes", num_processes)
    children = {}

    def start_child(i):
        pid = os.fork()
        if pid == 0:
            # child process
            _reseed_random()
            global _task_id
            _task_id = i
            return i
        else:
            children[pid] = i
            return None

    first = True
    for i in range(num_processes):
        id = start_child(i)
        if id is not None:
            if first:
                _first_child = True
            return id
        first = False
    num_restarts = 0
    while children:
        try:
            pid, status = os.wait()
        except OSError as e:
            if errno_from_exception(e) == errno.EINTR:
                continue
            raise
        if pid not in children:
            continue
        id = children.pop(pid)
        if os.WIFSIGNALED(status):
            informer.error("child %d (pid %d) killed by signal %d, restarting",
                            id, pid, os.WTERMSIG(status))
        elif os.WEXITSTATUS(status) != 0:
            informer.error("child %d (pid %d) exited with status %d, restarting",
                            id, pid, os.WEXITSTATUS(status))
        else:
            informer.info("child %d (pid %d) exited normally", id, pid)
            continue
        num_restarts += 1
        if num_restarts > max_restarts:
            raise RuntimeError("Too many child restarts, giving up")
        new_id = start_child(id)
        if new_id is not None:
            return new_id
    # All child processes exited cleanly, so exit the master process
    # instead of just returning to right after the call to
    # fork_processes (which will probably just start up another IOLoop
    # unless the caller checks the return value).
    sys.exit(0)
