import uuid
import datetime
import json
import smtplib
import hashlib
from os import makedirs, path
from statistics import median as stat_median, mean, StatisticsError
from google.cloud import bigquery
from google.cloud.exceptions import NotFound

from vsms.environment import informer


def median(data):
    try:
        m = stat_median(data)
    except StatisticsError:
        m = 0
    return m


class Query():

    def __init__(self, sql, fields=None):
        self.sql = sql
        self.fields = fields
    def __repr__(self):
        return self.sql


class BigQuieryStatistics():

    def __init__(self, service_account_json_fn, apps):

        self.client = bigquery.Client.from_service_account_json(
            service_account_json_fn
        )
        self.apps = apps

    def fetch_app_events(self, app=None, date=None, cache_dir=None):

        cache_dir = cache_dir or '/tmp'

        if not app:
            data = []
            for a in self.apps.values():
                data += self.fetch_app_events(a, date=date, cache_dir=cache_dir)
            return data

        table_name = 'app_events_{}'.format(date.strftime("%Y%m%d"))

        table_fn = path.join(cache_dir, app, table_name)
        try:
            with open(table_fn, 'r') as f:
                data = json.loads(f.read())
                informer.info('Loaded from cache {}'.format(table_fn))
        except Exception:
            makedirs(path.join(cache_dir, app), exist_ok=True)
        else:
            return data

        dataset = self.client.dataset(app)
        if not dataset.exists():
            raise Exception('Unknown dataset {}'.format(self.client.project))

        informer.info('Fetching {}.{}'.format(app, table_name))
        table = dataset.table(table_name)
        if not table.exists():
            informer.error('Unknown table {}'.format(self.client.project))
            return []
        table.reload()
        try:
            data = list(table.fetch_data())
        except NotFound as e:
            informer.error(e)
            return []

        if cache_dir:
            with open(table_fn, 'w') as f:
                f.write(json.dumps(data, indent=4))
                informer.info('Saved to cache {}'.format(table_fn))

        return data

    def run_query(self, query, cache_dir=None):
        cache_dir = cache_dir or '/tmp'

        if cache_dir:
            md5 = hashlib.md5()
            md5.update(query.sql.encode())
            md5.update(','.join(query.fields).encode())
            key = md5.hexdigest()
            table_fn = path.join(cache_dir, key)
            try:
                with open(table_fn, 'r') as f:
                    data = json.loads(f.read())
                informer.info('Loaded from cache {}'.format(key))
            except:
                makedirs(cache_dir, exist_ok=True)
            else:
                return data

        query_job = self.client.run_async_query(str(uuid.uuid4()), query.sql)
        # Set use_legacy_sql to False to use standard SQL syntax. See:
        # https://cloud.google.com/bigquery/docs/reference/standard-sql/enabling-standard-sql
        query_job.use_legacy_sql = False
        query_job.begin()

        data = list()
        try:
            query_job.result()  # Wait for job to complete.
        except NotFound as e:
            informer.error(e)
            return data

        # Print the results.
        destination_table = query_job.destination
        destination_table.reload()

        for row in destination_table.fetch_data():
            if query.fields:
                data.append(dict(zip(query.fields, row)))
            else:
                data.append(row)

        if cache_dir:
            with open(table_fn, 'w') as f:
                f.write(json.dumps(data, indent=4))
            informer.info('Saved to cache {}'.format(key))

        return data

    def build_query_sessions(self, app, date=None):
        '''
        https://blog.modeanalytics.com/finding-user-sessions-sql/
        '''

        date = date or datetime.date.today() - datetime.timedelta(days=1)

        sql = '''
SELECT app_instance_id, sess_id, first_open_timestamp_micros, MIN(min_time) sess_start, MAX(max_time) sess_end, COUNT(*) records, MAX(sess_id) OVER(PARTITION BY app_instance_id) total_sessions,
   (ROUND((MAX(max_time)-MIN(min_time))/(1000*1000),1)) sess_length_seconds
FROM (
  SELECT *, SUM(session_start) OVER(PARTITION BY app_instance_id ORDER BY min_time) sess_id
  FROM (
    SELECT *, IF(
                previous IS null 
                OR (min_time-previous)>(20*60*1000*1000),  # sessions broken by this inactivity 
                1, 0) session_start                 
    FROM (
      SELECT *, LAG(max_time, 1) OVER(PARTITION BY app_instance_id ORDER BY max_time) previous
      FROM (
        SELECT user_dim.app_info.app_instance_id, user_dim.first_open_timestamp_micros
          , (SELECT MIN(timestamp_micros) FROM UNNEST(event_dim)) min_time
          , (SELECT MAX(timestamp_micros) FROM UNNEST(event_dim)) max_time
        FROM `{table}`
      )
    )
  )
)
GROUP BY 1, 2, 3'''.format(
           table='{}.{}.app_events_{}'.format(self.client.project, app, date.strftime("%Y%m%d"))
        )
        return Query(
            sql,
            ['app_instance_id', 'sess_id', 'first_open_timestamp_micros', 'sess_start',
             'sess_end', 'records', 'total_sessions', 'sess_length_seconds']
        )

    def sessions(self, app, date=None):
        query = self.build_query_sessions(app, date)
        for row in self.run_query(query):
            yield row

    def sessions_analyse(self, date):
        report = {}
        for vapp, app in self.apps.items():
            sessions = list(self.sessions(app, date))
            report.update({
                '%s_session_count' % vapp: len(sessions),
                '%s_session_duration_median' % vapp: median([float(s['sess_length_seconds']) for s in sessions]),
                '%s_sessions_per_user' % vapp: median([float(s['total_sessions']) for s in sessions]),
                '%s_users_count' % vapp: len(set([s['app_instance_id'] for s in sessions])),
                '%s_new_users_count' % vapp: len(list(filter(lambda s: s['first_open_timestamp_micros']==s['sess_start'], sessions))),
            })
        report.update({
            'session_count': sum([report['%s_session_count' % vapp] for vapp in self.apps.keys()]),
            'session_duration_mean': mean([report['%s_session_duration_median' % vapp] for vapp in self.apps.keys()]),
            'users_count': sum([report['%s_users_count' % vapp] for vapp in self.apps.keys()]),
            'new_users_count': sum([report['%s_new_users_count' % vapp] for vapp in self.apps.keys()]),
            'sessions_per_user': mean([report['%s_sessions_per_user' % vapp] for vapp in self.apps.keys()]),
        })

        return report

    def media_analyse(self, data):

        def get_param(record, key):
            params = record.get('params', [])
            for param in params:
                if param.get('key') == key:
                    return param
            return {}

        def get_param_value(record, key, value):
            return get_param(record, key).get('value', {}).get(value)

        player_durations = list()
        livestream_watched = dict()
        video_watched = dict()
        nav_highlight_video = 0
        nav_library_video = 0
        nav_highlight_livestream = 0
        nav_event_livestream = 0
        for ses in data:
            event_dim = ses[1]
            for event in event_dim:
                event_name = event.get('name')
                if event_name == 'player_duration':
                    for param in event.get('params', []):
                        if param.get('key') == 'value':
                            value = param.get('value',{}).get('int_value') or param.get('value',{}).get('double_value')
                            if value:
                                player_durations.append(value)
                            break

                elif event_name == 'select_content':
                    content_type = get_param_value(event, 'content_type', 'string_value')
                    content_id = get_param_value(event, 'item_id', 'string_value') or \
                                 get_param_value(event, 'content_id', 'string_value')
                    origin = get_param_value(event, 'origin', 'string_value')

                    if content_type == 'Live stream' or content_type=='Live streams':
                        livestream_watched[content_id] = livestream_watched.get(content_id, 0) + 1
                        if origin == 'Highlights':
                            nav_highlight_livestream += 1
                        elif origin == 'Event':
                            nav_event_livestream += 1
                    elif content_type == 'Video':
                        video_watched[content_id] = video_watched.get(content_id, 0) + 1
                        if origin == 'Highlights':
                            nav_highlight_video += 1
                        elif origin == 'Library':
                            nav_library_video += 1

        return {
            'player_duration_median': median(player_durations) if player_durations else 0,
            'player_duration_sum': sum(player_durations),
            'video_watched_count': sum([x[1] for x in video_watched.items()]),
            'video_watched_uniq_count': len(video_watched.keys()),
            'livestream_watched_count': sum([x[1] for x in livestream_watched.items()]),
            'nav_highlight_video': nav_highlight_video,
            'nav_library_video': nav_library_video,
            'nav_highlight_livestream': nav_highlight_livestream,
            'nav_event_livestream': nav_event_livestream,
        }

    def analyse(self, date, trend=True):

        report = dict()
        report.update(self.sessions_analyse(date=date))
        data = self.fetch_app_events(date=date)
        report.update(self.media_analyse(data))

        if trend:
            prev_report = self.analyse(date - datetime.timedelta(days=1), trend=False)
            for key, val in list(report.items()):
                if type(val) is list:
                    continue
                report['%s_trend' % key] = val - prev_report[key]

        report['date'] = date
        return report


if __name__ == '__main__':

    from vsms.environment import service_env as env
    env.initialize(redefine_tornado_logging=True)

    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    from tornado.template import Loader

    loader = Loader(env.template_dir)

    date = datetime.date.today() - datetime.timedelta(days=1)
    stat = BigQuieryStatistics(
        env.bigquery_service_account_json_fn, {
            'ios': env.bigquery_ios_dataset,
            'android': env.bigquery_android_dataset,
        }
    )
    report = stat.analyse(date=date)

    if env.mail_enabled:
        html_report = loader.load('report.html').generate(
            **report
        ).decode()
        msg = MIMEMultipart('alternative')
        msg['Subject'] = 'Ultracast statistics {}'.format(date.strftime('%Y/%m/%d'))
        msg['From'] = env.mail_fromaddr
        msg['To'] = env.mail_toaddrs[0]
        msg.attach(MIMEText(html_report, 'html'))
        server = smtplib.SMTP(env.mail_smtpserver, 587)
        server.ehlo()
        server.starttls()
        server.login(env.mail_fromaddr, env.mail_password)
        server.sendmail(msg['From'], env.mail_toaddrs, msg.as_string())
        server.quit()
        informer.info('Statistics sent')
    else:
        informer.info(report)
