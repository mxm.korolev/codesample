from werkzeug.exceptions import BadRequest, MethodNotAllowed, Conflict
from mongoengine.queryset import DoesNotExist
import json
from time import sleep

from vsms.misc.time import utcnow_unix_time
from vsms.environment import service_env as env
from vsms.api.ws import WSHandler
from vsms.models.jrpc import JRPCCall
from vsms.models.appuser.activity import UserActivityVideoStart, UserActivityVideoFinish, \
    UserActivityLiveStreamStart, UserActivityLiveStreamFinish, \
    UserActivityResponse, UserActivityLiveStreamStartResponse, \
    EventPublish, EventPublishResponse, EventDone, EventDoneResponse
from vsms.models.video import Video
from vsms.models.livestream import LiveStream
from vsms.models.stitcher import Stitcher
from vsms.models.event import Event
from vsms.models.appuser.message import Message, MESSAGE_TYPE


@WSHandler.register_on_open(UserActivityVideoStart)
def on_open(handler):
    handler.activity = None


def user_activity_finish_route(handler):
    handler.activity = None
    handler.event = None
    handler.media = None


@WSHandler.register_on_close(UserActivityLiveStreamFinish)
def on_close(handler):
    if handler.activity == UserActivityLiveStreamStart:
        user_activity_livestream_finish_route(handler, UserActivityLiveStreamFinish())
    user_activity_finish_route(handler)

    if handler.permissions == WSHandler.PERM_AUTHORIZED:
        handler.profile.save()


@WSHandler.register_callback(UserActivityVideoStart, UserActivityResponse,
                             permissions=WSHandler.PERM_AUTHORIZED | WSHandler.PERM_GUEST)
def user_activity_video_start_route(handler, user_activity):
    if user_activity.media is None:
        raise BadRequest()

    Video.objects(id=user_activity.media.id).update_one(inc__views=1)

    if handler.permissions == WSHandler.PERM_AUTHORIZED:
        handler.profile.video_history[user_activity.media.id] = utcnow_unix_time()
        if len(handler.profile.video_history) > 210:
            handler.profile.video_history = dict(
                sorted(handler.profile.video_history.items(), key=lambda x: x[0], reverse=True)[:200]
            )

    handler.activity = UserActivityVideoStart
    handler.media = user_activity.media
    return UserActivityResponse()


@WSHandler.register_callback(UserActivityVideoFinish, UserActivityResponse,
                             permissions=WSHandler.PERM_AUTHORIZED | WSHandler.PERM_GUEST)
def user_activity_video_finish_route(handler, _):
    user_activity_finish_route(handler)
    return UserActivityResponse()


@WSHandler.register_callback(UserActivityLiveStreamStart, UserActivityLiveStreamStartResponse,
                             permissions=WSHandler.PERM_AUTHORIZED | WSHandler.PERM_GUEST)
def user_activity_livestream_start_route(handler, user_activity):
    if None in (user_activity.event, user_activity.media):
        raise BadRequest()

    handler.activity = UserActivityLiveStreamStart
    handler.event = user_activity.event
    handler.media = user_activity.media.fetch()

    handler.group_name = '{}/{}'.format(user_activity.event.id, user_activity.media.id)
    handler.group_add(handler.group_name)

    if handler.profile.channel is not None:
        handler.event = user_activity.event.fetch()
        if handler.event.channel == handler.profile.channel:
            return UserActivityLiveStreamStartResponse(active=handler.media.active)

    LiveStream.objects(id=user_activity.media.id).update_one(inc__views=1, inc__active=1)
    enter_message = Message(tp=MESSAGE_TYPE.ENTER, profile=handler.profile,
                            ts=utcnow_unix_time(), media=handler.media)
    jrpc_message = JRPCCall(Message)()
    jrpc_message.params = enter_message
    jrpc_message_json = json.dumps(jrpc_message.marshal())
    handler.group_publish(handler.group_name, jrpc_message_json)

    return UserActivityLiveStreamStartResponse(active=handler.media.active + 1)


@WSHandler.register_callback(UserActivityLiveStreamFinish, UserActivityResponse,
                             permissions=WSHandler.PERM_AUTHORIZED | WSHandler.PERM_GUEST)
def user_activity_livestream_finish_route(handler, _):
    if not handler.activity == UserActivityLiveStreamStart:
        raise BadRequest('Not UserActivityLiveStreamStart activity')

    if (handler.permissions == WSHandler.PERM_AUTHORIZED and
            (handler.profile.channel is None or handler.event.channel != handler.profile.channel)):
        LiveStream.objects(id=handler.media.id).update_one(dec__active=1)
        try:
            handler.group_remove(handler.group_name)
        except AttributeError:
            pass
        exit_message = Message(tp=MESSAGE_TYPE.EXIT, profile=handler.profile,
                               ts=utcnow_unix_time(), media=handler.media)
        jrpc_message = JRPCCall(Message)()
        jrpc_message.params = exit_message
        jrpc_message_json = json.dumps(jrpc_message.marshal())
        handler.group_publish(handler.group_name, jrpc_message_json)

    user_activity_finish_route(handler)
    return UserActivityResponse()


@WSHandler.register_callback(EventPublish, EventPublishResponse, permissions=WSHandler.PERM_AUTHORIZED)
def event_publish_route(handler, ls_publish):
    channel = handler.profile.channel
    if channel is None:
        raise MethodNotAllowed('User is not a publisher')
    if channel.publishing:
        raise Conflict('Secondary publishing')
    try:
        stitcher = Stitcher.objects.get(name=str(handler.profile.id))
    except DoesNotExist:
        stitcher = Stitcher(name=str(handler.profile.id))
        stitcher.save(validate=False)
    try:
        ls = LiveStream.objects.get(stitcher=stitcher)
    except DoesNotExist:
        ls = LiveStream(title=str(handler.profile.id), stitcher=stitcher)
    ls.views = 0
    ls.active = 0
    ls.save()
    event = Event(generated_by_tp=Event.GeneratedByType.user, title=ls_publish.title,
                  date=handler.now, livestreams=[ls], recording=True, channel=channel)
    event.save(validate=False)
    channel.publishing = True
    channel.save()
    return EventPublishResponse(rtmp_url='{}/hls/{}_hq'.format(env.nginx_rtmp_server_url, stitcher.name),
                                event=event, livestream=ls)


@WSHandler.register_callback(EventDone, EventDoneResponse, permissions=WSHandler.PERM_AUTHORIZED)
def event_done_route(handler, event_done):
    channel = handler.profile.channel
    if channel is None:
        raise MethodNotAllowed('User is not a publisher')
    if not channel.publishing:
        raise Conflict('No publishing')
    event = Event.objects.get(channel=channel, end_date=None)
    event.end_date = handler.now
    event.recording = False
    event.record = event_done.available
    event.title = event_done.title
    event.category = event_done.category
    event.save()
    channel.publishing = False
    channel.save()
    # TODO: blocking
    sleep(1)
    recorded_video = Video.objects.get(available=True, channel=channel, date=event.date)
    return EventDoneResponse(video=recorded_video)
