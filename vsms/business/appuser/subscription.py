from vsms.adapters.firebase import Firebase
from vsms.api.ws import WSHandler
from vsms.models.appuser.subscription import Subscription, Subscribe, SubscribeResponse, GetSubscriptions, \
    GetSubscriptionsResponse, Unsubscribe, UnsubscribeResponse
from vsms.models.channel import Channel


@WSHandler.register_callback(Subscribe, SubscribeResponse, permissions=WSHandler.PERM_AUTHORIZED)
def message_route(handler, subscribe):
    sub = Subscription(profile=handler.profile, event=subscribe.event,
                       category=subscribe.category, channel=subscribe.channel)
    gcm_topic_name = sub.fcm_topic_name()
    if gcm_topic_name:
        Firebase.subscribe(handler.iid_token, gcm_topic_name)
    sub.save()
    if subscribe.channel:
        Channel.objects(id=subscribe.channel.id).update_one(inc__subscribers=1)
    return SubscribeResponse(id=sub.id)


@WSHandler.register_callback(Unsubscribe, UnsubscribeResponse, permissions=WSHandler.PERM_AUTHORIZED)
def message_route(handler, unsubscribe):
    sub = Subscription.objects.get(id=unsubscribe.id)
    gcm_topic_name = sub.fcm_topic_name()
    if gcm_topic_name:
        Firebase.unsubscribe(handler.iid_token, gcm_topic_name)
    if sub.channel:
        Channel.objects(id=sub.channel.id).update_one(dec__subscribers=1)
    sub.delete()
    return UnsubscribeResponse()


@WSHandler.register_callback(GetSubscriptions, GetSubscriptionsResponse, permissions=WSHandler.PERM_AUTHORIZED)
def message_route(handler, get_subscriptions):
    resp = GetSubscriptionsResponse()
    resp.subscriptions = Subscription.objects(profile=handler.profile)
    return resp
