import json
from werkzeug.exceptions import NotAcceptable, InternalServerError, BadRequest
from mongoengine.queryset.visitor import Q
# from inapppy import GooglePlayValidator

from vsms.environment import service_env as env
from vsms.misc.time import utcnow_unix_time
from vsms.api.ws import WSHandler
from vsms.models.jrpc import JRPCCall
from vsms.models.appuser.message import Message, MessageResponse, \
    GetMessages, GetMessagesResponse, VideoMessage, LiveStreamMessage, \
    Donate, DonateResponse, MESSAGE_TYPE
from vsms.models.event import Event
from vsms.models.appuser.activity import UserActivityVideoStart, UserActivityLiveStreamStart
from vsms.adapters.inapp import OS, GooglePlayAPI


@WSHandler.register_callback(Message, MessageResponse, permissions=WSHandler.PERM_AUTHORIZED)
def message_route(handler, message):
    if handler.activity is None:
        raise NotAcceptable('No UserActivity passed')

    if handler.activity is UserActivityVideoStart:
        storable_message_type = VideoMessage
    elif handler.activity is UserActivityLiveStreamStart:
        storable_message_type = LiveStreamMessage
    else:
        raise InternalServerError('Not registered activity')

    storable_message = storable_message_type(ts=utcnow_unix_time(),
                                             text=message.text, media=handler.media.id,
                                             tp=message.tp, profile=handler.profile)

    if handler.activity is UserActivityLiveStreamStart:
        storable_message.event = Event(id=handler.event.id)
        storable_message.save()
        jrpc_message = JRPCCall(Message)()
        jrpc_message.params = Message(id=storable_message.id, ts=storable_message.ts,
                                      text=storable_message.text, media=handler.media,
                                      tp=storable_message.tp, profile=handler.profile)
        jrpc_message_json = json.dumps(jrpc_message.marshal())
        handler.group_publish(handler.group_name, jrpc_message_json)
    else:
        storable_message.save()

    return MessageResponse()


@WSHandler.register_callback(GetMessages, GetMessagesResponse,
                             permissions=WSHandler.PERM_AUTHORIZED | WSHandler.PERM_GUEST)
def get_messages_route(handler, get_messages):
    if handler.activity is None:
        raise NotAcceptable('No UserActivity passed')

    q = Q(media=handler.media.id, ts__gte=get_messages.ts)
    if get_messages.tp:
        q &= Q(tp__in=get_messages.tp)
    if handler.activity is UserActivityVideoStart:
        storable_message_type = VideoMessage
    elif handler.activity is UserActivityLiveStreamStart:
        storable_message_type = LiveStreamMessage
        q &= Q(event=handler.event.id)
    else:
        raise InternalServerError('Not registered activity')
    response = GetMessagesResponse()
    response.messages = storable_message_type.objects(q).order_by('ts').limit(get_messages.count)
    return response


@WSHandler.register_callback(Donate, DonateResponse,
                             permissions=WSHandler.PERM_AUTHORIZED | WSHandler.PERM_GUEST)
def donate_route(handler, donate):
    if handler.client_os not in OS.list():
        raise BadRequest('Unknown client_os={}'.format(handler.client_os))
    donate.client_os = handler.client_os
    donate.save()

    message = LiveStreamMessage(tp=MESSAGE_TYPE.DONATE, text=donate.amount, ts=utcnow_unix_time(),
                                event=donate.event.id, media=donate.media.id)
    if handler.permissions == WSHandler.PERM_AUTHORIZED:
        message.profile = handler.profile
    message.save()

    jrpc_message = JRPCCall(Message)()
    jrpc_message.params = message
    jrpc_message_json = json.dumps(jrpc_message.marshal())
    handler.group_publish('{}/{}'.format(donate.event.id, donate.media.id), jrpc_message_json)

    return DonateResponse()
