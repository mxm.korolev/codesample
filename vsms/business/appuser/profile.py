from werkzeug.exceptions import Conflict, NotFound
from datetime import datetime

from vsms.environment import informer
from vsms.api.ws import WSHandler
from vsms.adapters.firebase import Firebase
from vsms.models.appuser.profile import Profile, Register, RegisterResponse, \
    Authorize, AuthorizeResponse, EditProfile, EditProfileResponse, \
    GetProfile, GetProfileResponse, AcceptPromoCode, AcceptPromoCodeResponse
from vsms.models.appuser.subscription import Subscription


@WSHandler.register_on_open(Authorize)
def on_open(handler):
    handler.profile = None


@WSHandler.register_callback(Register, RegisterResponse,
                             permissions=WSHandler.PERM_NOTAUTHORIZED | WSHandler.PERM_GUEST)
def register_route(handler, register):
    if Profile.objects(social_network_id=register.social_network_id,
                       social_client_id=register.social_client_id):
        raise Conflict('User exists')

    profile = Profile(
        social_network_id=register.social_network_id,
        social_client_id=register.social_client_id,
        token=register.token,
        name=register.name,
        avatar_url=register.avatar_url,
        email=register.email,
        registered=datetime.utcnow()
    )
    profile.save()
    return RegisterResponse()


@WSHandler.register_callback(Authorize, AuthorizeResponse,
                             permissions=WSHandler.PERM_NOTAUTHORIZED | WSHandler.PERM_GUEST)
def authorize_route(handler, authorize):

    # will raise DoesNotExist
    profile = Profile.objects.get(social_network_id=authorize.social_network_id,
                                  social_client_id=authorize.social_client_id)
    handler.profile = profile
    handler.client_os = authorize.client_os
    handler.iid_token = authorize.iid_token
    handler.permissions = WSHandler.PERM_AUTHORIZED

    # sync subscriptions
    fcm_topics = Firebase.get_subscriptions(handler.iid_token)
    subs = Subscription.objects(profile=handler.profile)
    for sub in subs:
        topic = sub.fcm_topic_name()
        if not topic:
            continue
        if topic not in fcm_topics:
            Firebase.subscribe(handler.iid_token, topic)
        else:
            fcm_topics.remove(topic)
    for fcm_topic in fcm_topics:
        Firebase.unsubscribe(handler.iid_token, fcm_topic)

    return AuthorizeResponse(id=profile.id, name=profile.name, channel=profile.channel,
                             avatar_url=profile.avatar_url, private=profile.private)


@WSHandler.register_callback(GetProfile, GetProfileResponse,
                             permissions=WSHandler.PERM_AUTHORIZED)
def get_profile_route(handler, _):
    return GetProfileResponse(id=handler.profile.id, name=handler.profile.name, channel=handler.profile.channel,
                              avatar_url=handler.profile.avatar_url, private=handler.profile.private)


@WSHandler.register_callback(EditProfile, EditProfileResponse, permissions=WSHandler.PERM_AUTHORIZED)
def edit_profile_route(handler, edit_profile):
    if edit_profile.name:
        handler.profile.name = edit_profile.name
    if edit_profile.avatar_url:
        handler.profile.avatar_url = edit_profile.avatar_url
    handler.profile.save()
    return EditProfileResponse()


@WSHandler.register_callback(AcceptPromoCode, AcceptPromoCodeResponse, permissions=WSHandler.PERM_AUTHORIZED)
def accept_promo_code_route(handler, accept_promo_code):
    if accept_promo_code.promo_code == '253167':
        handler.profile.private = True
        handler.profile.save()
    else:
        raise NotFound('Unknown promo code')
    return AcceptPromoCodeResponse()