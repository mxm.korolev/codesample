from mongoengine.queryset import DoesNotExist

from vsms.api.ws import WSHandler
from vsms.models.appuser.history import GetVideoHistory, GetHistoryResponse, VideoHistory, \
    DeleteVideoHistory, DeleteVideoHistoryResponse


@WSHandler.register_callback(GetVideoHistory, GetHistoryResponse, permissions=WSHandler.PERM_AUTHORIZED)
def highlights_route(handler, get_history):
    resp = GetHistoryResponse()
    h = sorted(handler.profile.video_history.items(), key=lambda p: p[1], reverse=True)
    h = h[get_history.first:get_history.first+get_history.count]
    resp.total = len(handler.profile.video_history)
    resp.videos = []
    for video_id, ts in h:
        try:
            vh = VideoHistory(ts=ts, video=video_id)
            vh.video
        except DoesNotExist:
            del handler.profile.video_history[video_id]
            resp.total -= 1
            continue
        resp.videos.append(vh)
    return resp


@WSHandler.register_callback(DeleteVideoHistory, DeleteVideoHistoryResponse, permissions=WSHandler.PERM_AUTHORIZED)
def highlights_route(handler, delete_history):
    if delete_history.ids:
        for vid in delete_history.ids:
            try:
                handler.profile.video_history.pop(vid)
            except KeyError: # not std dict
                pass
    else:
        handler.profile.video_history.clear()
    return DeleteVideoHistoryResponse()
