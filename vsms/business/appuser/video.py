from werkzeug.exceptions import MethodNotAllowed, BadRequest

from vsms.api.ws import WSHandler
from vsms.models.video import EditVideo, EditVideoResponse


@WSHandler.register_callback(EditVideo, EditVideoResponse, permissions=WSHandler.PERM_AUTHORIZED)
def message_route(handler, edit_video):
    if handler.profile.channel is None:
        raise MethodNotAllowed('User is not a publisher')
    video = edit_video.video
    if handler.profile.channel != video.channel:
        raise MethodNotAllowed('User is not an owner')
    if not edit_video.available:
        video.delete()
        video = None
    else:
        video.title = edit_video.title
        video.category = edit_video.category
        video.save()
    return EditVideoResponse(video=video)
