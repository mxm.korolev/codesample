from vsms.api.ws import WSHandler
from vsms.models.appuser.handshake import Handshake, HandshakeResponse


@WSHandler.register_on_open(Handshake)
def init(handler):
    handler.permissions = WSHandler.PERM_NOTAUTHORIZED


@WSHandler.register_callback(Handshake, HandshakeResponse, permissions=WSHandler.PERM_NOTAUTHORIZED)
def handshake_route(handler, handshake):
    handler.permissions = WSHandler.PERM_GUEST
    handler.client_os = handshake.client_os
    return HandshakeResponse()
