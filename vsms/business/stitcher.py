from vsms.models.stitcher import Stitcher
from vsms.adapters.rabbitmq import online_idents
from vsms.adapters.elastic import ElasticSearch


def online_stitchers():
    idents = online_idents()
    return Stitcher.objects(name__in=idents)


def update_stitcher(stitcher, es=None):
    es = es or ElasticSearch()
    config = es.get_stitcher_config(stitcher.name)
    if config is None:
        return False
    stitcher.configuration.load_stitcher_format(config)
    stitcher.save()
    return True