import json

from vsms.environment import informer
from vsms.adapters.firebase import Firebase
from vsms.models.push import PushNotification, PushNotificationAndroid, PushNotificationData, \
    PushNotificationApns, PushNotificationApnsPayload, PushNotificationApnsHeaders, \
    PushNotificationApnsPayloadAlert, PushNotificationApnsPayloadAps, PUSH_NOTIFICATION_TYPE


def notify_event(event, tp, topic=None):

    notification_body = ''
    if tp == PUSH_NOTIFICATION_TYPE.EVENT_START:
        notification_body = 'The event is live'
    elif tp == PUSH_NOTIFICATION_TYPE.EVENT_FINISH:
        notification_body = 'The event has finished. You can find more upcoming events in the calendar.'

    notification = PushNotification(
        topic=str(topic or event.id),
        data=PushNotificationData(
            id=str(event.id),
            title=event.title,
            tp=PUSH_NOTIFICATION_TYPE.repr(tp),
        ),
        android=PushNotificationAndroid(
            collapse_key=str(event.id)
        ),
        apns=PushNotificationApns(
            headers=PushNotificationApnsHeaders(),
            payload=PushNotificationApnsPayload(
                aps=PushNotificationApnsPayloadAps(
                    thread_id=str(event.id),
                    alert=PushNotificationApnsPayloadAlert(
                        title=event.title,
                        body=notification_body
                    )
                )
            )
        ),
    )
    notification_marshal = notification.marshal()
    ret = Firebase.publish_topic_message(notification.marshal())
    informer.info('Push event={}: {}'.format(PUSH_NOTIFICATION_TYPE.repr(tp), json.dumps(notification_marshal)))
    if not ret:
        informer.error('Push event error')
    return ret


def notify_event_start(event):
    return notify_event(event, PUSH_NOTIFICATION_TYPE.EVENT_START)


def notify_event_finish(event):
    return notify_event(event, PUSH_NOTIFICATION_TYPE.EVENT_FINISH)
