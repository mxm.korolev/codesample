from werkzeug.exceptions import NotFound

from vsms.api.ws import WSHandler
from vsms.models.channel import Channel, EditChannel, EditChannelResponse, DeleteChannel, DeleteChannelResponse


@WSHandler.register_callback(EditChannel, EditChannelResponse,
                             permissions=WSHandler.PERM_AUTHORIZED)
def register_route(handler, edit_channel):
    if handler.profile.channel is None:
        handler.profile.channel = Channel()
    channel = handler.profile.channel
    channel.title = edit_channel.title
    channel.description = edit_channel.description
    channel.image_url = handler.profile.avatar_url
    channel.save()
    handler.profile.save()
    resp = EditChannelResponse()
    resp.channel = channel
    return resp


@WSHandler.register_callback(DeleteChannel, DeleteChannelResponse, permissions=WSHandler.PERM_AUTHORIZED)
def register_route(handler, _):
    if handler.profile.channel is None:
        raise NotFound()
    handler.profile.channel.delete()
    return DeleteChannelResponse()
