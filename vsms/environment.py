import argparse
import configparser
import pkg_resources
import logging
from copy import deepcopy
from os.path import join as path_join


class MultilineFormatter(logging.Formatter):
    def format(self, record):
        ret = str()
        msg_raws = deepcopy(record.getMessage())
        record.args = ()
        if record.exc_info:
            exc_text = self.formatException(record.exc_info)
            record.exc_info = None
        else:
            exc_text = None

        for msg_raw in msg_raws.split('\n'):
            record.msg = msg_raw
            if ret:
                ret += '\n'
            ret += super().format(record)

        if exc_text:
            for exc_raw in exc_text.split('\n'):
                record.msg = exc_raw
                if ret:
                    ret += '\n'
                ret += super().format(record)

        return ret


class Informer:

    @classmethod
    def initialize(cls, env, redefine_tornado_logging=True):

        logging_level = logging.INFO if env.debug else logging.ERROR
        cls.log = logging.getLogger(env.name)
        cls.log.propagate = False
        cls.log.setLevel(logging_level)
        cls.info = cls.log.info
        cls.warning = cls.log.warning
        cls.error = cls.log.error

        if env.log == 'syslog':
            handler = logging.handlers.SysLogHandler('/dev/log')
        elif env.log == '/dev/stdout':
            handler = logging.StreamHandler()
        else:
            handler = logging.FileHandler(env.log)

        formatter = MultilineFormatter('{0}[{1}] %(levelname)s: %(message)s'.format(env.name, env.port))
        handler.setFormatter(formatter)
        cls.log.addHandler(handler)

        if redefine_tornado_logging:
            for logger_name in ('tornado.access', 'tornado.application', 'tornado.general'):
                logger = logging.getLogger(logger_name)
                logger.handlers = []
                logger.addHandler(handler)
                logger.setLevel(logging_level)


class Environment:
    '''
    Environment configuration
    '''

    def __init__(self, ):
        self.initialized = False

    def __getattr__(self, name):
        if not self.initialized:
            raise Exception('Environment is not initialized')
        raise AttributeError(name)

    def fill_fields(self):
        pass

    def initialize(self, config_data=None, config_fn=None,
                   redefine_tornado_logging=True, raise_on_initialized=False,
                   command_interface=False, cli_args=None):

        cli_args = cli_args or []

        if self.initialized:
            if raise_on_initialized:
                raise Exception('Secondary initialization')
            else:
                return

        self.initialized = True
        self.config = configparser.ConfigParser()
        configfn = None

        if command_interface or cli_args or (not config_fn and not config_data):
            argparser = argparse.ArgumentParser()
            if not config_fn and not config_data:
                argparser.add_argument('--config', help='configuration file')
            if command_interface:
                argparser.add_argument('--command', help='command')
            for cli_arg in cli_args:
                argparser.add_argument('--{}'.format(cli_arg))
            args = argparser.parse_args()
            if command_interface:
                if not args.command:
                    raise Exception('--command "and options" required')
                self.command, *self.command_args = args.command.split(' ')
            if not config_fn and not config_data:
                if not args.config:
                    raise Exception('Use --help for args')
                configfn = args.config
            for cli_arg in cli_args:
                setattr(self, cli_arg, getattr(args, cli_arg))

        if config_fn:
            configfn = config_fn

        if configfn:
            self.config.read(configfn)
        elif config_data:
            self.config.read_string(config_data)
        else:
            raise FileNotFoundError('No configuration data passed')

        self.config.optionxform = str

        self.project_dir = pkg_resources.resource_filename('vsms', '')

        try:
            general = self.config['GENERAL']
            self.debug = general.get('debug', 'False') == 'True'
            self.log = general.get('log', '/dev/stdout')
            self.name = general.get('name', 'VSMS')
            self.fill_fields()
        except KeyError as e:
            print('No required field in configuration [%s]' % e)
            exit(1)
        except ValueError as e:
            print('Bad value in configuration [%s]' % e)
            exit(1)

        Informer.initialize(self, redefine_tornado_logging=redefine_tornado_logging)


class ServiceEnvironment(Environment):

    def fill_fields(self):

        self.template_dir = path_join(self.project_dir, 'templates')
        self.continents_fn = path_join(self.project_dir, 'data', 'continents.csv')
        self.countries_fn = path_join(self.project_dir, 'data', 'countries.csv')
        self.stop_words_fn = path_join(self.project_dir, 'data', 'stop_words.csv')

        general = self.config['GENERAL']
        self.timeout = int(general.get('timeout', 10))
        self.publisher_timeout = int(general.get('publisher_timeout', 600))
        self.delete_outdated_files = general.get('delete_outdated_files') == 'True'
        self.threads = int(general.get('threads', '0'))
        self.unittest = general.get('unittest') == 'True'
        self.ffmpeg = general.get('ffmpeg', '/usr/bin/ffmpeg')
        self.ffprobe = general.get('ffprobe', '/usr/bin/ffprobe')

        network = self.config['NETWORK']
        self.host = network.get('host', '127.0.0.1')
        self.port = int(network.get('port', 8888))

        mail = self.config['MAIL']
        self.mail_enabled = mail.get('enabled', 'True') == 'True'
        self.mail_ssl = mail.get('ssl', 'False') == 'True'
        self.mail_smtpserver = mail['smtpserver']
        self.mail_fromaddr = mail['fromaddr']
        self.mail_password = mail['password']
        self.mail_toaddrs = mail['toaddrs'].split(',')

        db = self.config['DB']
        self.db_host = db['host']
        self.db_port = int(db['port'])
        self.db_collection = db['collection']

        service = self.config['SERVICE']
        self.service_upload_dir = service['upload_dir']
        self.service_cdn_url = service['cdn_url']
        self.service_private_key = service['private_key']

        nginx = self.config['NGINX']
        self.nginx_stat_url = nginx['stat_url']
        self.nginx_hls_server_url = nginx['hls_server_url']
        self.nginx_control_url = nginx['control_url']
        self.nginx_rtmp_server_url = nginx['rtmp_server_url']
        self.nginx_rtmp_server_local_url = nginx['rtmp_server_local_url']
        self.nginx_hls_dir = nginx['hls_dir']
        self.nginx_hls_time = int(nginx['hls_time'])
        self.nginx_hls_list_size = int(nginx['hls_list_size'])
        self.nginx_record_dir = nginx['record_dir']
        self.nginx_rtmp_multiresolution_server_url = nginx['rtmp_multiresolution_server_url'].split(',')
        self.nginx_rtmp_application = nginx.get('application', 'hls')

        amqp = self.config['AMQP']
        self.amqp_host = amqp.get('host', '127.0.0.1')
        self.amqp_port = int(amqp.get('port', 5670))
        self.amqp_ssl = amqp['ssl'] == 'True'
        self.amqp_vhost = amqp['vhost']
        self.amqp_user = amqp['user']
        self.amqp_pass = amqp['pass']
        self.amqp_url = 'amqp://{user}:{pass}@{host}:{port}/{vhost}'.format(**amqp)

        bigquery = self.config['BIGQUERY']
        self.bigquery_service_account_json_fn = bigquery['service_account_json_fn']
        self.bigquery_ios_dataset = bigquery['ios_dataset']
        self.bigquery_android_dataset = bigquery['android_dataset']

        firebase = self.config['FIREBASE']
        self.firebase_firebase_url = firebase['firebaseurl']
        self.firebase_api_key = firebase['apikey']
        self.firebase_api_key_secret = firebase['apikeysecret']
        self.firebase_dynamic_link_domain = firebase['dynamiclinkdomain']
        self.firebase_app_link = firebase['applink']
        self.firebase_android_package_name = firebase['androidpackagename']
        self.firebase_android_min_package_version_code = firebase['androidminpackageversioncode']
        self.firebase_ios_bundle_id = firebase['iosbundleid']
        self.firebase_ios_app_store_id = firebase['iosappstoreid']
        self.firebase_service_account_json_fn = firebase['service_account_json_fn']

        elasticsearch = self.config['ELASTICSEARCH']
        self.elasticsearch_port = int(elasticsearch.get('port', 9201))

        inapp = self.config['INAPP']
        self.inapp_sandbox = inapp.get('sandbox', 'True') == 'True'
        self.inapp_applebundle = inapp['applebundle']
        self.inapp_androidbundle = inapp['androidbundle']
        self.inapp_androidapikey = inapp['androidapikey']


class StitcherEnvironment(Environment):
    '''
    Stitcher environment configuration
    '''

    def fill_fields(self):

        general = self.config['GENERAL']
        self.timeout = int(general.get('timeout', 30))

        network = self.config['NETWORK']
        self.host = network.get('host', '127.0.0.1')
        self.port = int(network.get('port', 8888))

        amqp = self.config['AMQP']
        self.amqp_host = amqp.get('host', '127.0.0.1')
        self.amqp_port = int(amqp.get('port', 5670))
        self.amqp_ssl = amqp['ssl'] == 'True'
        self.amqp_vhost = amqp['vhost']
        self.amqp_user = amqp['user']
        self.amqp_pass = amqp['pass']

        rtmp = self.config['RTMP']
        self.rtmp_default_app_url = rtmp['default_app_url']


service_env = ServiceEnvironment()
stitcher_env = StitcherEnvironment()
informer = Informer()


