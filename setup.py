'''
Setup script for Video Streaming Management System package
'''

from setuptools import setup, find_packages
import sys

import vsms

if sys.version_info <= (3,4):
    sys.exit('Python<=3.4 is not supported')

setup(
    name='vsms',
    version=vsms.__version__,
    description='Video Streaming Management System',

    packages=find_packages(),

    install_requires=[
        'coverage',
        'tornado==4.5.3',
        'psutil',
        'pika',
        'greenlet',
        'elasticsearch>=5.2',
        'pyinapp>=0.1.3',
        'pydevd',
        'Pillow',
        'flask',
        'flask-admin==1.5.0',
        'flask-mongoengine',
        'flask-security',
        'flask-restplus',
        'pymongo>=3.4,<4',
        'blinker',
        'bcrypt',
        'mongomock',
        'google-cloud-bigquery==0.26',
        'google_auth_oauthlib',
        'google-auth',
        'google-auth-httplib2',
        'google-api-python-client',
        'celery',
        'flower',
        'pytz',
        'werkzeug',
    ],

    entry_points={
        'console_scripts': [
            'vsms_stitcher=vsms.netcontrol.stitcher:entry',
            'vsms_admin=vsms.service:main',
            'vsms_terminal=vsms.terminal:main'
        ],
    },

    include_package_data=True,
)
